﻿using log4net;
using Newtonsoft.Json;
using ADP_Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using ADP_Common.Exception_Log;
using ADP_Common.Model;

namespace ADP_API.Controllers
{
    public class AdministratorController : ApiController
    {
        AdministratorRepository AdministratorRepositoryInstance = new AdministratorRepository();
        ErrorEntry errorLoger = new ErrorEntry();
        private static readonly log4net.ILog log = LogManager.GetLogger(typeof(AdministratorController));
        //Function called to get List of BaseKPIs for Admin Page BaseKPI Dropdown
        [Route("api/Administrator/GetBaseKPICollection")]
        [HttpGet]
        public HttpResponseMessage GetBaseKPICollection(int AsgId,string Keyword)
        {
            StringBuilder SearchJson = new StringBuilder();
            try
            {
                List<Search> GetSearchData = new List<Search>();
                GetSearchData = AdministratorRepositoryInstance.GetBaseKPICollection(AsgId,Keyword);
                SearchJson.Append("{\"BKPIDetails\":[");
                foreach (var Fields in GetSearchData)
                {
                    SearchJson.Append("\"" + Fields.SearchValue + "\",");
                }
                SearchJson = SearchJson.Remove(SearchJson.Length - 1, 1);
                SearchJson.Append("]}");
                return Request.CreateResponse(HttpStatusCode.OK, SearchJson);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //Function called to get the Columns mapped to associated Base KPI selected
        [Route("api/Administrator/GetBaseKPIColumnMapping")]
        [HttpGet]
        public HttpResponseMessage GetBaseKPIColumnMapping(int AsgId,string Keyword)
        {
            StringBuilder SearchJson = new StringBuilder();
            try
            {
                
                AsgName GetSearchData = new AsgName();
                GetSearchData = AdministratorRepositoryInstance.GetBaseKPIColumnMapping(AsgId,Keyword);
                SearchJson.Append("{\"ColumnData\":[");
                SearchJson.Append("{\"YesJSON\":[");
                foreach (var Fields in GetSearchData.listadmin)
                {
                    if (Fields.IsActive == "Y")
                    {
                        SearchJson.Append("{\"ColID\":\"" + Fields.ColID + "\",\"ColName\":\"" + Fields.ColName + "\",\"isActive\":\"" + Fields.IsActive + "\",\"DispColName\":\"" + Fields.DispColName + "\"},");
                    }

                }
                if (SearchJson.Length != 27)
                {
                    SearchJson = SearchJson.Remove(SearchJson.Length - 1, 1);
                }
                SearchJson.Append("]},");

                SearchJson.Append("{\"NoJSON\":[");
                foreach (var Fields in GetSearchData.listadmin)
                {
                    if (Fields.IsActive == "N")
                    {
                        SearchJson.Append("{\"ColID\":\"" + Fields.ColID + "\",\"ColName\":\"" + Fields.ColName + "\",\"isActive\":\"" + Fields.IsActive + "\",\"DispColName\":\"" + Fields.DispColName + "\"},");
                    }
                }
                SearchJson = SearchJson.Remove(SearchJson.Length - 1, 1);
                SearchJson.Append("]}");
                SearchJson = SearchJson.Remove(SearchJson.Length - 1, 1);
                SearchJson.Append("}],\"AsgName\":\""+ GetSearchData.Name+"\"}");
                return Request.CreateResponse(HttpStatusCode.OK, SearchJson);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //Function called to get the Master Column Mapping Table in the Admin page
        [Route("api/Administrator/GetMasterColumnMapping")]
        [HttpGet]
        public HttpResponseMessage GetMasterColumnMapping(int AsgID)
        {
            List<Admin> GetMasterData = new List<Admin>();
            try
            {
                GetMasterData = AdministratorRepositoryInstance.GetMasterColumnMapping(AsgID);
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(GetMasterData));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //Function to Save Columns mapped associated to given the BaseKPI
        [Route("api/Administrator/SaveBaseKPIColumnMapping")]
        [HttpPost]
        public HttpResponseMessage SaveBaseKPIColumnMapping(int AsgID,SaveBaseKPIColumnMapping SaveColumnMapping)
        {
            string GetSearchData ="";
            try
            {
                StringBuilder sbJSON = new StringBuilder();
                string name = SaveColumnMapping.name;
                string JsonPresent = SaveColumnMapping.YesArray;
                string JsonNotPresent = SaveColumnMapping.NoArray;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                AdminList sJson = (AdminList)serializer.Deserialize(JsonPresent, typeof(AdminList));
                AdminList nJson = (AdminList)serializer.Deserialize(JsonNotPresent, typeof(AdminList));
                GetSearchData = AdministratorRepositoryInstance.SaveBaseKPIColumnMapping(AsgID,name, sJson, nJson);
                return Request.CreateResponse(HttpStatusCode.OK, GetSearchData);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        
        //Function to save the Master Column mapping details
        [Route("api/Administrator/SaveMasterColumnMapping")]
        [HttpPost]
        public HttpResponseMessage SaveMasterColumnMapping(int AsgID, MasterDashColMapData MasterColMap)
        {
            string GetSearchData = "";
            StringBuilder SearchJson = new StringBuilder();
            try
            {
                string columndata = MasterColMap.MasterColMapData;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Admin> sJson = (List<Admin>)serializer.Deserialize(columndata, typeof(List<Admin>));
                GetSearchData = AdministratorRepositoryInstance.SaveMasterColumnMapping(AsgID,sJson);
                return Request.CreateResponse(HttpStatusCode.OK, GetSearchData);
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        [Route("api/Administrator/GetDetailKPILists")]
        [HttpGet]
        public HttpResponseMessage GetDetailKPILists()
        {
            List<Widget> GetKPIList = new List<Widget>();
            try
            {
                GetKPIList = AdministratorRepositoryInstance.GetKPIListDetail();
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(GetKPIList));
            }
            catch (Exception ex)
            {
                errorLoger.LogErrorMessage(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
