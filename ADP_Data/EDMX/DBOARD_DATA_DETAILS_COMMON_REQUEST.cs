//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ADP_Data.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class DBOARD_DATA_DETAILS_COMMON_REQUEST
    {
        public Nullable<int> ASG_ID { get; set; }
        public string ASG_CREATED_BY { get; set; }
        public string REQUEST_ID { get; set; }
        public string SUMMARY { get; set; }
        public string DESCRIPTION { get; set; }
        public string REQUEST_TYPE { get; set; }
        public string REQUEST_STATUS { get; set; }
        public string STATUS_STATE { get; set; }
        public string REQUEST_IMPACT { get; set; }
        public string REQUEST_URGENCY { get; set; }
        public string REQUEST_PRIORITY { get; set; }
        public string PRIORITY_REVISED_COUNT { get; set; }
        public string CREATED_BY { get; set; }
        public string REQUESTER_FULL_NAME { get; set; }
        public string REQUESTER_COMPANY_ID { get; set; }
        public string REQUESTER_COMPANY_NAME { get; set; }
        public string LOCATION { get; set; }
        public string COUNTRY { get; set; }
        public string REGION { get; set; }
        public string RESOLVED_1ST_LINE { get; set; }
        public string ASSIGNEE { get; set; }
        public string ASSIGNED_GROUP { get; set; }
        public string ASSIGNEE_GROUP_COMPETENCE { get; set; }
        public string ASSIGN_REVISED_COUNT { get; set; }
        public string ASSIGNED_SERVICE_DESK { get; set; }
        public string SUPPORT_TIER { get; set; }
        public string CREATED_TIMESTAMP { get; set; }
        public string RESPONSE_TIMESTAMP { get; set; }
        public string RESOLUTION_TIMESTAMP { get; set; }
        public string CLOSED_TIMESTAMP { get; set; }
        public string LAST_MODIFIED_TIMESTAMP { get; set; }
        public string LAST_ACTIONED_TIMESTAMP { get; set; }
        public string AREA_ID { get; set; }
        public string AREA_NAME { get; set; }
        public string SERVICE_CATEGORY_TIER_1 { get; set; }
        public string SERVICE_CATEGORY_TIER_2 { get; set; }
        public string SERVICE_CATEGORY_TIER_3 { get; set; }
        public string REQUESTER_CATEGORY { get; set; }
        public string ROOT_CATEGORY_TIER_1 { get; set; }
        public string ROOT_CATEGORY_TIER_2 { get; set; }
        public string ROOT_CATEGORY_TIER_3 { get; set; }
        public string ROOT_CAUSE { get; set; }
        public string CI { get; set; }
        public string TOTAL_ESTIMATED_TIME { get; set; }
        public string TOTAL_USED_TIME { get; set; }
        public string SOURCE { get; set; }
        public string Solution { get; set; }
        public string CAB_NAME { get; set; }
        public string CAB_STATUS { get; set; }
        public string RESPONSE_TIME_IN_MINS { get; set; }
        public string RESOLUTION_TIME_IN_MINS { get; set; }
        public string PENDING_REQUESTER_MINS { get; set; }
        public string PENDING_IT_MINS { get; set; }
        public string MINUTES_OPEN { get; set; }
        public string CURRENT_STATUS_TIME_IN_MINS { get; set; }
        public string IS_Exceeding_Response { get; set; }
        public string IS_Exceeding_Resolve { get; set; }
        public string REQUEST_TYPE_CATEGORY { get; set; }
        public string DUEDATE_TIMESTAMP { get; set; }
        public string REOPEN_COUNT { get; set; }
        public string ReOpened_Date { get; set; }
    }
}
