﻿using ADP_Data.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ADP_Data.Repository.Common
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetDetails(Expression<Func<T, bool>> UserID);
        void Add(T entity);
        void Update(T entity);
    }
}
