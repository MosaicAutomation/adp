﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADP_Data.SpModel
{
    class USP_CREATE_SCOREBOARD_DATA
    {
        public Int32 COUNT_TOTAL { get; set; }
        public string KPI_ICON { get; set; }
        public int? TYPE { get; set; }
    }
}
