﻿appDashboard.controller('AddPreProcWordModalController', function ($scope,$http, $uibModalInstance, mySharedService, Notification) {
    $scope.isAddStopSynm = true;
    $scope.AssessmentId = mySharedService.AssessmentId;
    if ($scope.IsStopVal == 0) {
        $scope.HeaderName = "Stop Words"
        $scope.IsStop = true;
        $scope.IsSynonym = false;
    } else {
        $scope.HeaderName = "Synonyms"
        $scope.IsStop = false;
        $scope.IsSynonym = true;
    }
    $scope.ok = function () {
        $scope.isAddStopSynm = false;
        if ($scope.IsStopVal == 0) {
            $scope.HeaderName="Stop Words"
            $scope.IsStop = true;
            $scope.IsSynonym = false;
            $scope.AddWordUrl = "api/Proffer/AddPreProcWord?AsgId=" + $scope.AssessmentId + "&IsStop=0&Word=" + $scope.StopWordVal;
        } else {
            $scope.HeaderName = "Synonyms"
            $scope.IsStop = false;
            $scope.IsSynonym = true;
            $scope.AddWordUrl = "api/Proffer/AddPreProcWord?AsgId=" + $scope.AssessmentId + "&IsStop=1&Word=" + $scope.UsedVal + "&ReplWord=" + $scope.SynonymVal;
        }
        $http({
            method: 'GET',
            url: uriApiURL + $scope.AddWordUrl,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .success(function (response) {
            var AsgWord = [];
            var RelId = angular.fromJson(response);
            Notification.success({message:""+$scope.AddType+" added successfully"});
            $uibModalInstance.dismiss('cancel');
            $scope.isAddStopSynm = true;
            if ($scope.IsStopVal == 0) {
                AsgWord.push({ 'Rel_Id': RelId, 'word': $scope.StopWordVal })
            }
            else{
                AsgWord.push({ 'Rel_Id': RelId, 'word': $scope.UsedVal, 'rep_word': $scope.SynonymVal })
            }
            $scope.$parent.$broadcast("GetStopWordSynonym", $scope.IsStopVal, AsgWord);
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

})