﻿appDashboard.controller('DwnlProcModalController', function ($scope, $http, $uibModalInstance, mySharedService, Notification) {
    $scope.isDwnldProc = true;
    $scope.AssessmentId = mySharedService.AssessmentId;
    $scope.ok = function () {
        $scope.isDwnldProc = false;
        if ($scope.chckProc == true) {
            $scope.ProcData = 0;
        }else{
            $scope.ProcData = 1;
        }
        if ($scope.chckDwnl == true) {
            $scope.DwnlData=0
        } else {
            $scope.DwnlData = 1;
        }
        $scope.UpdateStopSynm($scope.ProcData, $scope.DwnlData);
        $scope.isDwnldProc = true;
    };
    $scope.$on('CloseModal', function () {
        $uibModalInstance.dismiss('cancel');
    })
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
})