appDashboard.controller('DashboardCloneModalController', function ($scope, $uibModalInstance, $http, mySharedService, $uibModal, Notification, $window, $location, $routeParams) {
    $scope.chosenIconText = "fa-picture-o";
    $scope.AssessmentId = mySharedService.AssessmentId;
    $scope.SharedUser = [];
	$scope.HeaderName = "";
	$scope.dashname = $scope.dashname;
	$scope.dashBoardName = $scope.dashName+"_copy"
	$scope.chooseIconModal = function (size) {
		mySharedService.searchicon($scope, size)
	};
	$scope.$on('handlecloseIconModal', function () {
			$scope.chosenIconText = mySharedService.chosenIcon;
		});
		
		$scope.clonedash = function () {
			
		var CloneDashboardObj = { 'dashBoardName': $scope.dashBoardName,  'dashBoardIcon': $scope.chosenIconText};
		var CloneDashboardVal = "" + JSON.stringify(CloneDashboardObj) + "";
		 var CloneDashboardData = { 'CloneDb': CloneDashboardVal };
		$http({
                method: 'POST',
                url: uriApiURL + "api/Dashboard/CloneDashboardSettings?AsgId=" + $scope.AssessmentId + "&DashboardID=" + $scope.dashBoardID,
				params: {version: getEpocStamp()},
			    data: CloneDashboardData,
			    contentType: "application/json",
			    dataType: "json"
            })
            .success(function (response) {
                Notification.success({
                    verticalSpacing: 60,
                    message: 'New Dashboard Clone Created Succesfully.'
                });
				mySharedService.DashboardCache = false;
				$scope.NewDashData = angular.fromJson(response);				
				$uibModalInstance.dismiss('cancel');
				var newDashPath  = '/dashboard/'+$scope.NewDashData.Dashboard.Id;
				$location.path(newDashPath);
         
            }).error(function (response) {
                Notification.error({
                    verticalSpacing: 60,
                    message: 'Error on Cloning Dashboard. <br> Please Try again.'
                });
				
         
            });
		};
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
});