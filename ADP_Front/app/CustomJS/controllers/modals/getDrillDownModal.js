﻿appDashboard.controller('GetDrillDownModalController', function ($scope,$timeout, $rootScope, $http, $uibModalInstance, mySharedService, Notification) {
    $scope.IsDrillAvail = false;
    $scope.AssessmentId = mySharedService.AssessmentId;
        $scope.IsDrillNotAvail = false;
        $scope.isTckDrill = true;
        console.log("open" + $scope.isTckDrill);
        var steps = 6;
        $scope.colors = generateColorGradient('', $scope.ATClr, steps);
        $scope.colors.reverse();
        $scope.steps = steps;
        if ($scope.TckTypeVal == 0) {
            $scope.Tcktype = "Accept Clustering";
        } else if ($scope.TckTypeVal == 1) {
            $scope.Tcktype = "Uncategorized";
        } else if ($scope.TckTypeVal == 2) {
            $scope.Tcktype = "Send for Re-clustering";
        } else if ($scope.TckTypeVal == 3) {
            $scope.Tcktype = "Not yet Clustered ";
        }
        $scope.MFGetDrillValues = function () {
            $scope.isTckDrill = true;
            $http.get(uriApiURL + "api/Proffer/GetDrillDownData?AsgId=" + $scope.AssessmentId + "&Type=" + $scope.TckTypeVal)
            .then(function (response) {
                $scope.WhlDt = "";
                $scope.tst = [];
                console.log("success" + $scope.isTckDrill);
                $scope.WholeData = angular.fromJson(response.data);
                if ($scope.TckTypeVal == 0) {
                    if ($rootScope.ActDrillVal.length != 0) {
                        for (var i = 0; i < $rootScope.ActDrillVal.length; i++) {
                            if ($rootScope.ActDrillVal[i].IsTk == 0) {
                                $scope.WholeData.ClusterData.push($rootScope.ActDrillVal[i]);
                                $scope.WhlDt = $scope.WhlDt + " " + $rootScope.ActDrillVal[i].pd;
                            } else if ($rootScope.ActDrillVal[i].IsTk == 1) {
                                var position = search("tn", $rootScope.ActDrillVal[i].tn, $scope.WholeData.ClusterData)
                                $scope.WholeData.ClusterData.splice(position, 1);
                            }
                        }
                    }
                } else if ($scope.TckTypeVal == 3) {
                    if ($rootScope.NotClusteredVal.length != 0) {
                        for (var i = 0; i < $rootScope.NotClusteredVal.length; i++) {
                            if ($rootScope.NotClusteredVal[i].IsTk == 0) {
                                $scope.WholeData.ClusterData.push($rootScope.NotClusteredVal[i]);
                                $scope.WhlDt = $scope.WhlDt + " " + $rootScope.NotClusteredVal[i].pd;
                            } else if ($rootScope.NotClusteredVal[i].IsTk == 1) {
                                var position = search("tn", $rootScope.NotClusteredVal[i].tn, $scope.WholeData.ClusterData)
                                $scope.WholeData.ClusterData.splice(position, 1);
                            }
                        }
                    }
                } else if ($scope.TckTypeVal == 1) {
                    if ($rootScope.MarkDrillVal.length != 0) {
                        for (var i = 0; i < $rootScope.MarkDrillVal.length; i++) {
                            if ($rootScope.MarkDrillVal[i].IsTk == 0) {
                                $scope.WholeData.ClusterData.push($rootScope.MarkDrillVal[i]);
                                $scope.WhlDt = $scope.WhlDt + " " + $rootScope.MarkDrillVal[i].pd;
                            } else if ($rootScope.MarkDrillVal[i].IsTk == 1) {
                                var position = search("tn", $rootScope.MarkDrillVal[i].tn, $scope.WholeData.ClusterData)
                                $scope.WholeData.ClusterData.splice(position, 1);
                            }
                        }
                    }
                } else if ($scope.TckTypeVal == 2) {
                    if ($rootScope.SndDrillVal.length != 0) {
                        for (var i = 0; i < $rootScope.SndDrillVal.length; i++) {
                            if ($rootScope.SndDrillVal[i].IsTk == 0) {
                                $scope.WholeData.ClusterData.push($rootScope.SndDrillVal[i]);
                                $scope.WhlDt = $scope.WhlDt + " " + $rootScope.SndDrillVal[i].pd;
                            } else if ($rootScope.SndDrillVal[i].IsTk == 1) {
                                var position = search("tn", $rootScope.SndDrillVal[i].tn, $scope.WholeData.ClusterData)
                                $scope.WholeData.ClusterData.splice(position, 1);
                            }
                        }
                    }
                }
                $scope.tst=stringCount($scope.WhlDt, " ");
                if ($scope.WholeData.WordCloud == undefined) {
                    $scope.WholeData.WordCloud = [];
                }
                for (var i = 0; i < $scope.tst.length; i++) {
                    $scope.WholeData.WordCloud.push($scope.tst[i])
                }
                if ($scope.WholeData.ClusterData.length != 0) {
                    $scope.IsDrillAvail = true;
                    $scope.IsDrillNotAvail = false;
                    $scope.isTckDrill = false;
                    $timeout(function () {
                        $scope.PlotWordCloud($scope.WholeData.WordCloud);
                        $scope.GetDrillDownTable($scope.WholeData.ClusterData);
                    }, 0);
                } else {
                    $scope.IsDrillAvail = false;
                    $scope.IsDrillNotAvail = true;
                    $scope.isTckDrill = false;
                }
            })
        }
        function search(key, nameKey, myArray) {
            var pos = -1;
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i][key].toUpperCase() === nameKey.toUpperCase()) {
                    pos = i;
                    break;
                }
            }
            return pos;
        }
        $scope.PlotWordCloud = function (data) {
            $('#Drill_Cloud').empty();
            $('#Drill_Cloud').jQCloud('destroy');
            $timeout(function () {
                $('#Drill_Cloud').jQCloud(data, {
                    colors: $scope.colors,
                    autoResize: true
                });
            });
        }
        $scope.GetDrillDownTable = function (data) {
            var dropDownValues = ['Accept Clustering', 'Send for Re-clustering', 'Mark as Uncategorized'];
            $('#DrillDownTbl').handsontable({
                data: data,
                colHeaders: ['Ticket No', 'Category Tier 1', 'Category Tier 2', 'Category Tier 3', 'Description', 'P', 'Tags', 'Disposition'],
                columnHeaderHeight: 20,
                height: 350,
                rowHeight: function (row) {
                    return 30;
                },
                defaultRowHeight: 30,
                columnSorting: true,
                manualColumnResize: true,
                sortIndicator: true,
                stretchH: "all",
                colWidths: [110, 130, 130, 130, 250, 1, 150, 150],
                columns: [
                    {
                        data: 'tn',
                        readOnly: true,
                    },
                    {
                        data: 'ct1',
                        readOnly: true
                    },
                    {
                        data: 'ct2',
                        readOnly: true
                    },
                    {
                        data: 'ct3',
                        readOnly: true
                    },
                    {
                        data: 'sd',
                        readOnly: true
                    },
                    {
                        data: 'pd',
                        readOnly: true
                    },
                    {
                        data: 'Tags'
                    },
                    {
                        editor: 'select',
                        selectOptions: dropDownValues,
                        data: 'Disposition'
                    }
                ]
            });
            var ht = $("#DrillDownTbl").handsontable("getInstance");
            $scope.DrillRows = ht.countRows();
        }
        $timeout(function () {
            $scope.MFGetDrillValues();
        },0);
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        console.log("close" + $scope.isTckDrill);
        $scope.isTckDrill = false;
        function stringCount(haystack, needle) {
            var getval = [];
            if (!needle || !haystack) {
                return false;
            }
            else {

                var words = haystack.split(needle),
                    count = {};
                for (var i = 0, len = words.length; i < len; i++) {
                    if (count.hasOwnProperty(words[i])) {
                        count[words[i]] = parseInt(count[words[i]], 10) + 1;
                        getval = $.grep(getval, function (obj) {
                            if (obj.text.toUpperCase() != words[i].toUpperCase())
                                return obj
                        })
                        getval.push({ text: '' + words[i] + '', weight: count[words[i]] })
                    }
                    else {
                        count[words[i]] = 1;
                        getval.push({ text: '' + words[i] + '', weight: 1 });
                    }
                }
                return getval;
            }
        }
});