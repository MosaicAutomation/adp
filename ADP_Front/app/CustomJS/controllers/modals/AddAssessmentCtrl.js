﻿appDashboard.controller('AddAsgModalController', function ($scope, $http,$location, $uibModalInstance, mySharedService, Notification) {
    $scope.SaveTickFlag = false;
    $scope.AssessmentId = mySharedService.AssessmentId;
    $scope.ok = function () {
        $scope.SaveTickFlag = true;
        var url = uriApiURL + "api/Proffer/AddAsg?AsgName=" ;
        var AsgFileName = { 'AsgName': $scope.AsgFileName };
        $http({
            method: 'POST',
            url: url,
            params: { version: getEpocStamp() },
            data: AsgFileName,
            contentType: "application/json",
            dataType: "json"        
        })
        .success(function (response) {
            $scope.AssessmentId = angular.fromJson(response);
            mySharedService.AsgName = $scope.AsgFileName;
            $location.path("/proc/" + $scope.AssessmentId);
            $scope.SaveTickFlag = false;
        })
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
})