appDashboard.controller('ZoomChartModalController', function ($scope, $uibModalInstance, $http, mySharedService) {
    $scope.HeaderName = $scope.$parent.kpiname;
    $scope.AssessmentId = mySharedService.AssessmentId;
        mySharedService.KPIID = $scope.KPI.Id;
        mySharedService.dashBoardID = $scope.dashBoardID;
        mySharedService.IsZoom = true;
        $scope.$on("ZoomDataDone", function () {
            $scope.isFetchingDatas = true;
            $scope.BlurClass = "BlurClass";
        });

        $scope.cancelZoom = function () {
            mySharedService.IsZoom = false;
            $uibModalInstance.dismiss('cancel');
            $scope.$broadcast('ZoomClose');

        }

    })