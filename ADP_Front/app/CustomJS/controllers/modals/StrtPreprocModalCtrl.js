﻿appDashboard.controller('StartPreprocModalController', function ($scope, $http, $uibModalInstance,$location, mySharedService, Notification) {
    $scope.isStartPreProc = true;
    $scope.GotoView = function () {
        $http.get(uriApiURL + "api/Proffer/SetAsgDataProc?AsgID=" + $scope.AssessmentId + "&Condition=1&Type=0")
        .then(function (response) {
            mySharedService.IsProcChange = 1;
            $scope.AssessmentId = mySharedService.AssessmentId;
            $scope.$parent.$broadcast("SaveClusterData");
            $uibModalInstance.dismiss('cancel');
        })
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.DownloadClusterData = function () {
        $scope.$parent.$broadcast("SaveClusterData");
        $scope.ResendClusterDt = mySharedService.ResendClusterData;        
        mySharedService.Json2CSV($scope.ResendClusterDt, "Resend_For_Clustering");
        $uibModalInstance.dismiss('cancel');
    }
})