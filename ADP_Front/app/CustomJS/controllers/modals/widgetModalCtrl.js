appDashboard.controller('WidgetModalController', function ($scope, $uibModalInstance, $routeParams, $http, mySharedService, Notification) {
    $scope.AssessmentId = mySharedService.AssessmentId;
	/*WidgetModalCtrl Controller related to the _WidgetSettings.html*/    
    console.log("WIDGET SETTINGS..."); 
    /*Variable Declaration begins*/	
    $scope.QueryJs = {};
    $scope.chosenIconText = "fa-picture-o";
    $scope.QueryVl = {};
	//$scope.IsPeriodic =  false;
	//$scope.PeriodicTime = "60";
	//$scope.PeriodicTimeUnit = "SS";
	
    $scope.ScoreBoardType = [{
        name: '0',
		cname: 'Count',
        ticked: false
    }, {
        name: '1',		
		cname: 'Donut',
        ticked: false
    }];
    $scope.KpiGroupType = $scope.grpType;
    $scope.KpiGroupAvailType = [{
        name: 'ScoreCard',
        value: 0
    }, {
        name: 'Cursol',
        value: 1
    }, {
        name: 'Group',
        value: 3
    }];
	$scope.KpiPeriodTime = [{
        name: 'Hours',
        value: 'HH'
    }, {
        name: 'Minutes',
        value: 'MM'
    }, {
        name: 'Seconds',
        value: 'SS'
    }];
    $scope.KpiGroupAvailList = [];
    $scope.KpiGroupAvailList = mySharedService.GroupDetailFilter;
    if ($scope.grpID) {
        $scope.KpiGroupItem = $scope.grpID;
        for (var i = 0; i < $scope.KpiGroupAvailList.length; i++) {
            if ($scope.KpiGroupAvailList[i].GroupID == $scope.KpiGroupItem) {
                $scope.KpiGroupName = $scope.KpiGroupAvailList[i].GroupName;
                $scope.KpiGroupKey = $scope.KpiGroupAvailList[i].GroupKey;
            }
        }
    }
    $scope.KpiGroupID = "";
    $scope.changeClr = function (value) {
        if (value == false || value == undefined) {
            $scope.BlurClas = "BlurClass";
            $scope.colorhd = ""
        } else {
            $scope.BlurClas = "";
            $scope.colorhd = ""
        }
    }
    $scope.chooseIconModal = function (size) {
        mySharedService.searchicon($scope, size)
    };
    $scope.isQueryResult = false;
    $scope.changeFun = function (value) {
        if (value == false || value == undefined) {
            for (var i = 0; i < $scope.ChildValue.length; i++) {
                $scope.ChildValue[0].ticked = false;
            }
            $scope.sliObj = $scope.ChildValue;

            $scope.BlurClass = "BlurClass";

        } else {
            $scope.sliObj = $scope.CriticObjOP;

            $scope.BlurClass = "";
        }
    }
    $scope.GetChartColors = function (baseKPIID, value) {
        var chartColor;
        chartColor = {
            "BKPI0001": {
                "chartName": ["Average Response Time", "No. of Requests"],
                "chartColor": ["#13709E", "#A4D9F3"]
            },
            "BKPI0008": {
                "chartName": ["Average Resln Time", "No. of Requests"],
                "chartColor": ["#13709E", "#A4D9F3"]
            },
            "BKPI0002": {
                "chartName": ["Incoming", "Backlog", "Completed"],
                "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
            },
            "BKPI0003": {
                "chartName": ["Time spent", "No. of Requests"],
                "chartColor": ["#13709E", "#A4D9F3"],
                "children": {
                    "TIME": {
                        "chartName": ["Time spent", "No. of Requests"],
                        "chartColor": ["#13709E", "#A4D9F3"]
                    },
                    "COUNT": {
                        "chartName": ["Time spent", "No. of Requests"],
                        "chartColor": ["#13709E", "#A4D9F3"]
                    },
                    "AVG_RESP_TIME": {
                        "chartName": ["Average Response Time", "No. of Requests"],
                        "chartColor": ["#13709E", "#A4D9F3"]
                    },
                    "AVG_RESLN_TIME": {
                        "chartName": ["Average Resln Time", "No. of Requests"],
                        "chartColor": ["#13709E", "#A4D9F3"]
                    },
                    "EXCEEDING_RESP_KPI": {
                        "chartName": ["Requests Exceeding SLA", "Requests not exceeding SLA", "SLA Adherence%"],
                        "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
                    },
                    "EXCEEDING_RESLN_KPI": {
                        "chartName": ["Requests Exceeding SLA", "Requests not exceeding SLA", "SLA Adherence%"],
                        "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
                    },
                    "RESPONSE_EXCEEDING_PERC": {
                        "chartName": ["Requests Exceeding SLA", "Requests not exceeding SLA", "Avg. Response Time"],
                        "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
                    },
                    "RESOLVE_EXCEEDING_PERC": {
                        "chartName": ["Requests Exceeding SLA", "Requests not exceeding SLA", "Avg. Resolution Time"],
                        "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
                    },
                    "INCOMING_REQUEST": {
                        "chartName": ["Incoming", "Backlog", "Completed"],
                        "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
                    },
                    "COMPLETE_REQUEST": {
                        "chartName": ["Incoming", "Backlog", "Completed"],
                        "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
                    },
                    "BACKLOG_REQUEST": {
                        "chartName": ["Incoming", "Backlog", "Completed"],
                        "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
                    },
                    "REOPEN_COUNT": {
                        "chartName": ["ReOpenCount"],
                        "chartColor": ["#13709E"]
                    }
                }
            },
            "BKPI0004": {
                "chartColor": [],
                "chartName": []
            },
            "BKPI0005": {
                "chartName": ["Requests Exceeding SLA", "Requests not exceeding SLA", "SLA Adherence%"],
                "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
            },
            "BKPI0006": {
                "chartName": ["Requests Exceeding SLA", "Requests not exceeding SLA", "SLA Adherence%"],
                "chartColor": ["#13709E", "#A4D9F3", "#5BA4C8"]
            },
            "BKPI0007": {
                "chartColor": [],
                "chartName": []
            },
            "BKPI0009": {
                "chartColor": [],
                "chartName": []
            },
            "BKPI0010": {
                "chartColor": [],
                "chartName": []
            },
            "BKPI0011": {
                "chartColor": [],
                "chartName": []
            },
            "BKPI0012": {
                "chartName": ["Split By Color"],
                "chartColor": ["#13709E"]
            },
            "BKPI0013": {
                "chartName": ["Hour Heat Map"],
                "chartColor": []
            },
            "BKPI0014": {
                "chartName": ["DayHeat Map"],
                "chartColor": []
            }
        }
        if (chartColor[baseKPIID].hasOwnProperty("children")) {
            return chartColor[baseKPIID].children[value];
        } else {
            return chartColor[baseKPIID];
        }
    }
    $scope.HardCodedDropDownVal = function () {
        $scope.IsLineShowObj = [{
                name: "Yes",
                value: "Y",
                ticked: false
            },
            {
                name: "No",
                value: "N",
                ticked: false
            }];
        $scope.UnitShowObj = [
            {
                name: "Time spent",
                value: "TIME",
                ticked: false
            },
            {
                name: "No. of Requests",
                value: "COUNT",
                ticked: false
            },
            {
                name: "Highest Average Response Time",
                value: "AVG_RESP_TIME",
                ticked: false
            },
            {
                name: "Highest Average Resolution Time",
                value: "AVG_RESLN_TIME",
                ticked: false
            },
            {
                name: "Highest No. of Requests Exceeding Response SLA",
                value: "EXCEEDING_RESP_KPI",
                ticked: false
            },
            {
                name: "Highest Percentage of Requests Exceeding Response SLA",
                value: "RESPONSE_EXCEEDING_PERC",
                ticked: false
            },
            {
                name: "Highest No. Of Requests Exceeding Resolution SLA",
                value: "EXCEEDING_RESLN_KPI",
                ticked: false
            },
            {
                name: "Highest Percentage of Requests Exceeding Resolution SLA",
                value: "RESOLVE_EXCEEDING_PERC",
                ticked: false
            },
            {
                name: "Highest No. of Incoming Request",
                value: "INCOMING_REQUEST",
                ticked: false
            },
            {
                name: "Highest No. of Completed Requests",
                value: "COMPLETE_REQUEST",
                ticked: false
            },
            {
                name: "Highest No. of Backlog Requests",
                value: "BACKLOG_REQUEST",
                ticked: false
            },
            {
                name: "Highest No. of Reopened Tickets",
                value: "REOPEN_COUNT",
                ticked: false
            }
        ];        
        $scope.ageAnalysisCondSel;
        $scope.ageAnalysisCondObj = [{
            "name": "Greater Than",
            "ticked": false,
            "value": ">"
        }, {
            "name": " Equal  Greater Than",
            "ticked": false,
            "value": "<"
        }, {
            "name": "Lesser Than",
            "ticked": false,
            "value": "<"
        }, {
            "name": "Equal & Lesser Than",
            "ticked": false,
            "value": "<"
        }, {
            "name": "Lesser Than",
            "ticked": false,
            "value": "<"
        }, {
            "name": "Inbetween",
            "ticked": false,
            "value": "<>"
        }];
        $scope.ageAnalysisLimitObj = [{
            "name": "Hours",
            "ticked": false,
            "value": "HH"
        }, {
            "name": "Days",
            "ticked": false,
            "value": "DD"
        }, {
            "name": "Week",
            "ticked": false,
            "value": "WK"
        }, {
            "name": "Month",
            "ticked": false,
            "value": "MM"
        }, {
            "name": "Years",
            "ticked": false,
            "value": "YR"
        }];
        $scope.StatArrID = 0;
        $scope.ageAnalysisQueryObj = [{
            "id": 0,
            "value": "",
            "value1": "",
            "condition": "",
            "limit": ""
        }];

        $scope.upDageAnalysisCondSels = function (value) {
            $scope.ageAnalysisCondSel = value;
        }
        $scope.ageAnalysisAddObj = function () {
            $scope.StatArrID++;
            var newTEmpArr = {
                "id": $scope.StatArrID,
                "value": "",
                "value1": "",
                "condition": "",
                "limit": ""
            };
            $scope.ageAnalysisQueryObj.push(newTEmpArr);
        };
        $scope.ageAnalysisDelObj = function ($index) {
            $scope.ageAnalysisQueryObj.splice($index, 1);;
        };
        $scope.chartTypeObj = [{
            name: "Grouped",
            ticked: false,
            value: 1
        }, {
            name: "Stacked",
            ticked: false,
            value: 0
        }];
        $scope.ReportTypeObj = [{
            name: "Weeks",
            ticked: false
        }, {
            name: "Months",
            ticked: false
        }];
        $scope.MonthObj = [{
                name: "1",
                ticked: false
            },
            {
                name: "2",
                ticked: false
            },
            {
                name: "3",
                ticked: false
            },
            {
                name: "4",
                ticked: false
            },
            {
                name: "5",
                ticked: false
            },
            {
                name: "6",
                ticked: false
            },
            {
                name: "7",
                ticked: false
            },
            {
                name: "8",
                ticked: false
            },
            {
                name: "9",
                ticked: false
            }];
    }
    $scope.isLineChartShow = true;
    $scope.ok = function () {
        var widgetdata = $scope.WidgetDetailModel();
        if (widgetdata != undefined) {
            //mySharedService.KPIboardCache = false;
            mySharedService.addWidgetFun($scope.makeWidgetData(), widgetdata);            
            $uibModalInstance.dismiss('cancel');
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.HardCodedDropDownVal();
    var url = uriApiURL + "api/UserSetting/GetWidgetSettings";
    var userID = mySharedService.UserID;
    $scope.Asgid = mySharedService.AssessmentId;
    if ($scope.Asgid == undefined) {
        $scope.Asgid = '';
    }
    if ($scope.isKPISetting == true) {
        url = url + "?KPIID=" + $scope.KPIID + "&Asg_Id=" + $scope.Asgid + "&ISKPI=1";
    } else {
        url = url + "?KPIID=" + $scope.KPIID + "&Asg_Id=" + $scope.Asgid + "&ISKPI=0";
    }
    $http({
        method: 'GET',
        url: url,
        data: {
            'UserID': "",
            'KPIID': $scope.KPIID,
            'ISKPI': 1
        }
    })
        .success(function (response) {

            /* get group det*/
            //console.log("OPEN " + $scope.KpiGroupType + " = " + $scope.KpiGroupItem + " = " + $scope.KpiGroupName ) ;
            if ($scope.KPIID) {
                for (var i = 0; i < $scope.KpiGroupAvailList.length; i++) {
                    for (var j = 0; j < $scope.KpiGroupAvailList[i].KPI.length; j++) {
                        if ($scope.KPIID == $scope.KpiGroupAvailList[i].KPI[j].Id) {
                            $scope.KpiGroupItem = $scope.KpiGroupAvailList[i].GroupID;
                            mySharedService.KPIGroupUpdate.grpID = $scope.KpiGroupAvailList[i].GroupID;
                            $scope.KpiGroupName = $scope.KpiGroupAvailList[i].GroupName;
                            $scope.KpiGroupKey = $scope.KpiGroupAvailList[i].GroupKey;
                        }
                    }
                }

            }
            //console.log("OPEN " + $scope.KpiGroupType + " = " + $scope.KpiGroupItem + " = " + $scope.KpiGroupName ) ;
            /* get group det*/

            $scope.isFetchingDatas = true;
            $scope.disableSize = false;
            $scope.PriorityObjOP;
            $scope.WholeData = JSON.parse(response);
            $scope.PriorityObj = $scope.WholeData.Request_Priority;
            $scope.PriorityOutPutObj = $scope.PriorityObj;
            $scope.RequestObj = $scope.WholeData.Request;
            $scope.StatusObj = $scope.WholeData.Request_Status;
            $scope.CriticObj = $scope.WholeData.Criticality;
			//if($scope.WholeData.IsPeriodic == "Y"){
			//	$scope.IsPeriodic = true ;
			//}
			//$scope.PeriodicTime = $scope.WholeData.PeriodicTime;
			//$scope.PeriodicTimeUnit = $scope.WholeData.PeriodicTimeUnit;

            console.log("ScoreCard CHECH" + $scope.KpiGroupType);
            if ($scope.KpiGroupType == 'ScoreCard') {
                $scope.BaseKPIID = "BKPI0007";


            } else {
                $scope.BaseKPIID = $scope.WholeData.BaseKPI[0].BaseKPIID;
            }

            $scope.IsWithSLA = $scope.WholeData.IsWithSLA;
            // $scope.WidgetObj = JSON.parse(response).WidgetObj;
            $scope.WidgetObj = [];
            var AllWidgetObj = $scope.WholeData.WidgetObj;
            console.log("AllWidgetObj " + AllWidgetObj.length)
            for (var ri = 0; ri < AllWidgetObj.length; ri++) {
                if (($scope.KpiGroupType == 'ScoreCard') && (AllWidgetObj[ri].value == 'BKPI0007')) {
                    $scope.WidgetObj.push(AllWidgetObj[ri]);

                } else if (($scope.KpiGroupType != 'ScoreCard') && (AllWidgetObj[ri].value != 'BKPI0007')) {
                    $scope.WidgetObj.push(AllWidgetObj[ri]);

                }
            }

            console.log("WidgetObj " + $scope.WidgetObj.length)


            if ($scope.WholeData.FilterObj != undefined) {
                $scope.FilterObj = JSON.parse(response).FilterObj;
                $scope.FilterHMObj = JSON.parse(response).FilterObj;
            }
            if ($scope.WholeData.PivotObj != undefined) {
                $scope.GroupObj = JSON.parse(response).PivotObj;
                $scope.splitObj = JSON.parse(response).PivotObj;
                $scope.splitObj1 = JSON.parse(response).PivotObj;
            }
            $scope.sliObj = [];
            var position;
            $scope.Ob1 = [];
            if ($scope.isKPISetting == true) {
                $scope.BlurClass = "";
                $scope.b = [];
                $scope.CriticObj.forEach(function (entry) {
                    if (entry.ticked == true) {
                        $scope.b.push(entry);
                    }
                });
                $scope.b.forEach(function (entry1) {
                    sessionStorage.user = JSON.stringify(entry1);
                    $scope.a = JSON.parse(sessionStorage.user);
                    if ($scope.a.name != $scope.WholeData.SLICri[0].name) {
                        $scope.a.ticked = false;
                    }
                    $scope.sliObj.push($scope.a);
                });
                var position = search("value", $scope.BaseKPIID, $scope.WidgetObj);
                $scope.IsParents = $scope.WholeData.IsParent;
                $scope.IsUseTheme = $scope.WholeData.IsUseTheme;
                $scope.WidgetObj[position].ticked = true;
                position = searchval("value", $scope.WholeData.IsGrouped, $scope.chartTypeObj)
                $scope.chartTypeObj[position].ticked = true;
                $scope.chartColorJSON = {
                    "chartColor": [],
                    "chartName": []
                };
                if ($scope.BaseKPIID == "BKPI0009") {
                    $scope.SLIAgeQuery = angular.fromJson($scope.WholeData.SLIAgeQuery[0].SLIAgeQuery);
                    $scope.SLIAgeSelect = $scope.WholeData.SLIAgeSelect;
                    $scope.ageAnalysisCondSel = $scope.SLIAgeSelect;
                    $scope.ageAnalysisQueryObj = $scope.SLIAgeQuery;
                }
                if ($scope.BaseKPIID == "BKPI0003") {
                    position = search("value", $scope.WholeData.ParetoColumn[0].value, $scope.FilterObj)
                    $scope.FilterObj[position].ticked = true;
                    position = search("value", $scope.WholeData.IsLine[0].value, $scope.IsLineShowObj)
                    $scope.IsLineShowObj[position].ticked = true;
                    position = search("value", $scope.WholeData.WhichUnit[0].value, $scope.UnitShowObj)
                    $scope.UnitShowObj[position].ticked = true;
                    if ($scope.WholeData.WhichUnit[0].value == "COUNT" || $scope.WholeData.WhichUnit[0].value == "TIME") {
                        $scope.isLineChartShow = true;
                        $scope.isRespReslnPareto = false;
                    } else if ($scope.WholeData.WhichUnit[0].value == "AVG_RESP_TIME" || $scope.WholeData.WhichUnit[0].value == "AVG_RESLN_TIME" || $scope.WholeData.WhichUnit[0].value == "EXCEEDING_RESP_KPI" || $scope.WholeData.WhichUnit[0].value == "EXCEEDING_RESLN_KPI") {
                        $scope.isRespReslnPareto = true;
                        $scope.isLineChartShow = false;
                    } else {
                        $scope.isLineChartShow = false;
                        $scope.isRespReslnPareto = false;
                    }
                    $scope.chartColorJSON = $scope.GetChartColors($scope.BaseKPIID, $scope.WholeData.WhichUnit[0].value)
                } else if ($scope.BaseKPIID == "BKPI0011") {
                    position = search("value", $scope.WholeData.ParetoColumn[0].value, $scope.FilterObj)
                    $scope.FilterObj[position].ticked = true;
                    $scope.chartColorJSON = $scope.GetChartColors($scope.BaseKPIID, $scope.BaseKPIID)
                } else if ($scope.BaseKPIID == "BKPI0004" || $scope.BaseKPIID == "BKPI0012") {
                    var groupByData = $scope.WholeData.GroupData[0].value.split(",");
                    angular.forEach(groupByData, function (item) {
                        position = search("value", item, $scope.GroupObj)
                        $scope.GroupObj[position].ticked = true;
                        var position1 = search("value", item, $scope.splitObj)
                        $scope.splitObj.splice(position1, 1);
                        $scope.Ob1.push({
                            "name": $scope.GroupObj[position]['name'],
                            "value": $scope.GroupObj[position]['value'],
                            "ticked": $scope.GroupObj[position]['ticked']
                        });
                    });
                    position = search("value", $scope.WholeData.SplitData[0].value, $scope.splitObj)
                    $scope.splitObj[position].ticked = true;
                    var position1 = search("value", $scope.WholeData.SplitData[0].value, $scope.GroupObj)
                    $scope.GroupObj.splice(position1, 1)
                    $scope.chartColorJSON = $scope.GetChartColors($scope.BaseKPIID, $scope.BaseKPIID)
                } else if ($scope.BaseKPIID == "BKPI0010") {
                    var groupByData = $scope.WholeData.GroupData[0].value.split(",");
                    angular.forEach(groupByData, function (item) {
                        position = search("value", item, $scope.GroupObj)
                        $scope.GroupObj[position].ticked = true;
                        $scope.Ob1.push({
                            "name": $scope.GroupObj[position]['name'],
                            "value": $scope.GroupObj[position]['value'],
                            "ticked": $scope.GroupObj[position]['ticked']
                        });
                    });
                    $scope.chartColorJSON = $scope.GetChartColors($scope.BaseKPIID, $scope.BaseKPIID)
                } else if ($scope.BaseKPIID == "BKPI0007") {
                    $scope.chosenIconText = $scope.WholeData.QueryData.QueryIcon;
                    $scope.QueryJs = $scope.WholeData.QueryData.QueryJSON;
                    $scope.QueryVl = $scope.WholeData.QueryData.QueryValue;
                    $scope.SBType =""+ $scope.WholeData.QueryData.ScoreBoardType+"";
                    position = search("name", $scope.SBType,$scope.ScoreBoardType)
                    $scope.ScoreBoardType[position].ticked = true;
                    console.log("position" + position);
                    position = search("value", $scope.BaseKPIID, $scope.WidgetObj);
                    $scope.WidgetObj[position].ticked = true;
                } else {
                    position = search("name", $scope.WholeData.Plot[0].PlotValue, $scope.MonthObj)
                    $scope.MonthObj[position].ticked = true;
                    position = search("name", $scope.WholeData.Plot[0].PlotType, $scope.ReportTypeObj)
                    $scope.ReportTypeObj[position].ticked = true;
                    $scope.chartColorJSON = $scope.GetChartColors($scope.BaseKPIID, $scope.BaseKPIID)
                }
               if ($scope.BaseKPIID == "BKPI0014" || $scope.BaseKPIID == "BKPI0013") {
                    position = search("value", $scope.WholeData.ParetoColumn[0].value, $scope.FilterHMObj)
                    $scope.FilterHMObj[position].ticked = true;
                //$scope.chartColorJSON = $scope.GetChartColors($scope.BaseKPIID, $scope.BaseKPIID)
                }
                $scope.chartColorJSON.chartColor = $scope.WholeData.ChartColor.split(",");
                if ($scope.IsParents == 1) {
                    $scope.IsParent = true;
                    $scope.BlurClass = "BlurClass";
                }
            } else {
                $scope.IsParent = true;
                $scope.IsUseTheme = $scope.WholeData.IsUseTheme;
                for (var i = 1; i < $scope.ChildValue.length; i++) {
                    $scope.ChildValue[i].ticked = false;
                }
                //$scope.sliObj = $scope.ChildValue;
                $scope.CriticObj.forEach(function (entry) {
                    if (entry.ticked == true) {
                        $scope.sliObj.push(entry);
                    }
                });
                var result = [];
                $scope.distinct = [];
                for (var len = $scope.sliObj.length, i = 0; i < len; ++i) {
                    var name = $scope.sliObj[i].name;
                    if (result.indexOf(name) > -1) continue; {
                        result.push(name);
                        $scope.distinct.push($scope.sliObj[i]);
                    }
                }
                $scope.sliObj = $scope.distinct;
                position = search("name", "6", $scope.MonthObj);
                $scope.MonthObj[position].ticked = true;
                position = search("name", "Months", $scope.ReportTypeObj);
                $scope.ReportTypeObj[position].ticked = true;

                if ($scope.KpiGroupType == 'ScoreCard') {
                    position = search("value", "BKPI0007", $scope.WidgetObj);
                } else {
                    position = search("value", "BKPI0001", $scope.WidgetObj);
                }

                $scope.WidgetObj[position].ticked = true;
                $scope.FilterObj[1].ticked = true;
                position = search("value", "Y", $scope.IsLineShowObj);
                $scope.IsLineShowObj[position].ticked = true;
                position = search("value", "COUNT", $scope.UnitShowObj);
                $scope.UnitShowObj[position].ticked = true;
                position = searchval("value", 1, $scope.chartTypeObj)
                $scope.chartTypeObj[position].ticked = true;
                $scope.Ob1.push({
                    "name": "",
                    "value": "",
                    "ticked": ""
                });
                $scope.isLineChartShow = true;
                $scope.chartColorJSON = $scope.GetChartColors("BKPI0001", "BKPI0001")
                $scope.ScoreBoardType[0].ticked = true;
            }
            if ($scope.IsUseTheme == 1) {
                $scope.IsATCLr = true;
                $scope.BlurClas = "BlurClass"
            } else {
                $scope.IsATCLr = false;
                $scope.BlurClas = "";
            }
            $scope.chartName = $scope.chartColorJSON.chartName;
            $scope.chartColorItem = $scope.chartColorJSON.chartColor;
            $scope.widgetName = $scope.WholeData.KPIName;
            if ($scope.KpiGroupType == 'ScoreCard') {
                if ($scope.KPIID) {
                    $scope.BaseKPIName = $scope.WholeData.BaseKPI[0].BaseKPIName;



                } else {
                    $scope.BaseKPIName = $scope.WholeData.BaseKPI[6].BaseKPIName;

                }
                $scope.WidgetObjOP.push($scope.WholeData.WidgetObj[6]);



            } else {
                $scope.BaseKPIName = $scope.WholeData.BaseKPI[0].BaseKPIName;
            }

            $scope.chosenWidget($scope.BaseKPIID, $scope.WholeData.WhichUnit);

        });
    $scope.GetValues2Plot = function () {
        $scope.selectedVal = [];
        $scope.$broadcast('getSingleValue');
    }
    $scope.$on('pingqueryBack', function (e, data, jsonval, cmt) {
        $scope.output = data;
        $scope.comment = cmt;
        $scope.Jsonval = jsonval;
    });
    $scope.$on('pingSingleBack', function (e, data) {
        $scope.selectedVal.push(data);
    });
    $scope.selectWidgetClk = function (data) {
        $scope.isShown = false;
        $scope.isPivot = false;
        $scope.chosenWidget(data[0].value);
        if (data[0].value == "BKPI0003") {
            $scope.chartColorJSON = $scope.GetChartColors(data[0].value, $scope.UnitShowObOP[0].value);
            $scope.chartName = $scope.chartColorJSON.chartName;
            $scope.chartColorItem = $scope.chartColorJSON.chartColor;
        } else {
            $scope.chartColorJSON = $scope.GetChartColors(data[0].value, data[0].value);
            $scope.chartName = $scope.chartColorJSON.chartName;
            $scope.chartColorItem = $scope.chartColorJSON.chartColor;
        }

    };
    $scope.selectCriticality = function (data) {
        $scope.sliObj = [];
        $scope.CriticObjOP.forEach(function (entry) {
            entry.ticked = false;
            $scope.sliObj.push(entry);
        });
    };
    $scope.makeWidgetData = function () {
        var settingData = {};
        if ($scope.KPIID != "") {
            for (var i = 0; i < $scope.KPIVal.length; i++) {
                if ($scope.KPIVal[i].KPI.Id == $scope.KPIID) {
                    settingData.sizeX = parseInt($scope.KPIVal[i].sizeX);
                    settingData.sizeY = parseInt($scope.KPIVal[i].sizeY);
                }
            }
        } else {
            if ($scope.WidgetObjOP[0].value == "BKPI0007") {
                settingData.sizeX = 2;
                settingData.sizeY = 2;
            } else {
                settingData.sizeX = 4;
                settingData.sizeY = 2;
            }
        }
        settingData.KPI = {};
        settingData.KPI.KPIName = $scope.widgetName;
        settingData.KPI.Id = $scope.WidgetObjOP[0].idNum;
        settingData.KPI.idNum = $scope.WidgetObjOP[0].idNum;
        settingData.KPI.baseKPIID = $scope.WidgetObjOP[0].name;
        return settingData;
    };
    $scope.WidgetDetailModel = function () {
        var self = {},
            cmt = "";
        if ($scope.widgetName == "") {
            cmt += "\nPlease Enter Name for widget!"
        }
        if ($scope.IsParent != true) {
            if ($scope.PriorityObjOP.length == 0) {
                cmt += "\n Please Select Request Priority!"
            }
            if ($scope.RequestObjOP.length == 0) {
                cmt += "\n Please Select Request Type!"
            }
            if ($scope.StatusObjOP.length == 0) {
                cmt += "\n Please Select Request Status!"
            }
            if ($scope.CriticObjOP.length == 0) {
                cmt += "\n Please Select Criticality!"
            }
        }
        if ($scope.KpiGroupType == 'ScoreCard') {
            if ($scope.KPIID) {
                $scope.BaseKPIName = $scope.WholeData.BaseKPI[0].BaseKPIName;
            } else {
                $scope.BaseKPIName = $scope.WholeData.BaseKPI[6].BaseKPIName;
            }
            if ($scope.WidgetObjOP.length == 0) {
                $scope.WidgetObjOP.push($scope.WholeData.WidgetObj[6]);
            }



        }


        if ($scope.sliObjOP.length == 0 && ($scope.WidgetObjOP[0].value == "BKPI0001" || $scope.WidgetObjOP[0].value == "BKPI0008")) {
            cmt += "\n Please Select SLA Unit!"
        } else if ($scope.WidgetObjOP[0].value == "BKPI0003") {
            if ($scope.sliObjOP.length == 0 && ($scope.UnitShowObOP[0].value == "EXCEEDING_RESP_KPI" || $scope.UnitShowObOP[0].value == "EXCEEDING_RESLN_KPI")) {
                cmt += "\n Please Select SLA Unit!"
            }
        }
		if ($scope.WidgetObjOP[0].value == "BKPI0009") {
            
			if ($scope.ageAnalysisCondSel == "") {
               cmt += "\n Please Select Value to Group By!"
            }
			var AgeArrayValidate = false;
			angular.forEach($scope.ageAnalysisQueryObj, function (item) {
				if ((item.value == "") || (item.condition == "") || (item.limit == "")  ){
				AgeArrayValidate = true
				} 			
				               
            });
			if(AgeArrayValidate){
				 cmt += "\n Please Enter Values for Conditions!"
				}
			
        }
        if ($scope.WidgetObjOP[0].value == "BKPI0004" || $scope.WidgetObjOP[0].value == "BKPI0012") {
            $scope.GetValues2Plot();
            if ($scope.selectedVal.length == 0) {
                cmt += "\n Please Select Value to Group By!"
            }
            if (duplicateVal($scope.selectedVal).length > 0) {
                cmt += "\nGroup by should not be duplicate!"
            }

            if ($scope.splitObjOP != [] && $scope.splitObjOP.length == 0) {
                cmt += "\n Please Select Value to Split!"
            } else if ($scope.selectedVal.indexOf($scope.splitObjOP[0].value) > -1) {
                cmt += "\n Split By should have value other than Group by chosen values!"
            }
            if (($scope.Ob1.length == 1) && ($scope.WidgetObjOP[0].value == "BKPI0004")) {
                cmt += "\n Please Select Inner Level to Group by!"
            }

        }
        if ($scope.WidgetObjOP[0].value == "BKPI0010") {
            $scope.GetValues2Plot();
            if ($scope.selectedVal.length == 0) {
                cmt += "\n Please Select Value to Group By!"
            }
            if (duplicateVal($scope.selectedVal).length > 0) {
                cmt += "\nGroup by should not be duplicate!"
            }
        }
        if ($scope.WidgetObjOP[0].value == "BKPI0007") {
            $scope.$broadcast('buildquery');
            if ($scope.comment != "") {
                cmt += "\n" + $scope.comment + " in Query Builder";
            }
        }
        if (cmt != "") {
            Notification.error({
                verticalSpacing: 60,
                message: "" + cmt + "",
                title: '<i class="fa fa-warning"></i>&nbsp;Operation not allowed!'
            });
        } else {
            if ($scope.KPIID != "") {
                for (var i = 0; i < $scope.KPIVal.length; i++) {
                    if ($scope.KPIVal[i].KPI.Id == $scope.KPIID) {
                        self.SizeXAxis = parseInt($scope.KPIVal[i].sizeX);
                        self.SizeYAxis = parseInt($scope.KPIVal[i].sizeY);
                    }
                }
            } else {
                self.SizeXAxis = 2;
                self.SizeYAxis = 1;
            }
			
			//console.log(" $scope.IsPeriodic" + $scope.IsPeriodic)
			//if (($scope.IsPeriodic == true) || ($scope.IsPeriodic == 'true')){
			//	self.IsPeriodic = 'Y' ;
			//}else{
			//	self.IsPeriodic = 'N' ;
			//}			
			//self.PeriodicTime = $scope.PeriodicTime;
			//self.PeriodicTimeUnit = $scope.PeriodicTimeUnit;
			
			
            self.Request_Priority = $scope.makeStringOfKeyValues($scope.PriorityObjOP, "name");
            self.Request = $scope.makeStringOfKeyValues($scope.RequestObjOP, "name");
            self.Request_Status = $scope.makeStringOfKeyValues($scope.StatusObjOP, "name");
            self.Criticality = $scope.makeStringOfKeyValues($scope.CriticObjOP, "name");
            self.StartDate = "";
            self.EndDate = "";
            self.PositionXAxis = "";
            self.PositionYAxis = "";
            self.IsParent = $scope.IsParent ? 1 : 0;
            self.IsUseTheme = $scope.IsATCLr ? 1 : 0;
			
			if($scope.sliObjOP.length > 0) {
				self.IsWithSLA = $scope.IsWithSLA;
			}else{
				self.IsWithSLA = 2;
			}
            self.ChartColor = $scope.chartColorItem.join(",");
            self.BaseKPIID = $scope.WidgetObjOP[0].value;
            if (self.BaseKPIID == "BKPI0003") {
                self.ParetoBarColumnName = $scope.FilterObjOP[0].name;
                self.ParetoLineValue = $scope.FilterObjOP[0].name;
                self.ParetoLineUnit = $scope.FilterObjOP[0].value;
                self.ParetoBarUnit = $scope.UnitShowObOP[0].value;
                self.IsLine = $scope.IsLineShowObjOP[0].value;
            } else if (self.BaseKPIID == "BKPI0011") {
                self.ParetoBarColumnName = $scope.FilterObjOP[0].name;
                self.ParetoLineValue = $scope.FilterObjOP[0].name;
                self.ParetoLineUnit = $scope.FilterObjOP[0].value;
            } else if (self.BaseKPIID == "BKPI0004" || self.BaseKPIID == "BKPI0012") {
                $scope.GetValues2Plot();
                self.GroupData = $scope.selectedVal.toString();
                self.SplitData = $scope.splitObjOP[0].value;
            } else if (self.BaseKPIID == "BKPI0010") {
                $scope.GetValues2Plot();
                self.GroupData = $scope.selectedVal.toString();
            } else if (self.BaseKPIID == "BKPI0007") {
                self.QueryIcon = $scope.chosenIconText;
					self.SizeXAxis = 1;
					self.SizeYAxis = 1;
                self.IsQuery = 1;
                self.QueryValue = $scope.output;
                if ($scope.ScoreBoardTypeOp.length > 0) {
                    self.ScoreBoardType = $scope.ScoreBoardTypeOp[0].name;
                } else {
                    self.ScoreBoardType = $scope.ScoreBoardType[0].name;
                }
                self.QueryJSON = $scope.Jsonval;
            } else {
                self.PlotType = $scope.ReportTypeObjOP[0].name;
                self.PlotValue = $scope.MonthObjOP[0].name;
            }
            if (self.BaseKPIID == "BKPI0014" || self.BaseKPIID == "BKPI0013") {
                self.ParetoBarColumnName = $scope.FilterHMObjOp[0].name;
                self.ParetoLineValue = $scope.FilterHMObjOp[0].name;
                self.ParetoLineUnit = $scope.FilterHMObjOp[0].value;
            }
            if (self.BaseKPIID == "BKPI0001") {
                self.isResp = "Y";
                self.SLICriticality = $scope.sliObjOP[0].name;
            }
            if (self.BaseKPIID == "BKPI0008") {
                self.isResp = "N";
                self.SLICriticality = $scope.sliObjOP[0].name;
            }
            if (self.BaseKPIID == "BKPI0009") {
                self.SLIAgeSelect = $scope.ageAnalysisCondSel;


                var value = [],
                    condition = [],
                    unit = [];
                for (var i = 0; i < $scope.ageAnalysisQueryObj.length; i++) {
                    if ($scope.ageAnalysisQueryObj[i].condition == "<>") {
                        value.push($scope.ageAnalysisQueryObj[i].value + " ^ " + $scope.ageAnalysisQueryObj[i].value1);
                    } else {
                        value.push($scope.ageAnalysisQueryObj[i].value);
                    }
                    condition.push($scope.ageAnalysisQueryObj[i].condition);
                    unit.push($scope.ageAnalysisQueryObj[i].limit);
                }
                self.value = value.toString();
                self.unit = unit.toString();
                self.condition = condition.toString();
                self.SLIAgeQuery = JSON.stringify($scope.ageAnalysisQueryObj);
            }
            if (self.BaseKPIID == "BKPI0003") {
                if ($scope.UnitShowObOP[0].value == "EXCEEDING_RESP_KPI" || $scope.UnitShowObOP[0].value == "EXCEEDING_RESLN_KPI") {
                    self.SLICriticality = $scope.sliObjOP[0].name;
                }
            }
            self.DashBoardIID = $routeParams.dashboardId;
            self.IsGrouped = $scope.chartTypeObjOP[0].value;
            self.KPIName = $scope.widgetName;
            self.KPIID = $scope.KPIID;
            //self.UserID = mySharedService.UserID;
            self.Resolution = sessionStorage.getItem("Resolution");
            console.log("KpiGroupID " + $scope.KpiGroupItem)
            self.KpiType = $scope.KpiGroupType;
            self.GroupKey = $scope.KpiGroupKey;
            self.KpiGroupID = $scope.KpiGroupItem;
			

            //mySharedService.KPIGroupOldID  =  ;
            //console.log("$scope.KpiGroupItem" + $scope.KpiGroupItem)
            mySharedService.KPIGroupUpdate.grpID = $scope.KpiGroupItem;

            self.GroupName = '';
            for (var i = 0; i < $scope.KpiGroupAvailList.length; i++) {
                if ($scope.KpiGroupAvailList[i].GroupID == $scope.KpiGroupItem) {
                    self.GroupName = $scope.KpiGroupAvailList[i].GroupName;
                    self.GroupKey = $scope.KpiGroupAvailList[i].GroupKey;

                }
            }

            return self;
        }
    };
    $scope.makeStringOfKeyValues = function (data, keyTochoose) {
        var dataArr = [];
        for (var itemData in data) {
            dataArr.push(data[itemData][keyTochoose]);
        }
        return dataArr.join(",");
    };
    $scope.chosenWidget = function (baseKPIID, whichUnit) {
        $scope.isPivot = false;
        $scope.isPareto = false;
        $scope.isIcon = false;
	$scope.isDayHeatMap=false;
        $scope.isRespResln = false;
        $scope.isWordCloud = false;
        $scope.isIncomVsComp = false;
        $scope.isPivotChart = false;
        $scope.isQueryBuild = false;
        $scope.isSize = true;
        $scope.isHeatMapFilter = false;
        $scope.isHeatMap = false
        $scope.isChartTypeShown = false;
        $scope.isAgingReport = false;
        $scope.isTreeMap = false;
        $scope.isWidgetUIToshow = true;
        if (baseKPIID == "BKPI0001" || baseKPIID == "BKPI0008") {
            $scope.isRespResln = true;
            $scope.isSize = false;
        } else if (baseKPIID == "BKPI0002" || baseKPIID == "BKPI0005" || baseKPIID == "BKPI0006") {
            $scope.isIncomVsComp = true;
            $scope.isChartTypeShown = true;
            $scope.isSize = false;
        } else if (baseKPIID == "BKPI0003") {
            if (whichUnit != undefined && whichUnit[0].value != "COUNT" && whichUnit[0].value != "TIME" && whichUnit[0].value != "AVG_RESP_TIME" && whichUnit[0].value != "AVG_RESLN_TIME") {
                $scope.isChartTypeShown = true;
            } else {
                $scope.isChartTypeShown = false;
            }
            $scope.isPareto = true;
            $scope.isSize = false;
        } else if (baseKPIID == "BKPI0011") {
            $scope.isWordCloud = true;
            $scope.isSize = false;
            $scope.isWidgetUIToshow = false;
        } else if (baseKPIID == "BKPI0004") {
            $scope.isPivot = true;
            $scope.isSize = false;
            $scope.isWidgetUIToshow = false;
        } else if (baseKPIID == "BKPI0010") {
            $scope.isTreeMap = true;
            $scope.isSize = false;
            $scope.isWidgetUIToshow = false;
        } else if (baseKPIID == "BKPI0012") {
            $scope.isPivotChart = true;
            $scope.isSize = false;
            $scope.isWidgetUIToshow = true;
        } else if (baseKPIID == "BKPI0007") {
            $scope.isIcon = true;
            $scope.isQueryBuild = true;
            $scope.isSize = false;
            $scope.isWidgetUIToshow = false;
        } else if (baseKPIID == "BKPI0009") {
            $scope.isAgingReport = true;
            $scope.isSize = false;
            $scope.isWidgetUIToshow = false;
        } else if (baseKPIID == "BKPI0013" || baseKPIID == "BKPI0014") {
            $scope.isHeatMap = true;
            $scope.isHeatMapFilter = true;
        }
	if(baseKPIID == "BKPI0014")
	{
		$scope.isDayHeatMap=true;
	}
        if (baseKPIID == "BKPI0003" || baseKPIID == "BKPI0012"  || baseKPIID == "BKPI0013"||baseKPIID == "BKPI0014" || baseKPIID == "BKPI0011" || baseKPIID == "BKPI0001" || baseKPIID == "BKPI0008" || baseKPIID == "BKPI0002" || baseKPIID == "BKPI0004" || baseKPIID == "BKPI0010" || baseKPIID == "BKPI0005" || baseKPIID == "BKPI0006") {
            $scope.isSize = false;
        }
    }
    $scope.GroupByClose = function () {

        $scope.GetValues2Plot();
        $scope.GroupObjOP1 = $scope.selectedVal;
        $scope.splitObjs = [];
        for (var j = 0; j < $scope.splitObj1.length; j++) {
            $scope.splitObjs.push({
                "name": $scope.splitObj1[j].name,
                "value": $scope.splitObj1[j].value,
                "ticked": false
            });
        }
        if ($scope.GroupObjOP1.length != 0) {
            $scope.splitObj = $scope.splitObjs;
            angular.forEach($scope.GroupObjOP1, function (item) {
                var position = search("value", item, $scope.splitObj)
                if (position > -1) {
                    $scope.splitObj.splice(position, 1);
                }
            });
        } else {
            $scope.splitObj = $scope.GroupObj;
        }
        if ($scope.splitObjOP.length != 0) {
            var pos = search("value", $scope.splitObjOP[0].value, $scope.splitObj)
            $scope.splitObj[pos].ticked = true;
        }
    }
    $scope.SplitByClose = function () {

        $scope.GetValues2Plot();
        $scope.GroupObjOP1 = $scope.selectedVal;
        $scope.splitObjs = [];
        for (var j = 0; j < $scope.splitObj1.length; j++) {
            $scope.splitObjs.push({
                "name": $scope.splitObj1[j].name,
                "value": $scope.splitObj1[j].value,
                "ticked": false
            });
        }
        if ($scope.splitObjOP.length != 0) {
            $scope.GroupObj = $scope.splitObjs;
            var pos = 0;
            for (var i = 0; i < $scope.GroupObj.length; i++) {
                if ($scope.splitObjOP[0].value.toUpperCase() == $scope.GroupObj[i].value.toUpperCase()) {
                    pos = i;
                    break;
                }
            }
            $scope.GroupObj.splice(pos, 1);
        } else {
            $scope.GroupObj = $scope.splitObj;
        }
    }
    $scope.fUnitShowClose = function () {
        $scope.chartColorJSON = $scope.GetChartColors($scope.WidgetObjOP[0].value, $scope.UnitShowObOP[0].value);
        $scope.chartName = $scope.chartColorJSON.chartName;
        $scope.chartColorItem = $scope.chartColorJSON.chartColor;
        if ($scope.UnitShowObOP[0].value == "COUNT" || $scope.UnitShowObOP[0].value == "TIME" || $scope.UnitShowObOP[0].value == "AVG_RESP_TIME" || $scope.UnitShowObOP[0].value == "AVG_RESLN_TIME") {
            $scope.isLineChartShow = true;
            $scope.isRespReslnPareto = false;
            $scope.isChartTypeShown = false;
        } else if ($scope.UnitShowObOP[0].value == "EXCEEDING_RESP_KPI" || $scope.UnitShowObOP[0].value == "EXCEEDING_RESLN_KPI") {
            $scope.isRespReslnPareto = true;
            $scope.isLineChartShow = false;
            $scope.isChartTypeShown = true;
        } else {
            $scope.isLineChartShow = false;
            $scope.isRespReslnPareto = false;
            $scope.isRespResln = false;
            $scope.isChartTypeShown = true;
        }
    }
    $scope.closeColorPicker = function (item) {
        alert(item);
    }
    $scope.$on('handlecloseIconModal', function () {
        $scope.chosenIconText = mySharedService.chosenIcon;
    });
    $scope.IntroOptions = {
        steps: [
            {
                element: '#IntroTitle',
                position: 'bottom',
                intro: "We have defined Dashboard as a collection of widget(s) that have a common date range and parameters(filters). For example we can create a dashboard to shows some information for the last 3 months for a particular assignment group. <br/><br/>This introduction will explain to you how you can create a dashboard."
            },
            {
                element: '#IntroWN',
                position: 'bottom',
                intro: "Widget type along with its name is specified in this section."
            },
            {
                element: '#IntroWUI',
                position: 'bottom',
                intro: "Use this to change Widget's Look and feel. You can give widget specific UI setting"
            },
            {
                element: '#IntroFil',
                position: 'bottom',
                intro: "Some common parameters used in the widgets are placed here. You can override dashboard level settings at this level."
            },
        ],
        showStepNumbers: false,
        exitOnOverlayClick: true,
        exitOnEsc: true,
        nextLabel: '<span class="btn btn-sm">NEXT!</span>',
        prevLabel: '<span class="btn btn-sm">Previous</span>',
        doneLabel: 'Thanks'
    };
    $scope.ShouldAutoStart = false;

    function search(key, nameKey, myArray) {
        var pos = -1;
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i][key].toUpperCase() === nameKey.toUpperCase()) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    function searchval(key, nameKey, myArray) {
        var pos = -1;
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i][key] === nameKey) {
                pos = i;
                break;
            }
        }
        return pos;
    }
})