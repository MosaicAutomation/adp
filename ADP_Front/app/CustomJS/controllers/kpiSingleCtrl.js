appDashboard.controller('KPISignleController', ['$scope', '$rootScope', '$window', '$http', 'mySharedService', '$timeout', '$routeParams', 'Notification', '$uibModal', '$location', function ($scope, $rootScope, $window, $http, mySharedService, $timeout, $routeParams, Notification, $uibModal, $location) {
$scope.isFetchingData = false;$scope.BlurClass = "";
	$scope.Resolution = 8;
	$scope.KPICollections = [];
    $scope.KPIAllCollections = [];
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.widgetId = $routeParams.widgetId;


    $scope.CheckUserAuthentication = function () {
        $scope.urlGetUserDashboardSetting = uriApiURL + "api/UserSetting/UserAuthentication?DashBoardID=" + $scope.dashBoardID;

        $http({
                method: 'GET',
                params: {
                    version: getEpocStamp()
                },
                url: $scope.urlGetUserDashboardSetting
            })
            .success(function (response) {
                $scope.AuthUser = angular.fromJson(response);
                $scope.IsValidUser = $scope.AuthUser.IsValidUser;
                if ($scope.IsValidUser == 1) {
                    $scope.UserInfo = mySharedService.UserInfo;
                    $scope.UserID = $scope.UserInfo.UserID;
                    getKAPIValues($scope.columnValue);
                    $scope.GetDashboardSetting();
                } else {
                    mySharedService.makeBgLoad(false);
                }
            }).error(function (response) {

                $location.path('/login');
                mySharedService.makeBgLoad(false);
            });
    }

    $scope.GetDashboardSetting = function () {
        $scope.urlGetUserDashboardSetting = uriApiURL + "api/Dashboard/GetDashboardSettings";

        $scope.urlGetUserDashboardSetting = $scope.urlGetUserDashboardSetting + "?DashBoardID=" + $scope.dashBoardID + "&isSaved=1";

        $http({
                method: 'GET',
                params: {
                    version: getEpocStamp()
                },
                url: $scope.urlGetUserDashboardSetting
            })
            .success(function (response) {
                $scope.Dateformat = "dd-MMM-yyyy";
                $scope.DbHData = angular.fromJson(response)[0];
                $scope.DbHData.ChosenFilter = angular.fromJson(response)[1].ChosenFilter;
                mySharedService.SlaChoose = $scope.DbHData.AllCriticality;
                $scope.DashboardCollection = angular.fromJson(response)[0];
                $scope.Dashboard = {};
                $scope.DashboardIcon = $scope.DbHData.DashboardIcon;
                $scope.dashboardid = $scope.DbHData.DashBoardIID;
                $scope.Dashboard.DashboardName = $scope.DbHData.DashBoardName
                $scope.dashname = $scope.DbHData.DashBoardName;
                $scope.dashid = $scope.DbHData.DashBoardIID;
                $scope.dashicon = $scope.DbHData.DashboardIcon;
                $scope.strtdt = $scope.DbHData.DashboardStartDate;
                $scope.IsCreator = $scope.DbHData.IsCreator;
                mySharedService.DBstrtdt = $scope.strtdt;
                $scope.enddt = $scope.DbHData.DashboardEndDate;
                mySharedService.DBenddt = $scope.enddt;
                $scope.ChosenFilter = $scope.DbHData.ChosenFilter;
                $scope.Privacysetting = $scope.DbHData.IsPrivacySetting;
                mySharedService.Privacysetting = $scope.Privacysetting;

            });
    }

    $scope.hideBg = function () {
        $scope.gridsterOpts.draggable.enabled = false;
        $scope.isReorderData = false;
        $scope.$parent.BlurTop = "";
        $scope.reOrderCls = "";
        for (var i = 0; i < $scope.oldKPICollection.length; i++) {
            $scope.KPICollections[i].col = $scope.oldKPICollection[i].col;
            $scope.KPICollections[i].row = $scope.oldKPICollection[i].row;
            $scope.KPICollections[i].sizeX = $scope.oldKPICollection[i].sizeX;
            $scope.KPICollections[i].sizeY = $scope.oldKPICollection[i].sizeY;
        }
        mySharedService.KPIPositionArr = [];
        $scope.$broadcast('resizeCharts');
    };

    

   

  

   

    
    /*Angular Function Definition Starts*/
    /*Calling Function Definition Starts*/
    if (!sessionStorage.UserInfo) {
        $scope.$on('GotUserDetails', function () {
            /*mySharedService.KPIID is broadcasted KPIID after adding widgets*/
            $scope.CheckUserAuthentication();
        });
    } else {
        mySharedService.makeBgLoad(true);
        $scope.CheckUserAuthentication();

    }


    function getKAPIValues(columnValues) {
        $http({
                method: 'GET',
                cache: false,
                params: {
                    version: getEpocStamp()
                },
                url: uriApiURL + "api/UserSetting/GetKPICollections?PrivacySetting=" + mySharedService.Privacysetting + "&DashBoardID=" + $scope.dashBoardID + "&Resolution=" + $scope.Resolution
            })
            .success(function (response) {

                $scope.KPICollectionsAll = angular.fromJson(response);

                //$scope.WholeDatar = angular.toJson($scope.KPICollectionsAll, true);
                console.log("DASHBOARD LOAD/RELOAD");

                $scope.KPICollectionsHighRaw = $scope.KPICollectionsAll.High;
                $scope.KPICollectionsLowRaw = $scope.KPICollectionsAll.Low

                mySharedService.GroupDetailFilter = $scope.KPICollectionsAll.groups;
                //console.log("PI" + mySharedService.GroupDetailFilter.length);
                //$scope.KPICollectionsHigh  = $scope.KPICollectionsHighRaw.sort(sortFunc);	
                //$scope.KPICollectionsLow  = $scope.KPICollectionsLowRaw.sort(sortFunc);	
                $scope.tempKPIL = $scope.KPICollectionsAll;
                $scope.KPICollections = $scope.tempKPIL;
				if($scope.KPICollections.Cursol.length > 0){
					 angular.forEach($scope.KPICollections.Cursol, function (item) {
						$scope.KPIAllCollections.push(item)
					})
				}
				if($scope.KPICollections.ScoreCard.length > 0){
					 angular.forEach($scope.KPICollections.ScoreCard, function (item) {
						$scope.KPIAllCollections.push(item)
					})
				}
				if($scope.KPICollections.groups.length > 0){
					 angular.forEach($scope.KPICollections.groups, function (gitem) {
						if(gitem.KPI.length > 0){
							 angular.forEach(gitem.KPI, function (gkitem) {
								$scope.KPIAllCollections.push(gkitem)
							})
						}
					})
				}
				

				console.log("KPIAllCollections " + $scope.KPIAllCollections.length)
                if ($scope.KPIAllCollections.length == 0) {
                    $scope.isDisabled = true;
                } else {
                    $scope.isDisabled = false;
                }
        mySharedService.makeBgLoad(false);
            });
    }





	}])