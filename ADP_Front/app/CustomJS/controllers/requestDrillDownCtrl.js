appDashboard.controller('RequestDrillDownController', function ($scope, $window, $http, limitToFilter, $timeout, $routeParams) {
        var vis1;
        $(window).unbind('resize');
        $scope.Resolution = sessionStorage.getItem("Resolution");
        $scope.isSearch = true;
        $scope.isRequestCmpltd = false;
        $scope.UserInfo = angular.fromJson(sessionStorage.UserInfo);
        if (!$scope.Resolution) {
            if ($window.innerWidth > 1440) {
                sessionStorage.setItem("Resolution", "High");
            } else {
                sessionStorage.setItem("Resolution", "Low");
            }
            $scope.Resolution = sessionStorage.getItem("Resolution");
        }
        if ($scope.Resolution == "High") {
            $scope.ResolutionClass = "HighRes";
            $scope.opacityValCom = "";
            $scope.opacityValue = "theme-text";
        } else {

            $scope.opacityValCom = "theme-text";
            $scope.opacityValue = "";
            $scope.ResolutionClass = "LowRes";
        }
        $scope.$on('GetUSer', function (test) {
            $scope.GetRequestDetails(test);
        })
        $scope.ChangeViewMode = function (columnValue) {
            if (columnValue == 8) {
                sessionStorage.setItem("Resolution", "Low")
                $scope.Resolution = "Low";
                $scope.ResolutionClass = "LowRes";
                $scope.opacityValCom = "theme-text";
                $scope.opacityValue = "";
                $scope.plotChartStatusOverall();
                $scope.plotChart();
                $scope.plotChartTimeOpenAnalysis();
                $scope.ploTimeRegChart();
            } else {
                sessionStorage.setItem("Resolution", "High")
                $scope.Resolution = "High";
                $scope.ResolutionClass = "HighRes";
                $scope.opacityValue = "theme-text";
                $scope.opacityValCom = "";
                $scope.plotChartStatusOverall();
                $scope.plotChart();
                $scope.plotChartTimeOpenAnalysis();
                $scope.ploTimeRegChart();
            }

        }
        $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
            createColor();
            var jsonValueData = [],
                jsonKeyValue = [];
            var ColorArray2 = getColorArray(true, "", Bclr, Aclr, $scope.Pie.length);
            var colorObj = {};
            $.each($scope.Pie, function (i, item) {
                var obj = {};
                obj[item.label] = item.value;
                jsonValueData.push(obj);
                jsonKeyValue.push(item.label);
                colorObj[item.label] = ColorArray2[i];
            });
            $scope.KPITClr = Bclr;
            $scope.ATClr = Aclr;
            if ($scope.isTeamResetATClr != "") {
                $scope.isTeamResetATClr = $scope.ATClr;
            } else {
                $scope.isTeamResetATClr = "";
            }
            if ($scope.isStatusResetATClr != "") {
                $scope.isStatusResetATClr = $scope.ATClr;
            } else {
                $scope.isStatusResetATClr = "";
            }
            var ColorArray = getColorArray(true, "", Bclr, Aclr, 2);
            var ColorArray1 = getColorArray(true, "", Bclr, Aclr, 0);
            if ($scope.chartTeams != undefined) {
                $scope.chartTeams.load({
                    colors: {
                        y: ColorArray[0]
                    }
                });
            }
            if ($scope.chartStatus != undefined) {
                $scope.chartStatus.load({
                    colors: {
                        y: ColorArray1[0]
                    }
                });
            }
            if ($scope.chartTime != undefined) {
                $scope.chartTime.load({
                    colors: {
                        y: ColorArray1[0]
                    }
                });
            }
            if ($scope.donutChart != undefined) {
                $scope.donutChart.load({
                    colors: colorObj
                });
            }



        });
        $scope.$on('resizeCharts', function () {
            if ($scope.chartRespReslnExceedPercent != undefined) {
                setTimeout(function () {
                    $scope.chartRespReslnExceedPercent.resize();
                }, 500);
            }
        });
        $scope.isFetchData = true;
        $scope.isSearchVal = false;
        $scope.isTeamReset = "DisableColor";
        $scope.isStatusReset = "DisableColor";
        $scope.isTeamResetATClr = "";
        $scope.isStatusResetATClr = "";
        $scope.GetDropdownValues = function () {
            var url = uriApiURL + 'api/Dashboard/GetRequestIDCollections?Keyword=';
            $scope.getsuggesstion = function (sug) {
                return $http.get(url + sug, {
                        SearchJson: sug
                    })
                    .then(function (response) {
                        var WholeData = angular.fromJson(response.data.m_StringValue);
                        var SearchData = WholeData.SearchData;
                        $scope.isFetchData = false;
                        return limitToFilter(SearchData, 5);
                    })
            };
        }

        $scope.plotChart = function () {

            $timeout(
                function () {
                    var ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, 0);
                    $scope.chartTeams = c3.generate({
                        bindto: ".RequestInfoBarTeam",
                        padding: {
                            top: 5,
                            bottom: 0
                        },
                        data: {
                            json: $scope.BarTeam,
                            keys: {
                                x: 'x',
                                value: ['y'],

                            },
                            type: 'bar',
                            names: {
                                y: 'Time spent'
                            },
                            colors: {
                                y: (ColorArray != undefined ? ColorArray[0] : "")
                            },
                            onclick: function (d, element) {
                                var clickedVal = $scope.chartTeams.categories()[d.index]
                                newJson = "[";
                                $.each($scope.BarAllinOne, function (i, item) {
                                    if (clickedVal == $scope.BarAllinOne[i].team)
                                        newJson = newJson + "{ 'x': '" + item.Request_Status + "','y':" + item.value + "},"
                                });

                                newJson = newJson.substring(0, newJson.length - 1) + "]";
                                $scope.BarTeamOverall = eval(newJson);
                                $scope.selectedTeamVal = "for " + clickedVal;
                                $scope.plotChartStatusOverall();
                                $scope.isStatusReset = "";
                                $scope.isStatusResetATClr = $scope.ATClr;
                            }
                        },
                        tooltip: {
                            format: {
                                title: function (d, i) {
                                    return $scope.chartTeams.categories()[d];
                                },
                                value: function (value, ratio, id) {
                                    return convert2ddhhmm(value, "M");
                                }
                            }
                        },
                        transition: {
                            duration: 500
                        },
                        axis: {
                            x: {
                                type: 'category',
                                rotate: 60,
                                tick: {
                                    format: function (x) {
                                        if ($scope.BarTeam[x].x != undefined) {
                                            var barName = $scope.BarTeam[x].x
                                            if (barName.length > 7) {
                                                return barName.substring(0, 4) + "...";
                                            } else {
                                                return barName;
                                            }
                                        } else {
                                            return "";
                                        }
                                    },
                                }
                            },
                            y: {
                                tick: {
                                    format: function (x) {
                                        return convert2ddhhmm(x, "M")
                                    },
                                    count: 3
                                },
                                padding: {
                                    bottom: 0
                                },
                                min: 0
                            }
                        },
                    })
                }, 0);
        }

        $scope.plotChartStatusOverall = function () {
            var ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, 0);
            $timeout(
                function () {

                    $scope.chartStatus = c3.generate({
                        bindto: ".RequestInfoBarStatus",
                        padding: {
                            top: 5
                        },
                        data: {
                            json: $scope.BarTeamOverall,
                            keys: {
                                x: 'x',
                                value: ['y']
                            },
                            names: {
                                y: 'Time spent'
                            },
                            type: 'bar',
                            colors: {
                                y: (ColorArray != undefined ? ColorArray[0] : "")
                            },
                            onclick: function (d, element) {
                                var clickedVal = $scope.chartStatus.categories()[d.index]
                                newJson = "[";
                                $.each($scope.BarAllinOne, function (i, item) {
                                    if (clickedVal == $scope.BarAllinOne[i].Request_Status)
                                        newJson = newJson + "{ 'x': '" + item.team + "','y':" + item.value + "},"
                                });

                                newJson = newJson.substring(0, newJson.length - 1) + "]";
                                $scope.BarTeam = eval(newJson);

                                $scope.selectedStatusVal = "for Request_Status " + clickedVal;
                                $scope.plotChart();

                                $scope.isTeamReset = "";
                                $scope.isTeamResetATClr = $scope.ATClr;
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',

                                tick: {
                                    rotate: 45,
                                    format: function (x) {
                                        if ($scope.BarTeamOverall[x].x != undefined) {
                                            var barName = $scope.BarTeamOverall[x].x
                                            if (barName.length > 7) {
                                                return barName.substring(0, 4) + "...";
                                            } else {
                                                return barName;
                                            }
                                        } else {
                                            return "";
                                        }
                                    }
                                }

                            },
                            y: {
                                tick: {
                                    format: function (x) {
                                        return convert2ddhhmm(x, "M")
                                    },

                                    count: 5
                                },
                                padding: {
                                    bottom: 0
                                },
                                min: 0
                            }
                        },
                        tooltip: {
                            format: {
                                title: function (d, i) {
                                    return $scope.chartStatus.categories()[d];
                                },
                                value: function (value, ratio, id) {
                                    return convert2ddhhmm(value, "M");
                                }
                            }
                        },
                        transition: {
                            duration: 500
                        }

                    });
                }, 0);
        };

        $scope.ploTimeRegChart = function () {

            $timeout(
                function () {
                    var ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, 0);
                    $scope.chartTime = c3.generate({
                        bindto: ".RequestInfoTimeReg",
                        padding: {
                            top: 5,
                            bottom: 0
                        },
                        data: {
                            json: $scope.BarTime,
                            keys: {
                                x: 'x',
                                value: ['y'],

                            },
                            type: 'bar',
                            names: {
                                y: 'Time spent'
                            },
                            colors: {
                                y: (ColorArray != undefined ? ColorArray[0] : "")
                            }

                        },
                        tooltip: {
                            format: {
                                title: function (d, i) {
                                    return $scope.chartTime.categories()[d];
                                },
                                value: function (value, ratio, id) {
                                    return convert2ddhhmm(value, "M");
                                }
                            }
                        },
                        transition: {
                            duration: 500
                        },
                        axis: {
                            x: {
                                type: 'category',
                                rotate: 60,
                                tick: {
                                    format: function (x) {
                                        if ($scope.BarTime[x].x != undefined) {
                                            var barName = $scope.BarTime[x].x
                                            if (barName.length > 7) {
                                                return barName.substring(0, 4) + "...";
                                            } else {
                                                return barName;
                                            }
                                        } else {
                                            return "";
                                        }
                                    },
                                }
                            },
                            y: {
                                tick: {
                                    format: function (x) {
                                        return convert2ddhhmm(x, "M")
                                    },
                                    count: 3
                                },
                                padding: {
                                    bottom: 0
                                },
                                min: 0
                            }
                        },
                    })
                }, 0);
        }

        $scope.TeamReset = function () {
            if (!($scope.isTeamReset == "DisableColor")) {
                $scope.BarTeam = $scope.oldBarTeam;
                $scope.plotChart();
                $scope.selectedStatusVal = $scope.oldselectedStatusVal;
                $scope.isTeamReset = "DisableColor";
                $scope.isTeamResetATClr = "";
            }
        }

        $scope.StatusReset = function () {
            if (!($scope.isStatusReset == "DisableColor")) {
                $scope.BarTeamOverall = $scope.oldBarTeamOverall;
                $scope.plotChartStatusOverall();
                $scope.selectedTeamVal = $scope.oldselectedTeamVal;
                $scope.isStatusReset = "DisableColor";
                $scope.isStatusResetATClr = "";
            }
        }

        $scope.plotChartTimeOpenAnalysis = function () {

            $timeout(
                function () {
                    var jsonValueData = [];
                    $scope.jsonKeyValue = [];
                    $scope.ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, $scope.Pie.length);
                    $scope.colorObj = {};
                    $.each($scope.Pie, function (i, item) {
                        var obj = {};
                        obj[item.label] = item.value;
                        jsonValueData.push(obj);
                        $scope.jsonKeyValue.push(item.label);
                        $scope.colorObj[item.label] = $scope.ColorArray[i];

                    });
                    var tempTtl = 0;
                    for (x in $scope.Pie) {
                        tempTtl += $scope.Pie[x]['value'];
                    }
                    $scope.tempTtl = convert2ddhhmm(tempTtl, "M");
                    $scope.donutChart = c3.generate({
                        bindto: ".RequestInfoOpenTime",
                        data: {
                            json: jsonValueData,
                            keys: {
                                value: $scope.jsonKeyValue
                            },
                            onmouseover: function (d) {
                                document.getElementsByClassName('c3-chart-arcs-title')[2].textContent = convert2ddhhmm(d.value, "M");

                            },
                            onmouseout: function (d) {
                                var gg = 1;
                                document.getElementsByClassName('c3-chart-arcs-title')[2].textContent = $scope.tempTtl;
                                //REQ0687650,REQ0504456
                            },
                            type: 'donut',
                            colors: $scope.colorObj
                        },
                        donut: {
                            title: $scope.tempTtl
                        },
                        tooltip: {
                            format: {
                                title: function (d, i) {
                                    return "Value";
                                },
                                value: function (value, ratio, id) {
                                    return convert2ddhhmm(value, "M");
                                }
                            }
                        }
                    });

                }, 0);
        };

        $scope.GetDropdownValues();

        $scope.onSelect = function (item) {
            $scope.isLoadCmpltd = false;
            $scope.isRequestCmpltd = true;
            $scope.GetRequestDetails(item);
        };

        $scope.GetRequestDetails = function (item) {
            $http({
                method: 'POST',
				params: {version: getEpocStamp()},
                url: uriApiURL + 'api/Dashboard/GetRequestDeepDive?Keyword=' + item,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })

            .success(function (response) {
                $scope.isRequestCmpltd = false;
                $scope.WholeData = angular.fromJson(response);
                $scope.ReqId = $scope.WholeData.BasicInfo[0].REQUEST_ID;
                $scope.ReqType = $scope.WholeData.BasicInfo[0].REQUEST_TYPE;
                $scope.Request_Priority = $scope.WholeData.BasicInfo[0].REQUEST_PRIORITY;
                $scope.Request_Status = $scope.WholeData.BasicInfo[0].STATUS + "(" + $scope.WholeData.BasicInfo[0].STATUS_STATE + ")";
                $scope.Summary = $scope.WholeData.BasicInfo[0].SUMMARY;
                $scope.oldBarTeam = $scope.WholeData.BarTeam;
                $scope.BarTeam = $scope.WholeData.BarTeam;
                $scope.oldBarTeamOverall = $scope.WholeData.BarStatus;
                $scope.BarTeamOverall = $scope.WholeData.BarStatus;
                $scope.TimelineMajor = $scope.WholeData.TimeLineMajor;
                $scope.TimelineMinor = $scope.WholeData.TimeLineMinor;
                $scope.TimelineAll = $scope.WholeData.TimeLineMajor.concat($scope.WholeData.TimeLineMinor)
                $scope.BarAllinOne = $scope.WholeData.BarAllinOne;
                $scope.BarTime = $scope.WholeData.BarTime;
                $scope.oldselectedStatusVal = "(Overall)";
                $scope.oldselectedTeamVal = "(Overall)"
                $scope.selectedStatusVal = "(Overall)";
                $scope.selectedTeamVal = "(Overall)";
                $scope.Pie = $scope.WholeData.Pie;
                $scope.plotChartStatusOverall();
                $scope.plotChart();
                $scope.plotChartTimeOpenAnalysis();
                $scope.ploTimeRegChart();
                setTimeout(function () {
                    drawVisualization($scope.WholeData.TimeLineMajor);
                }, 0);
                $scope.isSearchVal = true;
                $scope.isLoadCmpltd = true;
                $scope.isTeamReset = "DisableColor";
                $scope.isStatusReset = "DisableColor";
                $scope.isStatusResetATClr = "";
                $scope.isTeamResetATClr = "";
                $scope.spantimelineHighlightGroup = "";
                $scope.spantimelineHighlightPriority = "";
                $scope.spantimelineHighlightStatus = "";
                $scope.spantimelineHighlightUser = "DisableColor";
                $scope.spantimelineHighlightEvents = "DisableColor";
                $scope.spantimelineHighlightLog = "DisableColor";
                $scope.spantimelineHighlightTime = "DisableColor";
                $scope.timelineShowAll = "fa-circle-o";
                $scope.timelineShowMajor = "fa-dot-circle-o";
                $scope.timelineShowMinor = "fa-circle-o";
                $scope.timelineHighlightGroup = "fa-square-o";
                $scope.timelineHighlightPriority = "fa-square-o";
                $scope.timelineHighlightStatus = "fa-square-o";
                $scope.timelineHighlightEvents = "fa-square-o";
                $scope.timelineHighlightUser = "fa-square-o";
                $scope.timelineHighlightLog = "fa-square-o";
                $scope.timelineHighlightTime = "fa-square-o";
            });
        };

        $scope.spantimelineShowAll = function () {
            myRadioReset();
            $scope.timelineShowAll = "fa-dot-circle-o";
            $scope.spantimelineHighlightGroup = "";
            $scope.spantimelineHighlightPriority = "";
            $scope.spantimelineHighlightStatus = "";
            $scope.spantimelineHighlightUser = "";
            $scope.spantimelineHighlightEvents = "";
            $scope.spantimelineHighlightLog = "";
            $scope.spantimelineHighlightTime = "";
            redrawVisualization($scope.TimelineAll);
        }

        $scope.spantimelineShowMajor = function () {
            myRadioReset();
            $scope.timelineShowMajor = "fa-dot-circle-o";
            $scope.spantimelineHighlightGroup = "";
            $scope.spantimelineHighlightPriority = "";
            $scope.spantimelineHighlightStatus = "";
            redrawVisualization($scope.TimelineMajor);
        }

        $scope.spantimelineShowMinor = function () {
            myRadioReset();
            $scope.timelineShowMinor = "fa-dot-circle-o";
            $scope.spantimelineHighlightUser = "";
            $scope.spantimelineHighlightEvents = "";
            $scope.spantimelineHighlightLog = "";
            $scope.spantimelineHighlightTime = "";
            redrawVisualization($scope.TimelineMinor);
        }

        $scope.spantimelineHighlightGroups = function () {
            var tileColor = $scope.ATClr;
            if (!($scope.spantimelineHighlightGroup == "DisableColor")) {
                if ($scope.timelineHighlightGroup == "fa-check-square-o") {
                    $scope.timelineHighlightGroup = "fa-square-o";
                    $('.group > div.vis-item-content').removeClass("amber blue brown cobalt crimson cyan magenta lime indigo green emerald mango mauve olive orange pink red sienna steel teal violet yellow theme-box");
                } else {
                    $scope.timelineHighlightGroup = "fa-check-square-o";
                    $('.group > div.vis-item-content').addClass(tileColor + ' theme-box');
                }
            }
        }

        $scope.spantimelineHighlightPrioritys = function () {
            var tileColor = $scope.ATClr;
            if (!($scope.spantimelineHighlightPriority == "DisableColor")) {
                if ($scope.timelineHighlightPriority == "fa-check-square-o") {
                    $scope.timelineHighlightPriority = "fa-square-o";
                    $('.priority > div.vis-item-content').removeClass("amber blue brown cobalt crimson cyan magenta lime indigo green emerald mango mauve olive orange pink red sienna steel teal violet yellow theme-box");
                } else {
                    $scope.timelineHighlightPriority = "fa-check-square-o";
                    $('.priority > div.vis-item-content').addClass(tileColor + ' theme-box');
                }
            }
        }

        $scope.spantimelineHighlightStatuss = function () {
            var tileColor = $scope.ATClr;
            if (!($scope.spantimelineHighlightStatus == "DisableColor")) {
                if ($scope.timelineHighlightStatus == "fa-check-square-o") {
                    $scope.timelineHighlightStatus = "fa-square-o";
                    $('.status > div.vis-item-content').removeClass("amber blue brown cobalt crimson cyan magenta lime indigo green emerald mango mauve olive orange pink red sienna steel teal violet yellow theme-box");
                } else {
                    $scope.timelineHighlightStatus = "fa-check-square-o";
                    $('.status > div.vis-item-content').addClass(tileColor + ' theme-box');
                }
            }
        }

        $scope.spantimelineHighlightEventss = function () {
            var tileColor = $scope.ATClr;
            if (!($scope.spantimelineHighlightEvents == "DisableColor")) {
                if ($scope.timelineHighlightEvents == "fa-check-square-o") {
                    $scope.timelineHighlightEvents = "fa-square-o";
                    $('.mail > div.vis-item-content').removeClass("amber blue brown cobalt crimson cyan magenta lime indigo green emerald mango mauve olive orange pink red sienna steel teal violet yellow theme-box");
                } else {
                    $scope.timelineHighlightEvents = "fa-check-square-o";
                    $('.mail > div.vis-item-content').addClass(tileColor + ' theme-box');
                }
            }
        }

        $scope.spantimelineHighlightUsers = function () {
            var tileColor = $scope.ATClr;
            if (!($scope.spantimelineHighlightUser == "DisableColor")) {
                if ($scope.timelineHighlightUser == "fa-check-square-o") {
                    $scope.timelineHighlightUser = "fa-square-o";
                    $('.assignee > div.vis-item-content').removeClass("amber blue brown cobalt crimson cyan magenta lime indigo green emerald mango mauve olive orange pink red sienna steel teal violet yellow theme-box");
                } else {
                    $scope.timelineHighlightUser = "fa-check-square-o";
                    $('.assignee > div.vis-item-content').addClass(tileColor + ' theme-box');
                }
            }
        }

        $scope.spantimelineHighlightLogs = function () {
            var tileColor = $scope.ATClr;
            if (!($scope.spantimelineHighlightLog == "DisableColor")) {
                if ($scope.timelineHighlightLog == "fa-check-square-o") {
                    $scope.timelineHighlightLog = "fa-square-o";
                    $('.log > div.vis-item-content').removeClass("amber blue brown cobalt crimson cyan magenta lime indigo green emerald mango mauve olive orange pink red sienna steel teal violet yellow theme-box");
                } else {
                    $scope.timelineHighlightLog = "fa-check-square-o";
                    $('.log > div.vis-item-content').addClass(tileColor + ' theme-box');
                }
            }
        }

        $scope.spantimelineHighlightTimes = function () {
            var tileColor = $scope.ATClr;
            if (!($scope.spantimelineHighlightTime == "DisableColor")) {
                if ($scope.timelineHighlightTime == "fa-check-square-o") {
                    $scope.timelineHighlightTime = "fa-square-o";
                    $('.time > div.vis-item-content').removeClass("amber blue brown cobalt crimson cyan magenta lime indigo green emerald mango mauve olive orange pink red sienna steel teal violet yellow theme-box");
                } else {
                    $scope.timelineHighlightTime = "fa-check-square-o";
                    $('.time > div.vis-item-content').addClass(tileColor + ' theme-box');
                }
            }
        }
        if ($routeParams.RequestId != undefined) {
            $scope.searchval = $routeParams.RequestId;
            $scope.GetRequestDetails($routeParams.RequestId);
        }

        function myRadioReset() {

            $scope.spantimelineHighlightGroup = "DisableColor";
            $scope.spantimelineHighlightPriority = "DisableColor";
            $scope.spantimelineHighlightStatus = "DisableColor";
            $scope.spantimelineHighlightUser = "DisableColor";
            $scope.spantimelineHighlightEvents = "DisableColor";
            $scope.spantimelineHighlightLog = "DisableColor";
            $scope.spantimelineHighlightTime = "DisableColor";

            $scope.timelineShowAll = "fa-circle-o";
            $scope.timelineShowMajor = "fa-circle-o";
            $scope.timelineShowMinor = "fa-circle-o";

            $scope.timelineHighlightGroup = "fa-square-o";
            $scope.timelineHighlightPriority = "fa-square-o";
            $scope.timelineHighlightStatus = "fa-square-o";
            $scope.timelineHighlightEvents = "fa-square-o";
            $scope.timelineHighlightUser = "fa-square-o";
            $scope.timelineHighlightLog = "fa-square-o";
            $scope.timelineHighlightTime = "fa-square-o";
        }


        function getSelectedRow() {
            var row = undefined;
            var sel = vis1.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    row = sel[0].row;
                }
            }
            return row;
        }

        function drawVisualization(data1) {
            // Create a JSON data table
            eval1 = eval(data1);
            // specify options
            var options = {
                'width': '100%',
                'minHeight': 250,
                'editable': false,
                'type': 'point'
            };

            // Instantiate our timeline object.
            if (vis1 != null)
            {
                vis1.destroy()
            }
            vis1 = new vis.Timeline(document.getElementById('mytimeline'), eval1, options);
            vis1.on('changed', function (properties) {
                createColor();
            });
            vis1.on('select', function (properties) {
                onselect();
            });
        }

        function redrawVisualization(data1) {
            vis1.setData({
                items: data1
            });
            vis1.redraw();
            vis1.fit();
        }


        function createColor() {
            $(".timeline-event").each(function () {
                $(this).addClass($scope.ATClr + " .theme-box");

            });
            $('.mytimeline .theme-box,.mytimeline .vis-dot').removeClass("amber blue brown cobalt crimson cyan magenta lime indigo green emerald mango mauve olive orange pink red sienna steel teal violet yellow").addClass($scope.ATClr + " theme-box");
        }

        var onselect = function (event) {
            var row = getSelectedRow();
            //if (row != undefined) {
            //    if (selectedDisplay == 1 && $scope.TimelineAll[row].className == 'mail') {
            //        window.open("http://servicedesk.kitenet.com/arsys/forms/kbnpdrem40/sd-shared-log-email/email/?format=html&eid=" + $scope.TimelineAll[row].idenitifer);
            //    }
            //    else if (selectedDisplay == 1 && $scope.TimelineAll[row].className == 'log') {
            //        window.open("http://servicedesk.kitenet.com/arsys/forms/kbnpdrem40/sd-shared-log-email/log/?format=html&eid=" + $scope.TimelineAll[row].idenitifer);
            //    }
            //    if (selectedDisplay == 3 && $scope.TimelineMinor[row].className == 'mail') {
            //        window.open("http://servicedesk.kitenet.com/arsys/forms/kbnpdrem40/sd-shared-log-email/email/?format=html&eid=" + $scope.TimelineMinor[row].idenitifer);
            //    }
            //    else if (selectedDisplay == 3 && $scope.TimelineMinor[row].className == 'log') {
            //        window.open("http://servicedesk.kitenet.com/arsys/forms/kbnpdrem40/sd-shared-log-email/log/?format=html&eid=" + $scope.TimelineMinor[row].idenitifer);
            //    }
            //}
        };

    })