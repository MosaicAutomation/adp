﻿appDashboard.controller('AtSettingController', function ($scope,$routeParams, $http, $location, limitToFilter, Notification, $window) {
    /*AdminController related to the _Admin.html for Admin activities */
    /*Variable Declaration begins*/
    $scope.AssessmentId = $routeParams.AssessmentId;
    $scope.ATSettingGrid = [
        {
            id: 1,
            Icon: "fa-ticket",
            name: "BaseKPIColMap",
            Header: "Base KPI Column Mapping",
            sizeX: 1,
            sizeY: 1,
            row: 0,
            col: 0
        },
        {
            id: 2,
            Icon: "fa-circle",
            name: "MasterColMap",
            Header: "Master Column Mapping",
            sizeX: 1,
            sizeY: 1,
            row: 0,
            col: 1
        },
        {
            id: 3,
            Icon: "fa-save",
            name: "StopWordData",
            Header: "Pre-Process - Stop Word Setting",
            sizeX: 1,
            sizeY: 1,
            row: 1,
            col: 2
        }
    ];
    /*Variable Declaration ends*/
    /*Function Declaration and definition begins*/
    /*Main Function to Navigate either to BaseKPI Column Mapping section or to the Maseter Column Mapping section*/
    $scope.navToBasicSet = function (id) {
        if (id == 1) {
            $location.path("/BaseKPIColMap/" + $scope.AssessmentId);
        } else if (id == 2) {
            $location.path("/MasterColMap/" + $scope.AssessmentId);
        }
        else {
            $location.path("/PreProcData/" + $scope.AssessmentId)
        }
    }
    /*Function Declaration and definition begins*/
})