appDashboard.controller('RespReslnExceedPercentCtrl', function ($scope, $routeParams, $filter, $http, $uibModal, mySharedService, $element, $timeout, Notification) {
        $scope.isLoadCmpltd = false;
        $scope.testData;
        $scope.toggle1 = false;
        $scope.datatoshow = true;
        $scope.dashBoardID = $routeParams.dashboardId;
        $scope.KPIIDs = $element.parent().parent().parent().attr("data-kpiid");
        $scope.AssessmentId = $routeParams.AssessmentId;
			$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
		
		
        $scope.getWidgetSize = function () {
            var sizeval = getKPISize($scope.KPIIDs, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
            $scope.SizeXVal = sizeval[0];
            $scope.SizeYVal = sizeval[1];
        }
		
		$scope.openWidgetError = function (response) {		
			 Notification.error({
					verticalSpacing: 60,
					message: $scope.IsShowErrorMsg,
					title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
			});	
		};
		
        $scope.getWidgetSize();
        $scope.$on('resizeCharts', function () {
            if ($scope.chartRespReslnExceedPercent != null) {
                setTimeout(function () {
                    $scope.chartRespReslnExceedPercent.resize();
                }, 500);
            }
        });
        $scope.plotChart = function (type) {
		 $scope.selectPriorityName = type;
            $scope.selectPriority = [type];
            $scope.chartData = $filter('filter')($scope.RespReslnExceedData, type.toUpperCase());
            $scope.StackedData = [['Exceeding', 'NonExceeding']];
            if ($scope.isGrouped) {
                $scope.StackedData = [];
            }
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, $scope.KPITClr, $scope.ATClr, 3);
            $scope.chartRespReslnExceedPercent = c3.generate({
                bindto: "." + $scope.KPIIDZoom + "RespReslnExceedPercent",
                padding: {
                    top: 5,
                    bottom: 0
                },
                data: {
                    json: $scope.chartData,
                    keys: {
                        x: 'PriorityMonthDate',

                        value: ['Exceeding', 'NonExceeding', 'ExceedingPerc'],
                    },
                    axes: {
                        'ExceedingPerc': 'y2'
                    },
                    type: 'bar',
                    types: {
                        'ExceedingPerc': 'spline'
                    },
                    groups: $scope.StackedData,
                    onclick: function (e, event) {
                        if (e.id != "ExceedingPerc") {
                            $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
                                //+ "&IsMonth=" + "1" + "&Request_Priority=" + $scope.selectPriority + "&YearMonthWeek=" + $scope.chartData[e.x]['PriorityMonthDate'] + "&FirstGenericData=" + e.id
                            var obj = new Object();
                            obj.IsMonth = "1";
                            obj.Request_Priority = $scope.selectPriority[0];
                            obj.YearMonthWeek = $scope.chartData[e.x]['PriorityMonthDate'];
                            obj.FirstGenericData = e.id;
                            var data = JSON.stringify(obj);
                            mySharedService.openGridModal('lg', $scope.url, data);

                        }
                    },
                    names: {
                        Exceeding: 'Request(s) Exceeding SLA',
                        NonExceeding: 'Request(s) Not Exceeding SLA',
                        ExceedingPerc: 'SLA Adherence(%)',
                    },
                    colors: {
                        Exceeding: $scope.ColorArray[0],
                        NonExceeding: $scope.ColorArray[1],
                        ExceedingPerc: $scope.ColorArray[2]
                    }
                },
                tooltip: {
                    format: {
                        title: function (d, i) {
                            return $scope.chartRespReslnExceedPercent.categories()[d];
                        },
                        value: function (value, ratio, id) {
                            return value.toFixed(2);
                        }
                    }
                },
                transition: {
                    duration: 500
                },
                axis: {
                    x: {
                        type: 'category',
                        tick: {
                            format: function (x) {
                                if ($scope.chartData[x] != undefined) {
                                    var barName = $scope.chartData[x].PriorityMonthDate
                                    if (barName.length > 7) {
                                        return barName.substring(0, 4) + "...";
                                    } else {
                                        return barName;
                                    }
                                } else {
                                    return "";
                                }
                            },
                        }
                    },

                    y: {
                        tick: {
                            
							format: function (x) {
								 return d3.round(x);
							}
                        },
                        padding: {
                            bottom: 0
                        },
                        min: 0
                    },
                    y2: {
                        show: true,
						min: 0,
						max: 100,
                        tick: {
							values:[0,25,50,75,100],    
							format: function (x) {
                                 return x+"%";
                            },
                            culling: {
                                max: 2 // the number of tick texts will be adjusted to less than this value
                            }
                        },
                        padding: {
                            bottom: 0
                        }
                    }
                }
            });
        };
        $scope.getData = function () {

            if (mySharedService.IsZoom) {
                $scope.KPIID = mySharedService.KPIID;
                $scope.IsZoom = mySharedService.IsZoom;
                $scope.KPIIDZoom = $scope.KPIID + "zoomed"
                $timeout(function () {
                    $scope.plotChart($scope.selectPriority[0])
                }, 10);
                $scope.$emit("ZoomDataDone");
            } else {
                $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
				$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
                $scope.IsZoom = false;
                $scope.KPIIDZoom = $scope.KPIID;

                $scope.type = $element.parent().attr("type");
                if ($scope.type == "5") {
                    $scope.url = "api/KPI/GetDataKPIResponseExceeding?DashboardId=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "";
                } else if ($scope.type == "6") {
                    $scope.url = "api/KPI/GetDataKPIResolveExceeding?DashboardId=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "";
                }
                $http({
                        method: 'GET',
						params: {version: getEpocStamp()},
						cache: mySharedService.KPIboardCache,
                        url: uriApiURL + $scope.url,
                        data: {
                            "kpiname": "test"
                        }
                    })
                    .success(function (response) {
                        $scope.isLoadCmpltd = true;
                        $scope.WholeData = angular.fromJson(response);
                        $scope.RespReslnExceedData = $scope.WholeData.IncomingVSCompleted;
                        $scope.prioritySwatch = [];
                        if ($scope.WholeData.Request_Priority != null) {
                            $scope.toggle1 = false;
                            $scope.datatoshow = true;
                            $scope.prioritiesArr = $scope.WholeData.Request_Priority.split(",");
                            for (var i in $scope.prioritiesArr) {
                                $scope.prioritySwatch.push({
                                    'name': $scope.prioritiesArr[i],
                                    'value': $scope.prioritiesArr[i],
                                    'opacity': 'theme-text'
                                });
                            }
                            $scope.prioritySwatch[0].opacity = "";
                            $scope.kpiname = $scope.WholeData["KPIName"];
                            $scope.kpiType = "kpiType12";
                            $scope.period = "Jun-14 to Jun-15";
                            $scope.selectPriority = [$scope.prioritySwatch[0].value];
                            $scope.ColorArray = $scope.WholeData.ChartColor.split(",");
                            $scope.DBColorArray = $scope.ColorArray;
                            $scope.isGrouped = $scope.WholeData.IsGrouped;
                            $scope.IsUseTheme = $scope.WholeData.IsUseTheme;
                            $timeout(function () {
                                $scope.plotChart($scope.selectPriority[0])
                            }, 10);
                            if (mySharedService.IsZoom) {
                                $scope.$emit("ZoomDataDone");
                            }

                        } else {
                            $scope.toggle1 = true;
                            $scope.datatoshow = true;
                        }


                    }).error(function (response) {
										
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= response;
						
					});
            }
        }
        $scope.getData();
        $scope.$on('handleaaddUpdateWidgetDataBroadcast', function (kpiValue) {
            /*kpiValue is KPIID*/
            if (mySharedService.KPIID == 5) {
                $scope.getData();
            } else if (mySharedService.KPIID == 6) {
                $scope.getData();
            }
        });
        $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, Bclr, Aclr, 3);
            $scope.chartRespReslnExceedPercent.load({
                colors: {
                    Exceeding: $scope.ColorArray[0],
                    NonExceeding: $scope.ColorArray[1],
                    ExceedingPerc: $scope.ColorArray[2]
                }
            });
        });

        $scope.expandChart = function () {
            $scope.animationsEnabled = true;
            if ($scope.type == "5") {
                $scope.KPI = {
                    idNum: 5,
                    Id: $scope.KPIID
                }
            } else if ($scope.type == "6") {
                $scope.KPI = {
                    idNum: 6,
                    Id: $scope.KPIID
                }
            }
            mySharedService.OpenZoomModal($scope);
        }
    })