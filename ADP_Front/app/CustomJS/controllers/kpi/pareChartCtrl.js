
appDashboard.controller('ParetoChartController', function ($scope, $routeParams, $timeout, $filter, $http, mySharedService, $element, Notification) {
        $scope.isLoadCmpltds = false;
		$scope.IsShowError= false;
        $scope.isThirdRow = false;
        $scope.toggle1 = false;
        $scope.toggle2 = false;
        $scope.datatoshow = false;
        $scope.AssessmentId = $routeParams.AssessmentId;
        $scope.testData;
        $scope.SpinnerData = ($scope.SpinnerData == undefined) ? "6N" : $scope.SpinnerData;
        $scope.whichColorperc = "white";
        $scope.whichColorNum = "white";
        $scope.PriorityType = "ALL";
        $scope.MakePriorityBasedData = {};
        $scope.MakePriorityBasedNumber = {};
        $scope.IsRedrawZoom = false;
        $scope.initialVal = ($scope.initialVal == undefined) ? 6 : $scope.initialVal;
        $scope.numberVal = ($scope.numberVal == undefined) ? 6 : $scope.numberVal;
        $scope.numberValNew = ($scope.numberValNew == undefined) ? 6 : $scope.numberValNew;
        $scope.dashBoardID = $routeParams.dashboardId;
        $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
		$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
        $scope.$on('resizeCharts', function () {
            if ($scope.chartPareto != null) {
                setTimeout(function () {
                    $scope.chartPareto.resize();
                }, 500);
            }
        });
        $scope.isTimeChart = false;
        $scope.ParData = [];
        $scope.getWidgetSize = function () {
            var sizeval = getKPISize($scope.KPIID, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
            $scope.SizeXVal = sizeval[0];
            $scope.SizeYVal = sizeval[1];
        }
		
		$scope.openWidgetError = function (response) {		
			 Notification.error({
					verticalSpacing: 60,
					message: $scope.IsShowErrorMsg,
					title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
			});	
		};
				
        $scope.GetTableValues = function (chartData) {
            if ($scope.chosenUnit == "RESOLVE_EXCEEDING_PERC" || $scope.chosenUnit == "RESPONSE_EXCEEDING_PERC" || $scope.chosenUnit == "EXCEEDING_RESLN_KPI" || $scope.chosenUnit == "EXCEEDING_RESP_KPI") {
                $scope.RowValue1 = "Request(s) Exceeding SLA"
                $scope.RowValue2 = "Request(s) Not Exceeding SLA"
                $scope.RowValue3 = $scope.Exceeding;
                $scope.RowData1 = [];
                $scope.RowData2 = [];
                $scope.RowData3 = [];

                for (var i = 0; i < chartData.length; i++) {
                    $scope.RowData1.push({
                        "RowVal1": chartData[i].Exceeding
                    });
                    $scope.RowData2.push({
                        "RowVal2": chartData[i].NonExceeding
                    })
                    $scope.RowData3.push({
                        "RowVal3": chartData[i].ExceedingPerc
                    })
                }
            } else if ($scope.chosenUnit == "INCOMING_REQUEST" || $scope.chosenUnit == "COMPLETE_REQUEST" || $scope.chosenUnit == "BACKLOG_REQUEST" || $scope.chosenUnit == "REOPEN_COUNT") {
                $scope.RowValue1 = "New"
                $scope.RowValue2 = "Backlog"
                $scope.RowValue3 = "Completed"
                $scope.RowData1 = [];
                $scope.RowData2 = [];
                $scope.RowData3 = [];

                for (var i = 0; i < chartData.length; i++) {
                    $scope.RowData1.push({
                        "RowVal1": chartData[i].Incoming
                    });
                    $scope.RowData2.push({
                        "RowVal2": chartData[i].Backlog
                    })
                    $scope.RowData3.push({
                        "RowVal3": chartData[i].Completed
                    })
                }
             
			 } else if ($scope.chosenUnit == "REOPEN_COUNT") {
                $scope.RowValue1 = "Reopen "
                $scope.RowData1 = [];

                for (var i = 0; i < chartData.length; i++) {
                    $scope.RowData1.push({
                        "RowVal1": chartData[i].ReOpenCount
                    });
                }
            } 
			
			else {
                $scope.isThirdRow = true;
                $scope.RowData1 = [];
                $scope.RowData2 = [];
                if ($scope.chosenUnit == "TIME" || $scope.chosenUnit == "COUNT") {
                    if ($scope.chosenUnit == "TIME") {
                        $scope.RowValue2 = "No. Of Requests"
                        $scope.RowValue1 = "Time Spent"
                        for (var i = 0; i < chartData.length; i++) {
                            chartData[i].PriorityMonthDate = chartData[i].BarName;
                            $scope.RowData2.push({
                                "RowVal2": chartData[i].LineValue
                            });
                            $scope.RowData1.push({
                                "RowVal1": convert2ddhhmm(chartData[i].BarValue, 'm')
                            })
                        }
                    } else {
                        $scope.RowValue1 = "No. Of Requests"
                        $scope.RowValue2 = "Time Spent"
                        for (var i = 0; i < chartData.length; i++) {
                            chartData[i].PriorityMonthDate = chartData[i].BarName;
                            $scope.RowData1.push({
                                "RowVal1": chartData[i].BarValue
                            });
                            $scope.RowData2.push({
                                "RowVal2": convert2ddhhmm(chartData[i].LineValue, 'm')
                            })

                        }
                    }

                } else {
                    $scope.RowValue1 = "No. Of Requests"
                    $scope.RowValue2 = "Average Response Time"
                    for (var i = 0; i < chartData.length; i++) {
                        chartData[i].PriorityMonthDate = chartData[i].BarName;
                        $scope.RowData1.push({
                            "RowVal1": chartData[i].BarValue
                        });
                        $scope.RowData2.push({
                            "RowVal2": convert2ddhhmm(chartData[i].LineValue, 'm')
                        })
                    }
                }
            }
        }
        $scope.initialChart = function (type) {
            $scope.makeLegend();
            $scope.toggle = false;
            for (var k = 0; k < ParetoData.length; k++) {
                if (ParetoData[k].KPIID == $scope.KPIID) {
                    $scope.ParData = ParetoData[k].ParetoVal
                }
            }
            if (type == undefined) {
                $scope.chartData = $scope.ParData;
            } else if ((type == "ALL" || type == "TOTAL") && (($scope.chosenUnit == "TIME") || ($scope.chosenUnit == "COUNT"))) {
                $scope.chartData = $scope.ParData;
            } else {
                $scope.chartData = $filter('filter')($scope.ParData, type.toUpperCase());
            }
            $scope.GetTableValues($scope.chartData);
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, $scope.KPITClr, $scope.ATClr, 2);

            $scope.chartPareto = c3.generate({
                bindto: "." + $scope.KPIIDZoom + "ParetoChrt",
                padding: {
                    top: 5,
                    bottom: 0
                },
                axis: {
                    x: {
                        type: 'category', // this needed to load string x value

                        tick: {
                            format: function (x) {
                                if ($scope.items[x] != undefined) {
                                    var barName = $scope.items[x].BarName
                                    if (barName.length > 7) {
                                        return barName.substring(0, 4) + "...";
                                    } else {
                                        return barName;
                                    }
                                } else {
                                    return "";
                                }
                            },
                        }
                    },
                    y: {
                        tick: {
                            format: function (x) {
                                if ($scope.chosenUnit == "TIME") {
                                    return convert2ddhhmm(x, 'm')
                                } else {
                                    return d3.round(x)
                                }
                            },
                            count: 5
                        },
                        padding: {
                            bottom: 0
                        },
                        min: 0
                    },
                    y2: {
                        show: true,
                        tick: {
                            format: function (x) {
                                if ($scope.chosenUnit != "TIME") {
                                    return convert2ddhhmm(x, 'm')
                                } else {
                                    return d3.round(x)
                                }
                            },
                            culling: {
                                max: 2 // the number of tick texts will be adjusted to less than this value
                            },
                            count: 5
                        },
                        padding: {
                            bottom: 0
                        },
                        min: 0
                    }
                },
                transition: {
                    duration: 500
                },
                data: {
                    json: $scope.items,
                    keys: {
                        x: 'BarName',
                        value: ['BarValue', 'LineValue'],
                    },
                    type: 'bar',
                    types: {
                        'LineValue': 'spline'
                    },
                    names: $scope.LegenNames,
                    axes: {
                        'LineValue': 'y2'
                    },
                    onclick: function (e, event) {
                      $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
                    // + "&Request_Priority=" + $scope.items[e.x].Request_Priority + "&FirstGenericData=" + encodeURIComponent($scope.items[e.x]['BarName'])
                    var obj = new Object();
                    obj.Request_Priority = $scope.items[e.x].Request_Priority;
                    obj.FirstGenericData = $scope.items[e.x]['BarName'];
                    var data = JSON.stringify(obj);
                    mySharedService.openGridModal('lg', $scope.url, data);                        
		 

                        //event.stopPropagation()
                    },
                    colors: {
                        BarValue: $scope.ColorArray[0],
                        LineValue: $scope.ColorArray[1]
                    }
                },
                tooltip: {
                    format: {
                        title: function (d, i) {
                                return $scope.chartPareto.categories()[d];
                            }
                            //            value: d3.format(',') // apply this format to both y and y2
                    }
                },
                legend: {
                    position: 'bottom'
                }
            });
            $scope.toggle = true;
        }
        $scope.plotIncVsCompChart = function (type) {
			$scope.selectPriorityName = type;
            $scope.StackedData = [['Incoming', 'Backlog']];
            if ($scope.isGrouped) {
                $scope.StackedData = [];
            }
            for (var k = 0; k < ParetoData.length; k++) {
                if (ParetoData[k].KPIID == $scope.KPIID) {
                    $scope.ParData = ParetoData[k].ParetoVal
                }
            }
            $scope.chartData = $filter('filter')($scope.ParData, type.toUpperCase());
            $scope.GetTableValues($scope.chartData);
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, $scope.KPITClr, $scope.ATClr, 3);
            $scope.chartPareto = c3.generate({
                bindto: "." + $scope.KPIIDZoom + "ParetoChrt",

                data: {
                    json: $scope.items,
                    keys: {
                        x: 'PriorityMonthDate',
                        value: ['Incoming', 'Backlog', 'Completed'],
                    },
                    type: 'bar',
                    types: {
                        'Completed': 'spline'
                    },
                    groups: $scope.StackedData,
                    onclick: function (e, event) {
                        if (e.id == 'Incoming') {
                            $scope.id = "New"
                        } else {
                            $scope.id = e.id;
                        }
                       $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
                    //+ "&Request_Priority=" + $scope.items[e.x].Request_Priority + "&FirstGenericData=" + $scope.items[e.x].PriorityMonthDate + "&SecondGenericData=" + $scope.id
                    var obj = new Object();
                    obj.Request_Priority = $scope.items[e.x].Request_Priority;
                    obj.FirstGenericData = $scope.items[e.x].PriorityMonthDate
                    obj.SecondGenericData = $scope.id;
                    var data = JSON.stringify(obj);
                    mySharedService.openGridModal('lg', $scope.url, data);
                    },
                    names: {
                        Incoming: 'New',
                        Backlog: 'Backlog',
                        Completed: 'Completed',
                    },
                    colors: {
                        Incoming: $scope.ColorArray[0],
                        Backlog: $scope.ColorArray[1],
                        Completed: $scope.ColorArray[2]
                    }
                },
                tooltip: {
                    format: {
                        title: function (d, i) {
                            return $scope.chartPareto.categories()[d];
                        },
                        value: function (value, ratio, id) {
                            return value;
                        }
                    }
                },
                transition: {
                    duration: 500
                },
                axis: {
                    x: {
                        type: 'category',
                        tick: {
                            format: function (x) {
                                if ($scope.items[x] != undefined) {
                                    var barName = $scope.items[x].PriorityMonthDate;
									if( (barName != 'null') && (barName != 'undefined') &&  (barName != null) &&  (barName != undefined)  ){
										if (barName.length > 7) {
											return barName.substring(0, 4) + "...";
										} else {
											return barName;
										}
									}
                                } else {
                                    return "";
                                }
                            },
                        }
                    },
                    y: {
                        tick: {
                            format: function (d) {
								return (parseInt(d) == d) ? d : null;
							}

                        },
                        padding: {
                            bottom: 0
                        },
                        min: 0
                    }
                }
            });
        };
		
		 $scope.plotReopenChart = function (type) {
			$scope.selectPriorityName = type;
            $scope.StackedData = [['ReOpenCount']];
            if ($scope.isGrouped) {
                $scope.StackedData = [];
            }
            for (var k = 0; k < ParetoData.length; k++) {
                if (ParetoData[k].KPIID == $scope.KPIID) {
                    $scope.ParData = ParetoData[k].ParetoVal
                }
            }
            $scope.chartData = $filter('filter')($scope.ParData, type.toUpperCase());
            $scope.GetTableValues($scope.chartData);
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, $scope.KPITClr, $scope.ATClr, 3);
            $scope.chartPareto = c3.generate({
                bindto: "." + $scope.KPIIDZoom + "ParetoChrt",

                data: {
                    json: $scope.items,
                    keys: {
                        x: 'Pareto_Cat',
                        value: ['ReOpenCount'],
                    },
                    type: 'bar',
                    types: {
                        'ReOpenCount': 'spline'
                    },
                    groups: $scope.StackedData,
                    onclick: function (e, event) {
                        if (e.id == 'ReOpenCount') {
                            $scope.id = "New"
                        } else {
                            $scope.id = e.id;
                        }
                        $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
                          //  + "&Request_Priority=" + $scope.items[e.x].Request_Priority + "&FirstGenericData=" + $scope.items[e.x].PriorityMonthDate + "&SecondGenericData=" + $scope.id
                        var obj = new Object();
                        obj.Request_Priority = $scope.items[e.x].Request_Priority;
                        obj.FirstGenericData = encodeURIComponent($scope.items[e.x].PriorityMonthDate);
                        obj.SecondGenericData = $scope.id;
                        var data = JSON.stringify(obj);
                        mySharedService.openGridModal('lg', $scope.url,data);
                    },
                    names: {
                        ReOpenCount: 'Reopen Count'
                    },
                    colors: {
                        ReOpenCount: $scope.ColorArray[0],
                    }
                },
                tooltip: {
                    format: {
                        title: function (d, i) {
                            return $scope.chartPareto.categories()[d];
                        },
                        value: function (value, ratio, id) {
                            return value;
                        }
                    }
                },
                transition: {
                    duration: 500
                },
                axis: {
                    x: {
                        type: 'category',
                        tick: {
                            format: function (x) {
                                if ($scope.items[x] != undefined) {
                                    var barName = $scope.items[x].PriorityMonthDate;
									if( (barName != 'null') && (barName != 'undefined') &&  (barName != null) &&  (barName != undefined)  ){
										if (barName.length > 7) {
											return barName.substring(0, 4) + "...";
										} else {
											return barName;
										}
									}
                                } else {
                                    return "";
                                }
                            },
                        }
                    },
                    y: {
                        tick: {
                            format: function (d) {
								return (parseInt(d) == d) ? d : null;
							}

                        },
                        padding: {
                            bottom: 0
                        },
                        min: 0
                    }
                }
            });
        };
        $scope.plotRespResPercChart = function (type) {
            if ($scope.chosenUnit == "EXCEEDING_RESP_KPI") {
                $scope.Exceeding = 'Average Response Time';
            } else if ($scope.chosenUnit == "EXCEEDING_RESLN_KPI") {
                $scope.Exceeding = 'Average Resolution Time';
            } else {
                $scope.Exceeding = 'Exceeding(%)';
            }
            $scope.StackedData = [['Exceeding', 'NonExceeding']];
            if ($scope.isGrouped) {
                $scope.StackedData = [];
            }
            for (var k = 0; k < ParetoData.length; k++) {
                if (ParetoData[k].KPIID == $scope.KPIID) {
                    $scope.ParData = ParetoData[k].ParetoVal
                }
            }
            if (type == "ALL") {
                $scope.chartData = $scope.ParData;
            } else {

                $scope.chartData = $filter('filter')($scope.ParData, type.toUpperCase());
            }

            $scope.GetTableValues($scope.chartData);
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, $scope.KPITClr, $scope.ATClr, 3);
            $scope.chartPareto = c3.generate({
                bindto: "." + $scope.KPIIDZoom + "ParetoChrt",
                data: {
                    json: $scope.items,
                    keys: {
                        x: 'PriorityMonthDate',
                        value: ['Exceeding', 'NonExceeding', 'ExceedingPerc'],
                    },
                    axes: {
                        'ExceedingPerc': 'y2'
                    },
                    type: 'bar',
                    types: {
                        'ExceedingPerc': 'spline'
                    },
                    groups: $scope.StackedData,
                    onclick: function (e, event) {
                        if (e.id != "ExceedingPerc") {
                   if ($scope.chosenUnit == "EXCEEDING_RESLN_KPI" || $scope.chosenUnit == "EXCEEDING_RESP_KPI") {
                            $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
                            //+ "&Request_Priority=" + $scope.items[e.x].Request_Priority + "&FirstGenericData=" + encodeURIComponent($scope.items[e.x].PriorityMonthDate) + "&SecondGenericData=" + e.id
                            var obj = new Object();
                            obj.Request_Priority = $scope.items[e.x].Request_Priority;
                            obj.FirstGenericData = $scope.items[e.x].PriorityMonthDate;
                            obj.SecondGenericData = e.id;
                            var data = JSON.stringify(obj);
                        } else {
                            $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
                            //+ "&Request_Priority=" + $scope.items[e.x].Request_Priority + "&FirstGenericData=" + encodeURIComponent($scope.items[e.x].PriorityMonthDate) + "&SecondGenericData=" + e.id
                            var obj = new Object();
                            obj.Request_Priority = $scope.items[e.x].Request_Priority;
                            obj.FirstGenericData = $scope.items[e.x].PriorityMonthDate;
                            obj.SecondGenericData = e.id;
                            var data = JSON.stringify(obj);
                        }
                        console.log("CLICK PARETO C" + data + " " + $scope.KPIID)

                        mySharedService.openGridModal('lg', $scope.url, data);
                        }
                        //event.stopPropagation();
                    },
                    names: {
                        Exceeding: 'Request(s) Exceeding SLA',
                        NonExceeding: 'Request(s) Not Exceeding SLA',
                        ExceedingPerc: $scope.Exceeding
                    },
                    colors: {
                        Exceeding: $scope.ColorArray[0],
                        NonExceeding: $scope.ColorArray[1],
                        ExceedingPerc: $scope.ColorArray[2]
                    }
                },
                tooltip: {
                    format: {
                        title: function (d, i) {
                            return $scope.chartPareto.categories()[d];
                        },
                        value: function (value, ratio, id) {
                                //console.log(value);
                                return value.toFixed(2);
                            }
                            //            value: d3.format(',') // apply this format to both y and y2
                    }
                },
                transition: {
                    duration: 500
                },
                axis: {
                    x: {
                        type: 'category',
                        tick: {
                            format: function (x) {
                                if ($scope.items[x] != undefined) {
                                    var barName = $scope.items[x].PriorityMonthDate
									if( (barName != 'null') && (barName != 'undefined') &&  (barName != null) &&  (barName != undefined)  ){
										if (barName.length > 7) {
											return barName.substring(0, 4) + "...";
										} else {
											return barName;
										}
									}
                                } else {
                                    return "";
                                }
                            },
                        }
                    },
                    y: {
                        tick: {
                            format: function (d) {
								return (parseInt(d) == d) ? d : null;
							}

                        },
                        padding: {
                            bottom: 0
                        },
                        min: 0
                    },
                    y2: {
                        show: true,
                        tick: {
                            values:[0,25,50,75,100],    
							format: function (x) {
                                 return x+"%";
                            },
                            culling: {
                                max: 2 // the number of tick texts will be adjusted to less than this value
                            },
                            count: 5
                        },
                        padding: {
                            bottom: 0
                        },
                        min: 0,
                        max: 100
                    }
                }
            });
        };
        $scope.getData = function () {
            $scope.isLoadCmpltds = false;
            if (mySharedService.IsZoom) {
                $scope.isLoadCmpltds = true;
                $scope.KPIID = mySharedService.KPIID;

                if ($scope.PriorityType == "ALL") {
                    $scope.PriorityType = "TOTAL"
                }
                if ($scope.chosenUnit == "INCOMING_REQUEST" || $scope.chosenUnit == "COMPLETE_REQUEST" || $scope.chosenUnit == "BACKLOG_REQUEST") {
		//|| $scope.chosenUnit == "REOPEN_COUNT"
                    //$scope.plotIncVsCompChart();
                    setTimeout(function () {
                        $scope.plotIncVsCompChart($scope.PriorityType);
                    }, 100);
                } else if (($scope.chosenUnit == "RESOLVE_EXCEEDING_PERC" || $scope.chosenUnit == "RESPONSE_EXCEEDING_PERC" || $scope.chosenUnit == "EXCEEDING_RESP_KPI" || $scope.chosenUnit == "EXCEEDING_RESLN_KPI")) {
                    setTimeout(function () {
                        $scope.plotRespResPercChart($scope.PriorityType);
                    }, 100);
                } 
				else if ($scope.chosenUnit == "REOPEN_COUNT") {
                    setTimeout(function () {
                        $scope.plotReopenChart($scope.PriorityType);
                    }, 100);
                } else {
                    setTimeout(function () {
                        $scope.initialChart($scope.PriorityType)
                    }, 100);
                }

                if ($scope.chosenUnit == "TIME" || $scope.chosenUnit == "COUNT") {
                    $scope.isTimeChart = true;
                } else {
                    $scope.isTimeChart = false;
                }
                $scope.toggle1 = false;
                $scope.toggle2 = false;
                $scope.datatoshow = true;
                $scope.IsZoom = mySharedService.IsZoom;
                $scope.KPIIDZoom = $scope.KPIID + "zoomed"
                $scope.$emit("ZoomDataDone");
                $scope.isSpinnerChanged = false;
            } else {

                $scope.KPIID = ($element.parent().parent().parent().attr("data-kpiid") != undefined) ? $element.parent().parent().parent().attr("data-kpiid") : $scope.KPIID;

                if ($scope.IsRedrawZoom == true) {
                    $scope.KPIIDZoom = $scope.KPIID + "zoomed";
                } else {
                    $scope.KPIIDZoom = $scope.KPIID;
                }
                $scope.IsZoom = false;
                var url = uriApiURL + "api/KPI/GetDataKPIPareto";
                var userID = mySharedService.UserID;
                url = url + "?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID;

               
                var GetParetoData = { 'SpinnerData': $scope.SpinnerData, 'PrioritySelected': $scope.PriorityType };

                $http({
                        method: 'POST',
						params: {version: getEpocStamp()},
						cache: mySharedService.KPIboardCache,
						//url: uriApiURL + "api/KPI/GetDataKPIPareto?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + "&SpinnerData=" + $scope.SpinnerData + "&PrioritySelected=" + $scope.PriorityType + "",
						url: url,
						data: GetParetoData
                    })
                    .success(function (response) {
                        $scope.isLoadCmpltds = true;
                        $scope.WholeData = angular.fromJson(response);
						
                        $scope.ParetoData = $scope.WholeData["Pareto"];						
						
						if ($scope.ParetoData.length > 0) {
							$scope.kpiname = $scope.WholeData["KPIName"];
							$scope.kpiType = "kpiType12";
							$scope.period = "Jun-14 to Jun-15";

							$scope.whichType = $scope.WholeData["ParetoBarColumnName"];
							$scope.chosenUnit = $scope.WholeData["ParetoLineUnit"];
							if (($scope.chosenUnit != "COUNT" && $scope.chosenUnit != "TIME")) {
								if ($scope.PriorityType == "ALL") {
									if (ParetoData.length != 0) {
										var c = 0;
										for (var k = 0; k < ParetoData.length; k++) {
											if (ParetoData[k].KPIID == $scope.KPIID) {
												ParetoData = ParetoData.filter(function (obj) {
													return obj.KPIID !== $scope.KPIID;
												})
											}
										}
										ParetoData.push({
											KPIID: $scope.KPIID,
											ParetoVal: $scope.ParetoData
										})
									} else {
										ParetoData.push({
											KPIID: $scope.KPIID,
											ParetoVal: $scope.ParetoData
										})
									}
								} else {
									for (var k = 0; k < ParetoData.length; k++) {
										if (ParetoData[k].KPIID == $scope.KPIID) {
											var counter = 0;
											ParetoData[k].ParetoVal = ParetoData[k].ParetoVal.filter(function (obj) {
												return obj.Request_Priority !== $scope.ParetoData[0].Request_Priority;
											})
										}
										for (var l = 0; l < $scope.ParetoData.length; l++) {
											ParetoData[k].ParetoVal.push($scope.ParetoData[l]);
										}
									}
								}
							} else {
								ParetoData.push({
									KPIID: $scope.KPIID,
									ParetoVal: $scope.ParetoData
								})
							}
							if ($scope.WholeData.Error != "No Data") {
								if ($scope.WholeData.Request_Priority != null) {
									$scope.toggle1 = false;
									$scope.toggle2 = false;
									$scope.datatoshow = true;
									$scope.prioritiesArr = $scope.WholeData.Request_Priority.split(",");
									$scope.items = $scope.ParetoData;
									$scope.ColorArray = $scope.WholeData.ChartColor.split(",");
									$scope.DBColorArray = $scope.ColorArray;
									$scope.isGrouped = $scope.WholeData.IsGrouped;
									$scope.IsUseTheme = $scope.WholeData.IsUseTheme;
									if (($scope.chosenUnit != "COUNT" && $scope.chosenUnit != "TIME")) {

										if ($scope.PriorityType == "ALL") {
											$scope.prioritySwatch = [];
											for (var i in $scope.prioritiesArr) {
												$scope.prioritySwatch.push({
													'name': $scope.prioritiesArr[i],
													'value': $scope.prioritiesArr[i],
													'opacity': 'theme-text'
												});
												$scope.filterChartData = $filter('filter')($scope.ParetoData, $scope.prioritiesArr[i].toUpperCase());
												$scope.MakePriorityBasedData[$scope.prioritiesArr[i].toUpperCase()] = $scope.filterChartData;
												$scope.MakePriorityBasedNumber[$scope.prioritiesArr[i].toUpperCase()] = 6;
											}
											$scope.prioritySwatch[0].opacity = "";
											$scope.PriorityType = $scope.prioritySwatch[0].value;
												$scope.selectPriorityName =  $scope.prioritySwatch[0].value;
											$scope.items = $scope.MakePriorityBasedData[$scope.PriorityType.toUpperCase()];
											MakePriorityBasedData1 = $scope.MakePriorityBasedData;
											MakePriorityBasedNumber1 = $scope.MakePriorityBasedNumber;
										} else {
											$scope.filterChartData = $filter('filter')($scope.ParetoData, $scope.PriorityType.toUpperCase());
											$scope.MakePriorityBasedData[$scope.PriorityType] = $scope.filterChartData;
											$scope.items[$scope.PriorityType.toUpperCase()] = $scope.MakePriorityBasedData[$scope.PriorityType];
											MakePriorityBasedData1[$scope.PriorityType] = $scope.MakePriorityBasedData[$scope.PriorityType];
											MakePriorityBasedNumber1[$scope.PriorityType] = $scope.MakePriorityBasedNumber[$scope.PriorityType];
											//$scope.numberValNew = $scope.MakePriorityBasedNumber[$scope.selectPriority[0].toUpperCase()];
										}
										$scope.isTimeSpentKPI = false;

										$scope.subKPIName = $scope.mapColumnName($scope.chosenUnit);
									} else {
										$scope.FirstSpinnerArr = $scope.WholeData["FirstSpinner"].split(",");
										$scope.SecondSpinnerArr = $scope.WholeData["SecondSpinner"].split(",");
										$scope.minNumberVal = Math.floor($scope.FirstSpinnerArr[1]);
										$scope.maxNumberVal = Math.floor($scope.FirstSpinnerArr[2]);
										$scope.numberVal = parseInt($scope.FirstSpinnerArr[0]);
										$scope.defaultnumberVal = parseInt($scope.FirstSpinnerArr[0]);
										$scope.defaultpercVal = parseInt($scope.SecondSpinnerArr[0]);
										$scope.percVal = parseInt($scope.SecondSpinnerArr[0]);
										$scope.minPercVal = Math.floor($scope.SecondSpinnerArr[1]);
										$scope.maxPercVal = Math.floor($scope.SecondSpinnerArr[2]);
										$scope.isTimeSpentKPI = true;
									}
									if (mySharedService.IsZoom) {
										$scope.$emit("ZoomDataDone");
									}
									if ($scope.chosenUnit == "INCOMING_REQUEST" || $scope.chosenUnit == "COMPLETE_REQUEST" || $scope.chosenUnit == "BACKLOG_REQUEST" ) {
										//$scope.plotIncVsCompChart();
										$timeout(function () {
											$scope.plotIncVsCompChart($scope.PriorityType);
										}, 10);
									} 
									else if ($scope.chosenUnit == "REOPEN_COUNT") {
										//$scope.plotIncVsCompChart();
										$timeout(function () {
											$scope.plotReopenChart($scope.PriorityType);
										}, 10);
									}
									else if (($scope.chosenUnit == "RESOLVE_EXCEEDING_PERC" || $scope.chosenUnit == "RESPONSE_EXCEEDING_PERC" || $scope.chosenUnit == "EXCEEDING_RESP_KPI" || $scope.chosenUnit == "EXCEEDING_RESLN_KPI")) {
										$timeout(function () {
											$scope.plotRespResPercChart($scope.PriorityType);
										}, 10);
									} else {
										$timeout(function () {
											$scope.initialChart($scope.PriorityType);
										}, 10);

									}
									if ($scope.chosenUnit == "TIME" || $scope.chosenUnit == "COUNT") {
										$scope.isTimeChart = true;
									} else {
										$scope.isTimeChart = false;
									}
									$scope.isSpinnerChanged = false;
								} else {
									$scope.toggle1 = true;
									$scope.toggle2 = false;
									$scope.datatoshow = false;
								}
							} else {
								$scope.toggle1 = false;
								$scope.toggle2 = true;
								$scope.datatoshow = false;
							}
						}else{
						
						$scope.IsShowError = true;
						$scope.IsShowErrorMsg= 'Data Not Available !';
						}

                        //$timeout($scope.plotChart($scope.selectPriority[0]), 1000);
                    }).error(function (response) {
										
						$scope.IsShowError= true;
						$scope.IsShowErrorMsg= response;
						
					});
            }
        }

        $scope.$on('ZoomClose', function () {
            $scope.KPIIDZoom = $scope.KPIID;
            if (($scope.chosenUnit == "INCOMING_REQUEST" || $scope.chosenUnit == "COMPLETE_REQUEST" || $scope.chosenUnit == "BACKLOG_REQUEST" || $scope.chosenUnit == "REOPEN_COUNT")) {
                setTimeout(function () {
                    $scope.plotIncVsCompChart($scope.PriorityType)
                }, 100);
            } else if (($scope.chosenUnit == "RESOLVE_EXCEEDING_PERC" || $scope.chosenUnit == "RESPONSE_EXCEEDING_PERC" || $scope.chosenUnit == "EXCEEDING_RESP_KPI" || $scope.chosenUnit == "EXCEEDING_RESLN_KPI")) {
                setTimeout(function () {
                    $scope.plotRespResPercChart($scope.PriorityType)
                }, 100);
            } else {
                setTimeout(function () {
                    $scope.initialChart($scope.PriorityType)
                }, 100);
            }
            $scope.$parent.$parent.$parent.initialVal = ($scope.initialVal == undefined) ? 6 : $scope.initialVal;
            if ($scope.isTimeSpentKPI == true) {
                $scope.IsRedrawZoom = true;
                $scope.$parent.$parent.$parent.numberVal = ($scope.numberVal == undefined) ? 6 : $scope.numberVal;
                $scope.$parent.$parent.$parent.defaultnumberVal = $scope.defaultnumberVal;
                $scope.$parent.$parent.$parent.defaultpercVal = $scope.defaultpercVal;
                $scope.$parent.$parent.$parent.percVal = $scope.percVal;
            }
            $scope.$parent.$parent.$parent.numberValNew = ($scope.numberValNew == undefined) ? 6 : $scope.numberValNew;
            if ($scope.prioritySwatch != undefined) {
                for (var i = 0; i < $scope.prioritySwatch.length; i++) {
                    if ($scope.prioritySwatch[i].name.toUpperCase() == $scope.PriorityType) {
                        $scope.prioritySwatch[i].opacity = "";
                    } else {
                        $scope.prioritySwatch[i].opacity = "theme-text";
                    }
                }
            }
        })
        $scope.redraw = function () {
            if (($scope.chosenUnit != "COUNT" && $scope.chosenUnit != "TIME")) {
                $scope.initialVal = $scope.numberValNew;
                $scope.MakePriorityBasedNumber[$scope.PriorityType.toUpperCase()] = $scope.initialVal;
            }

            if (mySharedService.IsZoom == true) {
                $scope.IsRedrawZoom = true;
                mySharedService.IsZoom = false;
                //$scope.PriorityType = "ALL"
                //MakePriorityBasedData1 = {};
            }
            $scope.getData();
        }
        $scope.$watch('numberVal', function (newValue, oldValue) {
            if (newValue > $scope.maxNumberVal) {
                $scope.isSpinnerChanged = true;
                $scope.SpinnerData = $scope.maxNumberVal + 'N';
                $scope.whichColorperc = "grey";
                $scope.whichColorNum = "white";
                //Notification.error({ message: 'Entered value is greater than the maximum value.Please Enter a value below ' + $scope.maxNumberVal });
            } else if (newValue < $scope.minNumberVal) {
                $scope.isSpinnerChanged = true;
                $scope.SpinnerData = $scope.minNumberVal + 'N';
                $scope.whichColorperc = "grey";
                $scope.whichColorNum = "white";
                //Notification.error({ message: 'Entered value is lesser than the minimum value.Please Enter a value above ' + $scope.minNumberVal });
            } else if (newValue != undefined && newValue != $scope.defaultnumberVal) {
                $scope.isSpinnerChanged = true;
                $scope.SpinnerData = newValue + 'N';
                $scope.whichColorperc = "grey";
                $scope.whichColorNum = "white";
            } else {
                $scope.isSpinnerChanged = false;
                $scope.whichColorperc = "white";
                $scope.whichColorNum = "white";
            }

        }, true);
        $scope.$watch('percVal', function (newValue, oldValue) {
            if ($scope.IsRedrawZoom != true) {
                if (newValue > $scope.maxPercVal) {
                    $scope.isSpinnerChanged = true;
                    $scope.SpinnerData = $scope.maxPercVal + 'Y';
                    $scope.whichColorperc = "white";
                    $scope.whichColorNum = "grey";
                    //Notification.error({ message: 'Entered value is greater than the maximum value.Please Enter a value below ' + $scope.maxPercVal });
                } else if (newValue < $scope.minPercVal) {
                    $scope.isSpinnerChanged = true;
                    $scope.SpinnerData = $scope.minPercVal + 'Y';
                    $scope.whichColorperc = "white";
                    $scope.whichColorNum = "grey";
                    //Notification.error({ message: 'Entered value is lesser than the minimum value.Please Enter a value above ' + $scope.minPercVal });
                } else if (newValue != undefined && newValue != $scope.defaultpercVal) {
                    $scope.isSpinnerChanged = true;
                    $scope.SpinnerData = newValue + 'Y';
                    $scope.whichColorperc = "white";
                    $scope.whichColorNum = "grey";
                } else {
                    $scope.isSpinnerChanged = false;
                    $scope.whichColorperc = "white";
                    $scope.whichColorNum = "white";
                }
            }
        }, true);
        $scope.$on('handleaaddUpdateWidgetDataBroadcast', function (kpiValue) {
            /*kpiValue is KPIID*/
            if (mySharedService.KPIID == 2) {
                $scope.getData();
            }
        });
        $scope.makeLegend = function (jsonFilteredData) {
            if ($scope.chosenUnit == "COUNT") {
                $scope.LegenNames = {
                    LineValue: 'Time Spent',
                    BarValue: 'No. of Requests'
                };
            } else if ($scope.chosenUnit == "TIME") {
                $scope.LegenNames = {
                    LineValue: 'No. of Requests',
                    BarValue: 'Time Spent'
                };
            } 
			else if ($scope.chosenUnit == "AVG_RESLN_TIME") {
                $scope.LegenNames = {
                      LineValue: 'Average Resolution Time',
                    BarValue: 'No. of Requests'
                };
            } else {
                $scope.LegenNames = {
                    LineValue: 'Average Response Time',
                    BarValue: 'No. of Requests'
                };
            }
        };
        $scope.$on('setBaseColor', function (e, Aclr, Bclr) {
            $scope.ColorArray = getColorArray($scope.IsUseTheme, $scope.DBColorArray, Bclr, Aclr, 3);
            $scope.chartPareto.load({
                colors: {
                    BarValue: $scope.ColorArray[0],
                    LineValue: $scope.ColorArray[2],
                    Exceeding: $scope.ColorArray[0],
                    NonExceeding: $scope.ColorArray[1],
                    ExceedingPerc: $scope.ColorArray[2],
                    Incoming: $scope.ColorArray[0],
                    Backlog: $scope.ColorArray[1],
                    Completed: $scope.ColorArray[2]
                }
            });
        });
        $scope.mapColumnName = function (chosenVal) {
            var mappedVal = "";
            switch (chosenVal) {
                case "AVG_RESP_TIME":
                    mappedVal = "Highest Average Response Time";
                    $scope.LegenNames = {
                        LineValue: 'Average Response Time',
                        BarValue: 'No. of Requests'
                    };
                    break;
                case "AVG_RESLN_TIME":
                    mappedVal = "Highest Average Resolution Time";
                    $scope.LegenNames = {
                        LineValue: 'Average Resolution Time',
                        BarValue: 'No. of Requests'
                    };
                    break;
                case "EXCEEDING_RESP_KPI":
                    mappedVal = "Highest No. of Requests Exceeding Response SLA";
                    $scope.LegenNames = {
                        LineValue: 'Average Response Time',
                        BarValue: 'No. of Requests'
                    };

                    break;
                case "EXCEEDING_RESLN_KPI":
                    mappedVal = "Highest No. Of Requests Exceeding Resolution SLA";
                    $scope.LegenNames = {
                        LineValue: 'Average Resolution Time',
                        BarValue: 'No. of Requests'
                    };
                    break;
                case "RESOLVE_EXCEEDING_PERC":
                    mappedVal = "Highest Percentage of Requests Exceeding Resolution SLA";
                    $scope.LegenNames = {
                        LineValue: 'Average Resolution Time',
                        BarValue: 'No. of Requests'
                    };
                    break;
                case "RESPONSE_EXCEEDING_PERC":
                    mappedVal = "Highest Percentage of Requests Exceeding Response SLA";
                    $scope.LegenNames = {
                        LineValue: 'Average Response Time',
                        BarValue: 'No. of Requests'
                    };
                    break;
                case "INCOMING_REQUEST":
                    mappedVal = "Highest No. of Incoming Requests";
                    $scope.LegenNames = {
                        LineValue: 'No. of Requests',
                        BarValue: 'Time Spent'
                    };
                    break;
                case "COMPLETE_REQUEST":
                    mappedVal = "Highest No. of Completed Requests";
                    $scope.LegenNames = {
                        LineValue: 'No. of Requests',
                        BarValue: 'Time Spent'
                    };
                    break;
                case "BACKLOG_REQUEST":
                    mappedVal = "Highest No. of Backlog Requests";
                    $scope.LegenNames = {
                        LineValue: 'No. of Requests',
                        BarValue: 'Time Spent'
                    };
                    break;
		case "REOPEN_COUNT":
                    mappedVal = "Highest No. of Reopen Requests";
                    $scope.LegenNames = {
                        LineValue: 'No. of Requests',
                        BarValue: 'Time Spent'
                    };
                    break;

            }
            return mappedVal;
        }
        $scope.plotChart = function (Request_Priority) {
            $scope.PriorityType = Request_Priority.toUpperCase();			
			$scope.selectPriorityName = Request_Priority;
            $scope.numberValNew = ($scope.IsZoom == true || $scope.IsRedrawZoom == true) ? MakePriorityBasedNumber1[Request_Priority.toUpperCase()] : $scope.MakePriorityBasedNumber[Request_Priority.toUpperCase()];
            $scope.items = ($scope.IsZoom == true || $scope.IsRedrawZoom == true) ? MakePriorityBasedData1[Request_Priority.toUpperCase()] : $scope.MakePriorityBasedData[Request_Priority.toUpperCase()];
            if (($scope.chosenUnit == "INCOMING_REQUEST" || $scope.chosenUnit == "COMPLETE_REQUEST" || $scope.chosenUnit == "BACKLOG_REQUEST") ) {
                $scope.plotIncVsCompChart($scope.PriorityType);
            } else if (($scope.chosenUnit == "RESOLVE_EXCEEDING_PERC" || $scope.chosenUnit == "RESPONSE_EXCEEDING_PERC" || $scope.chosenUnit == "EXCEEDING_RESP_KPI" || $scope.chosenUnit == "EXCEEDING_RESLN_KPI")) {

                $scope.plotRespResPercChart($scope.PriorityType);

            }
			else if ($scope.chosenUnit == "REOPEN_COUNT"){

                $scope.plotReopenChart($scope.PriorityType);

            } 
			else {
                $scope.initialChart($scope.PriorityType);
            }
        }
        $scope.changeVal = function () {
            if ($scope.initialVal != $scope.numberValNew) {
                $scope.isSpinnerChanged = true;
            } else {
                $scope.isSpinnerChanged = false;
            }
            $scope.SpinnerData = $scope.numberValNew + "N";
        }
        $scope.substringName = function (value) {
            var val = "";
            if (value.length > 10) {
                val = value.substring(0, 7) + "...";
            } else {
                val = value;
            }
            return val;
        }
        $scope.expandChart = function () {
            $scope.animationsEnabled = true;
            $scope.KPI = {
                idNum: 3,
                Id: $scope.KPIID
            }
            mySharedService.OpenZoomModal($scope);
        }
        $scope.getWidgetSize();
        $scope.getData();
    })