appDashboard.controller('HeatMapController', function ($scope, $timeout, $routeParams, $filter, $http, mySharedService, $element , Notification) {
    $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
	$scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
	$scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
	$scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
	$scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
$scope.type = $element.parent().attr("type");
$scope.AssessmentId = $routeParams.AssessmentId;
    //$scope.ChartElement = angular.element($element[0].querySelector('.heatchart'));
	$scope.IsZoom = false;
	$scope.IsConnectionError  = false;
	var ChartElement = "#heatchart"+$scope.KPIID+$scope.IsZoom;
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.groupedName;
	$scope.PriorityType = "TOTAL";
$scope.wholeData=[];
    $scope.theColorIs = returnThemeColor($scope.ATClr);
	
	$scope.openWidgetError = function (response) {		
			 Notification.error({
					verticalSpacing: 60,
					message: $scope.IsShowErrorMsg,
					title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
			});	
		};
    $scope.getWidgetSize = function () {
        var sizeval = getKPISize($scope.KPIID, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
        $scope.SizeXVal = sizeval[0];
        $scope.SizeYVal = sizeval[1];
    }
    $scope.fClose = function () {
        $scope.wholeData = [];
        for (var i = 0; i < $scope.EntireData.length; i++) {
            if ($scope.EntireData[i].Category.toUpperCase() == $scope.CategoryObjOp[0].name.toUpperCase().split('-')[1]) {
                $scope.wholeData.push($scope.EntireData[i])
            }
        }
        $timeout(function () {
            $scope.plotHourlyHeatMap($scope.wholeData, $scope.PriorityType)
        }, 200);
    }
    $scope.getWidgetSize();
    var recursiveAllName = function (params) {
        if (params.parent == null) {
            return params.key;
        } else {
            if (params.key == undefined) {
                return arguments.callee(params.parent) + "," + params.data[$scope.groupedName]; //
            } else {
                return arguments.callee(params.parent) + "," + params.key;
            }
        }
    }
    $scope.type = $element.parent().attr("type");
    if (mySharedService.IsZoom) {
        $scope.isIncVsCompLoaded = true;
        $scope.KPIID = mySharedService.KPIID;
        $scope.IsZoom = mySharedService.IsZoom;
        ChartElement = "#heatchart" + $scope.KPIID + $scope.IsZoom;
        $scope.KPIIDZoom = $scope.KPIID + "zoomed"
        $scope.toggle1 = false;
        $scope.datatoshow = true;
        $timeout(function () {
		$scope.PriorityType = "TOTAL"
            $scope.plotHourlyHeatMap($scope.wholeData, $scope.PriorityType)
        }, 200);
        $scope.$emit("ZoomDataDone");
    } else {
        //$scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
        $scope.IsZoom = false;
        $scope.KPIIDZoom = $scope.KPIID;
      //  $scope.type = $element.parent().attr("type");
        $scope.url = uriApiURL + "api/KPI/GetHourHeatmap?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + ""
        $http({
            method: 'GET',
            cache: mySharedService.KPIboardCache,
	    params: {version: getEpocStamp()},
            url: $scope.url
        })
        .success(function (response) {
            $scope.AllData = angular.fromJson(response);
            $scope.EntireData = $scope.AllData.HeatMapDay;
            $scope.PriorityType = "TOTAL";
            $scope.CategoryObj = $scope.AllData.Category;
            $scope.kpiname = $scope.AllData["KPIName"];
            $scope.prioritySwatch = [];
            for (var i = 0; i < $scope.EntireData.length; i++) {
                if ($scope.EntireData[i].Category.toUpperCase() == $scope.CategoryObj[0].name.toUpperCase().split('-')[1]) {
                    $scope.wholeData.push($scope.EntireData[i])
                }
            }
            if ($scope.AllData.Request_Priority != null) {
                $scope.toggle1 = false;
                $scope.datatoshow = true;
                $scope.prioritiesArr = $scope.AllData.Request_Priority.split(",");
                for (var i in $scope.prioritiesArr) {
                    $scope.prioritySwatch.push({
                        'name': $scope.prioritiesArr[i],
                        'value': $scope.prioritiesArr[i],
                        'opacity': 'theme-text'
                    });
                }
                $scope.prioritySwatch[0].opacity = "";
                $scope.selectPriority = [$scope.prioritySwatch[0].value];
                $scope.isGrouped = $scope.AllData.IsGrouped;
                $scope.IsUseTheme = $scope.AllData.IsUseTheme;
                $timeout(function () {
                    $scope.plotHourlyHeatMap($scope.wholeData, $scope.PriorityType)
                }, 100);
                if (mySharedService.IsZoom) {
                    $scope.$emit("ZoomDataDone");
                }
            }
                 $timeout(function () {
				
				
            }, 200);
        }).error(function (response) {
										
            $scope.IsShowError= true;
            $scope.IsShowErrorMsg= response;
						
        });
    }
    $scope.changePriority = function (Request_Priority) {


		
        //console.log("changePriority" + Request_Priority)
        $scope.PriorityType = Request_Priority.toUpperCase();
        $timeout(function () {
            $scope.plotHourlyHeatMap($scope.wholeData ,$scope.PriorityType )
        }, 100);
    }		
    $scope.plotHourlyHeatMap = function (datais, reqPriority) {
        var data = [];
        for (var k = 0; k < datais.length; k++) {
            if (datais[k].Request_Priority.toUpperCase() == reqPriority.toUpperCase()) {
                var newtem = datais[k];
                data.push(newtem)
            }
        }
        //if (data.length != 0) {
        //    $scope.IsWithVal = true;
        //    $scope.IsWithNoVal = false;
        $timeout(function () {
            ChartElement = "#heatchart" + $scope.KPIID + $scope.IsZoom;
            $(ChartElement).empty();
            var parentEl = $(ChartElement);
            var margin = {
                top: 30,
                right: 20,
                bottom: 20,
                left: 40
            },
                width = parentEl.width() - margin.left - margin.right,
                height = parentEl.height() - margin.top - margin.bottom,
                gridSize = Math.floor(width / 24),
                legendElementWidth = gridSize * 2,
                buckets = 9,
                colors = generateColorGradientFromBaseTheme($scope.KPITClr, $scope.ATClr, buckets),
                days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                times = ["1a", "2a", "3a", "4a", "5a", "6a", "7a", "8a", "9a", "10a", "11a", "12p", "1p", "2p", "3p", "4p", "5p", "6p", "7p", "8p", "9p", "10p", "11p", "12a"];
            var svg = d3.select(ChartElement).append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var dayLabels = svg.selectAll(".dayLabel")
                .data(days)
                .enter().append("text")
                .text(function (d) {
                    return d;
                })
                .attr("x", 0)
                .attr("y", function (d, i) {
                    return i * gridSize;
                })
                .style("text-anchor", "end")
                .attr("transform", "translate(-6," + gridSize / 1.5 + ")")
                .attr("class", function (d, i) {
                    return ((i >= 1 && i <= 5) ? "dayLabel mono axis axis-workweek" : "dayLabel mono axis");
                });

            var timeLabels = svg.selectAll(".timeLabel")
                .data(times)
                .enter().append("text")
                .text(function (d) {
                    return d;
                })
                .attr("x", function (d, i) {
                    return i * gridSize;
                })
                .attr("y", 0)
                .style("text-anchor", "middle")
                .attr("transform", "translate(" + gridSize / 2 + ", -6)")
                .attr("class", function (d, i) {
                    return ((i >= 7 && i <= 16) ? "timeLabel mono axis axis-worktime" : "timeLabel mono axis");
                });
            $scope.isLoadCmpltd = true;


            //console.log(data);
            $scope.drawChart = function () {

                //  angular.forEach(angular.fromJson(response), function (data) {
                var colorScale = d3.scale.quantile()
                    .domain([0, buckets - 1, d3.max(data, function (d) {
                        return d.value;
                    })])
                    .range(colors);
                var classScale = d3.scale.quantile()
                    .domain([0, buckets - 1, d3.max(data, function (d) {
                        return d.value;
                    })])
                    .range(colors);

                var cards = svg.selectAll(".hour")
                    .data(data, function (d) {
                        return d.day + ':' + d.hour;
                    });

                cards.append("title");

                cards.enter().append("rect")
                    .attr("x", function (d) {
                        return (d.hour) * gridSize;
                    })
                    .attr("y", function (d) {
                        return (d.day - 1) * gridSize;
                    })
                    .attr("rx", 4)
                    .attr("ry", 4)
                    .attr("class", "hour bordered ")
                    .attr("width", gridSize)
                    .attr("height", gridSize)
                    .style("fill", colors[0])
.on({
    "click": function (d, i) {

        var iii = i;
        if (d.value > 0) {

            $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
            //+ "&Request_Priority=" + d.Request_Priority + "&FirstGenericData=" + d.Category + "&SecondGenericData=" + d.day + "&ThirdGenericData=" + d.hour;
            var obj = new Object();
            obj.Request_Priority = d.Request_Priority;
            obj.FirstGenericData = d.Category;
            obj.SecondGenericData = d.day;
            obj.ThirdGenericData = d.hour;
            var data = JSON.stringify(obj);
            mySharedService.openGridModal('lg', $scope.url, data);


        }
    }
});
                cards.transition().duration(10)
                    .style("fill", function (d) {
                        return colorScale(d.value);
                    });

                cards.append("svg:title").text("Your tooltip info");

                cards.select("title").text(function (d) {
                    return d.value;
                });

                cards.exit().remove();

                var legend = svg.selectAll(".legend")
                    .data([0].concat(colorScale.quantiles()), function (d) {
                        return d;
                    });

                legend.enter().append("g")
                    .attr("class", "legend");

                    legend.append("rect")
                        .attr("x", function (d, i) {
                            return legendElementWidth * i;
                        })
						.attr("y", function (d, i) {
                            return gridSize * 9;
                        })
                        .attr("width", legendElementWidth)
                        .attr("height", gridSize / 2)
                        .style("fill", function (d, i) {
                            return colors[i];
                        });

                    legend.append("text")
                        .attr("class", "mono")
                        .text(function (d) {
                            return "≥ " + Math.round(d);
                        })
                        .attr("x", function (d, i) {
                            return legendElementWidth * i;
                        })
                        .attr("y", gridSize*9 );

                legend.exit().remove();
            }
            //});

            //
            $scope.drawChart();

        }, 200);
        // }
        // else {
        //     $scope.IsWithVal = false;
        //   $scope.IsWithNoVal = true;
        
    };
    $scope.expandChart = function () {
        $scope.animationsEnabled = true;
        $scope.KPI = {
            idNum: 13,
            Id: $scope.KPIID
        }
        mySharedService.OpenZoomModal($scope);
    }	
    $scope.$on('setBaseColor', function (e, Aclr, Bclr) {

        $scope.theColorIs = returnThemeColor($scope.ATClr);
        $timeout(function () {
            $scope.plotHourlyHeatMap($scope.wholeData ,$scope.PriorityType )
        }, 100);


    });
    $scope.$on('resizeCharts', function () {
        $timeout(function () {
            $scope.plotHourlyHeatMap($scope.wholeData, $scope.PriorityType)
        }, 100);
    });
})