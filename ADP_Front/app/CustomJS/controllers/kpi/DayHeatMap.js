appDashboard.controller('DayHeatMapController', function ($scope, $timeout, $routeParams, $filter, $http, mySharedService, $element, mbScrollbar, Notification) {
    $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
    $scope.KpiGroupType = $element.parent().parent().parent().attr("data-kpigroup");
    $scope.KpiGroupID = $element.parent().parent().parent().attr("data-kpigropid");
    $scope.KpiGroupName = $element.parent().parent().parent().attr("data-kpigropname");
    $scope.KpiGroupKey = $element.parent().parent().parent().attr("data-kpigroupkey");
    $scope.ChartElement = angular.element($element[0].querySelector('.heatchart'));
    $scope.type = $element.parent().attr("type");

    $scope.AssessmentId = $routeParams.AssessmentId;

    $scope.IsZoom = false;
    $scope.dashBoardID = $routeParams.dashboardId;
    $scope.groupedName;
    $scope.wholeData = [];
    $scope.ColorArray = getColorArray(true, "", $scope.KPITClr, $scope.ATClr, 9);

    $scope.openWidgetError = function (response) {
        Notification.error({
            verticalSpacing: 60,
            message: $scope.IsShowErrorMsg,
            title: '<i class="fa fa-warning"></i>&nbsp;Wrong Query or Insuffucuent Data'
        });
    };
    $scope.getWidgetSize = function () {
        var sizeval = getKPISize($scope.KPIID, $scope.KPICollections, $scope.SizeObjX, $scope.SizeObjY)
        $scope.SizeXVal = sizeval[0];
        $scope.SizeYVal = sizeval[1];
    }
    $scope.fClose = function () {
        $scope.wholeData = [];
        for (var i = 0; i < $scope.EntireData.length; i++) {
            if ($scope.EntireData[i].Category.toUpperCase() == $scope.CategoryObjOp[0].name.toUpperCase().split('-')[1]) {
                $scope.wholeData.push($scope.EntireData[i])
            }
        }
        $timeout(function () {
            $scope.plotdaYHeatMap($scope.wholeData, $scope.PriorityType)
        }, 100);
    }
    $scope.getWidgetSize();
    var recursiveAllName = function (params) {
        if (params.parent == null) {
            return params.key;
        } else {
            if (params.key == undefined) {
                return arguments.callee(params.parent) + "," + params.data[$scope.groupedName]; //
            } else {
                return arguments.callee(params.parent) + "," + params.key;
            }
        }
    }
    if (mySharedService.IsZoom) {
        $scope.isIncVsCompLoaded = true;
        $scope.KPIID = mySharedService.KPIID;
        $scope.IsZoom = mySharedService.IsZoom;
        $scope.KPIIDZoom = $scope.KPIID + "zoomed"
        $scope.toggle1 = false;
        $scope.datatoshow = true;
        $timeout(function () {
            $scope.plotdaYHeatMap($scope.wholeData, $scope.PriorityType)
        }, 200);
        $scope.$emit("ZoomDataDone");
    } else {
        $scope.KPIID = $element.parent().parent().parent().attr("data-kpiid");
        $scope.IsZoom = false;
        $scope.KPIIDZoom = $scope.KPIID;
        $scope.prioritySwatch = [];

        $scope.type = $element.parent().attr("type");
        if ($scope.type == "14") {
            $scope.url = uriApiURL + "api/KPI/GetDayHeatmap?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID + ""
        }
        $http({
                method: 'GET',
                cache: mySharedService.KPIboardCache,
                url: $scope.url
            })
            .success(function (response) {
                $scope.AllData = angular.fromJson(response);
                $scope.EntireData = $scope.AllData.HeatMapDay;
                $scope.PriorityType = "TOTAL";
                $scope.prioritySwatch = [];
                $scope.CategoryObj = $scope.AllData.Category;
                $scope.kpiname = $scope.AllData["KPIName"];
                for (var i = 0; i < $scope.EntireData.length; i++) {
                    if ($scope.EntireData[i].Category.toUpperCase() == $scope.CategoryObj[0].name.toUpperCase().split('-')[1]) {
                        $scope.wholeData.push($scope.EntireData[i])
                    }
                }
                if ($scope.AllData.Request_Priority != null) {
                    $scope.toggle1 = false;
                    $scope.datatoshow = true;
                    $scope.prioritiesArr = $scope.AllData.Request_Priority.split(",");
                    for (var i in $scope.prioritiesArr) {
                        $scope.prioritySwatch.push({
                            'name': $scope.prioritiesArr[i],
                            'value': $scope.prioritiesArr[i],
                            'opacity': 'theme-text'
                        });
                    }
                    $scope.prioritySwatch[0].opacity = "";
                    $scope.selectPriority = [$scope.prioritySwatch[0].value];
					$scope.selectPriorityName = $scope.prioritySwatch[0].value;
                    $scope.isGrouped = $scope.AllData.IsGrouped;
                    $scope.IsUseTheme = $scope.AllData.IsUseTheme;
                    $scope.ParData = [];
 		    $scope.isLoadCmpltd = true;
                    $timeout(function () {
                        $scope.plotdaYHeatMap($scope.wholeData, $scope.PriorityType)
                    }, 100);
                    if (mySharedService.IsZoom) {
                        $scope.$emit("ZoomDataDone");
                    }
                }



            }).error(function (response) {

                $scope.IsShowError = true;
                $scope.IsShowErrorMsg = response;

            });
    }
    $scope.changePriority = function (Request_Priority) {
        $scope.PriorityType = Request_Priority.toUpperCase();
        $timeout(function () {
            $scope.plotdaYHeatMap($scope.wholeData, $scope.PriorityType)
        }, 100);
    }
    $scope.plotdaYHeatMap = function (datais, PriorityType) {
        var data = [];
        for (var k = 0; k < datais.length; k++) {
            if (datais[k].Request_Priority.toUpperCase() == PriorityType.toUpperCase()) {
                var newtem = datais[k];
                data.push(newtem)
            }
        }




        var parser = function (data) {
            var stats = {};
            for (var d in data) {
                stats[data[d].date] = data[d].value;
            }
            return stats;
        };
        $scope.calELID = "#cal-heatmap-" + $scope.KPIID;
        $($scope.calELID).empty();
        var calendar = new CalHeatMap();
        //$scope.calELIDOrg = $element.find($scope.calELID);

        $scope.fieldArray = [];
        angular.forEach(data, function (item) {
            if (item.value) {
                $scope.fieldArray.push(item.value)
            }
        })
        $scope.fielddataJson = angular.toJson($scope.fieldArray, true);
        $scope.legMax = Math.max.apply(Math, $scope.fieldArray);
        $scope.legMin = Math.min.apply(Math, $scope.fieldArray);

        $scope.legvar = ($scope.legMax - $scope.legMin) / 9;
        $scope.legvar = Math.round($scope.legvar);

        $scope.LegArray = [];
        //$scope.LegArray.push($scope.legMin)
        /*for(i = 1; i < 9 ; i++){
        	var tempp = $scope.legvar * i;
        	console.log("tempp " + tempp);
        	$scope.LegArray.push(tempp)
        }*/
        for (i = 8; i > 0; i--) {
            var tempp = Math.round($scope.legMax / i) - $scope.legvar + 1;
            $scope.LegArray.push(tempp)
        }
        function monthDiff(d1, d2) {
            if (d2 < d1) {
                var dTmp = d2;
                d2 = d1;
                d1 = dTmp;
            }

            var months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth() + 1;
            months += d2.getMonth();

            if (d1.getDate() <= d2.getDate()) months += 1;

            return months;
        }

        $scope.MonthRange = Math.round(data.length / 30);
        $scope.MonthRange++;
        if ($scope.MonthRange == 0)
            $scope.MonthRange = 1;
        var dllst = new Date(data[0].date * 1000);
        var dllpr = new Date(data[data.length - 1].date * 1000);
        var modif = monthDiff(dllst, dllpr)
        $scope.MonthRange = monthDiff(dllst, dllpr);
        $scope.MonthRange++;

        console.log("DLL " + dllst + " - " + dllpr + " " + modif);
        calendar.init({
            itemSelector: $scope.calELID,
            data: data,
            afterLoadData: parser,
            start: new Date(data[0].date * 1000),
            end: new Date(data[data.length - 1].date * 1000),
            cellSize: 20,
            cellPadding: 5,
            domainGutter: 20,
            domain: "month",
            range: $scope.MonthRange,
            domainLabelFormat: "%m - %Y",
            subDomain: "x_day",
            subDomainTextFormat: "%d",
            tooltip: true,
            weekStartOnMonday: false,
            legendCellSize: 30,
            legendCellPadding: 5,
            legend: $scope.LegArray,
            legendtooltip: true,
            onComplete: function () {

                setTimeout(function () {
                    $scope.DayHeatAppend();
                    $scope.$broadcast('recalculateMBScrollbars');
                    mbScrollbar.recalculate($scope);
                }, 100);
            },
            onClick: function (date, nb) {
                console.log("CAL HHEATMAP clicked" + date + " " + nb);;
                if (nb > 0) {
                    $scope.url = "api/KPI/GetDrillDownData?DashboardID=" + $scope.dashBoardID + "&KPIID=" + $scope.KPIID
                    //+ "&FirstGenericData=" + $scope.CategoryObjOp[0].name.split('-')[1] + "&Request_Priority=" + $scope.PriorityType + "&SecondGenericData=" + ((new Date(date)).getTime()) / 1000;
                    var obj = new Object();
                    //obj.FirstGenericData = $scope.CategoryObjOp[0].name;
                    obj.FirstGenericData = $scope.CategoryObjOp[0].name.toUpperCase().split('-')[1];
                    obj.Request_Priority = $scope.PriorityType;
                    obj.SecondGenericData = ((new Date(date)).getTime()) / 1000;
                    var data = JSON.stringify(obj);
                    mySharedService.openGridModal('lg', $scope.url, data);
                }
            }
        });
        var temp = '<svg x="0" y="-16" class="graph-subdomain-group leg-sm"> <g> <text class="subdomain-text" x="10" y="35" text-anchor="middle" dominant-baseline="central">S</text> </g> <g> <text class="subdomain-text" x="35" y="35" text-anchor="middle" dominant-baseline="central">M</text> </g> <g> <text class="subdomain-text" x="60" y="35" text-anchor="middle" dominant-baseline="central">T</text> </g> <g> <text class="subdomain-text" x="85" y="35" text-anchor="middle" dominant-baseline="central">W</text> </g> <g> <text class="subdomain-text" x="110" y="35" text-anchor="middle" dominant-baseline="central">T</text> </g> <g> <text class="subdomain-text" x="135" y="35" text-anchor="middle" dominant-baseline="central">F</text> </g> <g> <text class="subdomain-text" x="160" y="35" text-anchor="middle" dominant-baseline="central">S</text> </g> </svg>';

        $scope.DayHeatAppend = function () {

            var ElegParentCont = ".CH" + $scope.KPIID + $scope.IsZoom;
            var ElegPrCont = $(ElegParentCont).find("rect");

            var El = $(ElegParentCont).find('.graph-domain');
            $(El).each(function (i, item) {

                $(item).find(".graph-subdomain-group").attr("y", "30");
                $(item).append(temp);
                var tempel = $(item).find(".graph-label");
                //tempel.attr("y", Number(tempel.attr("y"))+30);
                tempel.attr("y", 175);
            });




            //var ElegParent = angular.element($element[0].querySelector('.graph-legend'));
            // var Eleg = angular.element($element[0].querySelector('.graph-legend')).find("rect");
            //  console.log("ElegParent" + ElegParent + " " + Eleg);

            var ElegParent = $(ElegParentCont).find(".graph-legend");
            var Eleg = $(ElegParent).find("rect");
            $(ElegParent).addClass('ng-hide');
            var widthRange = 0;
            var widthTime = Eleg.length * 110;
            var widthTimeTest = "";
            setTimeout(function () {
                $(Eleg).each(function (i, item) {

                    $(item).attr("x", widthRange);
                    $(item).attr("width", 50);
                    $(item).attr("height", 12);
                    //widthTimeTest = widthTimeTest + "<text>  "+$scope.LegArray[i] +"</text>";
                    if (i == 0) {
                        widthTimeTest = widthTimeTest + "<span class='pull-left' style=''>  > 0 </span>";
                    } else {
                        widthTimeTest = widthTimeTest + "<span class='pull-left' style=''>  > " + $scope.LegArray[i - 1] + "</span>";
                    }

                    widthRange = widthRange + 50;
                });
                //widthTimeTest = "<g transform x='0' y ='0'>" + widthTimeTest + "</g> ";
                //$(Eleg).closest('svg').append(widthTimeTest);

                // widthTimeTest = "<g>" + widthTimeTest + " <rect class='hidevis' x='0' y='0' width='200' height='50'></rect> </g>";
                // $(Eleg).closest('svg').append(widthTimeTest);

                widthTimeTest = "<div class='leg-day-heat'>" + widthTimeTest + "<div>";
                $(Eleg).closest('.day-heat-cont').append(widthTimeTest);


                var textXRange = 0;
                setTimeout(function () {
                    var TextLeg = $(ElegParent).find('text');
                    // console.log("TextLeg" + TextLeg.length)
                    $(TextLeg).each(function (i, item) {
                        $(item).attr("x", textXRange).attr("y", 21);
                        $(item).attr("transform", "translate(0,0)");

                        textXRange = textXRange + 50;
                    });
                }, 200);

                $(ElegParent).attr("width", widthTime);
                $(ElegParent).attr("height", 60);
                $(ElegParent).attr("y", 180);
                $(ElegParent).removeClass('ng-hide');

                setTimeout(function () {
                    $(ElegParent).find("g").attr("transform", "translate(0,0)");
                }, 200);
            }, widthTime);

            if ($scope.IsZoom) {
                var ElegPrCWidth = $(ElegParentCont).width();
                var ElegPrChWidth = $(ElegParentCont).find(".cal-heatmap-container").width();
                if (ElegPrCWidth < ElegPrChWidth) {
                    /*RESIZE*/
                }

            }




        }


    };








    ////////////////
    /*
     verticalOrientation: true
    */


    $scope.expandChart = function () {
        $scope.animationsEnabled = true;
        $scope.KPI = {
            idNum: 15,
            Id: $scope.KPIID
        }
        mySharedService.OpenZoomModal($scope);
    }
    $scope.$on('resizeCharts', function () {

        $timeout(function () {
            $scope.plotdaYHeatMap($scope.wholeData, $scope.PriorityType)
        }, 100);

    });

});
