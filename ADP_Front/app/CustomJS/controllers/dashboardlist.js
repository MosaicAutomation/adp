/// <reference path="../../Views/_AddAssessment.html" />
/// <reference path="../../Views/_AddAssessment.html" />
appDashboard.controller('DashboardListController', function ($scope, $rootScope, $http,$uibModal, $window, $timeout, $location, $routeParams, Notification, mySharedService) {
	mySharedService.DashboardCache= true;
	$scope.AsgObjOpName= "";
	$scope.AsgObj= [];
	$scope.AsgObjOp= [];
	
	$scope.$on('walkthrough', function () {
	
		$timeout(function () {
			$scope.WalkThroughCall()
		},10);
		$scope.ShouldAutoStarts = true;
		$scope.IntroOption = {
			steps: [
				{
					element: '#IntroViewmode',
					position: 'bottom',
					intro: 'We have provided with two modes i.e.,Computer and Tablet.'
				},
				{
					element: '#IntroDash',
					position: 'bottom',
					intro: 'Here Custom and Predefined dashboards will be available.'
				},
				{
					element: '#addDashboard',
					position: 'left',
					intro: 'You can add your new Dashboard here and change the Settings'
				}
				],
			showStepNumbers: false,
			exitOnOverlayClick: true,
			exitOnEsc: true,
			nextLabel: '<span class="btn btn-sm">NEXT!</span>',
			prevLabel: '<span class="btn btn-sm">Previous</span>',
		};

	});
	$scope.Resolution = sessionStorage.getItem("Resolution");
	if (!$scope.Resolution) {
		if ($window.innerWidth > 1440) {
			sessionStorage.setItem("Resolution", "High");
		} else {
			sessionStorage.setItem("Resolution", "Low");
		}
		$scope.Resolution = sessionStorage.getItem("Resolution");
	}
	if ($scope.Resolution == "High") {
		$scope.columnValue = 12;
		$scope.opacityValCom = "";
		$scope.opacityValue = "theme-text";
	} else {
		$scope.columnValue = 8;
		$scope.opacityValCom = "theme-text";
		$scope.opacityValue = "";
	}
	$scope.gridsterOpts = {
		columns: $scope.columnValue,
		mobileBreakPoint: 800,
		margins: [15, 15],
		outerMargin: true,
		rowHeight: 105,
		pushing: true,
		floating: true,
		draggable: {
			enabled: false
		},
		resizable: {
			enabled: false
		}
	};
	$scope.GetUserDashDetails = function (userid,AsgIds) {
	var columnValues = 1;
		var epoTsmp = "version="+getEpocStamp();
	    $http.get(uriApiURL + "api/Dashboard/GetDashBoardCollections?AsgIDs=" + AsgIds)// {cache:mySharedService.DashboardCache})
			.then(function (response) {
				$scope.gridsterOpts.columns = columnValues;
				var JsonRes = angular.fromJson(response.data.m_StringValue);
				var tempArr = JsonRes.DashBoardDetails;
				$scope.DashboardCollectionR = tempArr.splice(0, 1);
				$scope.DashboardCollectionC = tempArr[0].ClusterData;
				$scope.DashboardCollection = tempArr[0].DashBoard;
				$scope.DashboardCollectionName = tempArr[0].ClusterData[0].Cluster.ClusterName;
				console.log("ASSD " + tempArr[0].ClusterData[0].Cluster.ClusterName);
				mySharedService.makeBgLoad(false);
			});
	}
	$scope.RemAssessment = function () {
	    $scope.IsFromSecStep = 1;
	    $scope.DeleteVar = " Assessment"
	    $scope.Deleteurl = uriApiURL + "api/Proffer/DelAsg?AsgId=" + $routeParams.AssessmentId;
	    var modalInstance = $uibModal.open({
	        animation: $scope.animationsEnabled,
	        templateUrl: 'app/views/_DeleteModal.html',
	        controller: 'DeleteModalController',
	        backdrop: 'static',
	        size: 'md large-model',//size
	        scope: $scope
	    })
	}
	$scope.AddAssessment = function () {
	    $scope.AsgName = $scope.AsgFileName;
	    var modalInstance = $uibModal.open({
	                animation: $scope.animationsEnabled,
	                templateUrl: 'app/views/_AddAssessment.html',	              
	                controller: 'AddAsgModalController',
	                backdrop: 'static',
	                size: 'md large-model',//size
	                scope: $scope
	            })
	}

    
    $scope.GetAsgDetails = function () {


        if ($scope.AsgObjOpName != "") {




            for (var i = 0; i < $scope.AsgObj.length; i++) {
                console.log("ENTY I" + $scope.AsgObj[i].value + " " + $scope.AsgObjOpName);
                if ($scope.AsgObjOpName == $scope.AsgObj[i].name) {
                   // mySharedService.AssessmentId = $scope.AsgObjOp[i].value;
                    mySharedService.currentSelAssesment = $scope.AsgObj[i].name;
                    $location.path("/assessment/" + $scope.AsgObj[i].value);
                }
            }

        }
        else {
            alert("Enter Assessment name")
        }
    }
    $scope.onSearchAssSelect = function (item) {
        console.log("ENTY item" + item);
        $scope.AsgObjOpName = item.name;
        $scope.AsgObjOp.push(item);
        $scope.GetAsgDetails();
    };
	$scope.$on("DelAssessment", function () {
	    $scope.IsDashElem = true;
	    $scope.AsgObjOp = [];
	    $scope.GetAssesmentDetails($scope.columnValue);
	    $scope.DashboardCollectionR = [];
	    $scope.DashboardCollection = [];
	})
	$scope.GetAssesmentDetails = function (columnValues) {
	    $scope.isLoadAsgData = false;
	    $scope.YesAssesment = true;
	    $scope.NoAssesment = true;
	    $http.get(uriApiURL + "api/Proffer/GetUserAssesmentData")
			.then(function (response) {
			    //$scope.AsgObjOp;
			    $scope.AsgObj = angular.fromJson(response.data);
			    if ($scope.AsgObj.length == 0) {
			        $scope.IsDashElem = true;
			    } else {
			        $scope.IsDashElem = false;
			    }
			    if (mySharedService.AssessmentId != undefined) {
                    		for (var i = 0; i < $scope.AsgObj.length; i++) {
			            if ($scope.AsgObj[i].value == mySharedService.AssessmentId) {
			                $scope.AsgObj[i].ticked = true;
			                $scope.IsDashElem = false;
			                $scope.GetUserDashDetails(mySharedService.UserID, columnValues)
			            }
			        }
			    }
			    if ($scope.AsgObj.length == 0) {
			        $scope.YesAssesment = true;
			        $scope.NoAssesment = false;
			    } else {
			        $scope.YesAssesment = false;
			        $scope.NoAssesment = true;
			    }
			    
			    $scope.isLoadAsgData = true;
			});
	}
	$scope.GetHomeDetails = function (columnValues) {
	    if (!sessionStorage.UserInfo) {
	        $scope.$on('GotUserDetails', function () {;
	            var UserDetails = JSON.parse(sessionStorage.UserInfo)
	            $scope.uid = UserDetails.userID;
	            $scope.colval = columnValues;
	            $scope.GetAssesmentDetails(columnValues);
	        });
	    } else {
	        var UserDetails = JSON.parse(sessionStorage.UserInfo)
	        $scope.uid = UserDetails.userID;
	        $scope.colval = columnValues;
	        $scope.GetAssesmentDetails(columnValues);
	    }
	}
	$scope.navToMasterColMap = function (id) {
	    if (id == 1) {
	        $location.path("/BaseKPIColMap/" + $routeParams.AssessmentId);
	    } else {
	        $location.path("/MasterColMap/" + $routeParams.AssessmentId);
	    }
	}
	$scope.RequestInfoclick = function () {
		sessionStorage.RequestId = "null"
		$location.path("/RequestDrillDown");

	}
	$scope.GetHomeDetails($scope.columnValue);
	$scope.$on('handleaddWidgetroadcast', function () {
		$scope.saveDashboardSetting();
	});
	$scope.ChangeViewMode = function (columnValue) {

		mySharedService.GetchangeViewMode($scope, columnValue, $scope.GetHomeDetails);
	}
	$scope.$on('handleaddUpdateDbSettingbroadcast', function () {
		$scope.saveDashboardSetting();
	});
	function search(nameKey, myArray) {
		var pos = -1;
		for (var i = 0; i < myArray.length; i++) {
			if (myArray[i].Dashboard.Id === nameKey) {
				pos = i;
				break;
			}
		}
		return pos;
	}
	$scope.ClearSessionstorage = function () {
		sessionStorage.RequestId = null;
	}
	$scope.saveDashboardSetting = function () {
		var url = uriApiURL + "api/Dashboard/SaveDashboardSettings";
		var userID = mySharedService.UserID;
		url = url + "?Asg_Id=" + $routeParams.AssessmentId + "&DashboardID=" + $scope.dashBoardID;
    	var Userval = "" + JSON.stringify(mySharedService.DbSettingJSON) + "";
        var UserDashboardData = { 'Userval': Userval };

        $http({
		method: 'POST',
               url: url,
			   params: {version: getEpocStamp()},
			   data: UserDashboardData,
			   contentType: "application/json",
			   dataType: "json"

            })
			.success(function (response) {
			    $scope.WholeData = angular.fromJson(response);
			    mySharedService.DashboardDetailsJSON.sizeX = $scope.WholeData.sizeX;
			    mySharedService.DashboardDetailsJSON.sizeY = $scope.WholeData.sizeY;
				mySharedService.DashboardDetailsJSON.Dashboard.Id = $scope.WholeData.Dashboard.Id;
				mySharedService.DashboardDetailsJSON.Dashboard.DashboardName = $scope.WholeData.Dashboard.DashboardName;
				mySharedService.DashboardDetailsJSON.DashboardIcon = $scope.WholeData.DashBoardIcon;
				Notification.success({
					verticalSpacing: 60,
					message: 'Settings saved successfully.'
				});
				var pos = search($scope.WholeData.Dashboard.Id, $scope.DashboardCollection);
				if (pos == -1) {
					$scope.DashboardCollection.push(mySharedService.DashboardDetailsJSON);
				} else {
					$scope.DashboardCollection[pos].Dashboard = mySharedService.DashboardDetailsJSON.Dashboard;
					$scope.DashboardCollection[pos].DashboardIcon = mySharedService.DashboardDetailsJSON.DashboardIcon;
					$scope.DashboardCollection[pos].ChoosenFilter = mySharedService.DashboardDetailsJSON.ChoosenFilter;
				}
			});
	};
	$scope.$on("handleDashboarddel", function () {
		console.log("DELETE DASHBOARD/ KPI/ WIDGET");
		mySharedService.DashboardCache= false;
		$scope.GetUserDashDetails(mySharedService.UserID, $scope.columnValue)
	})
	$scope.UserDBoardDetailModel = function (DBData) {
		var self = {};
		self.SizeXAxis = DBData.sizeX;
		self.SizeYAxis = DBData.sizeY;
		self.PositionXAxis = DBData.row;
		self.PositionYAxis = DBData.col;
		self.KPIID = DBData.Dashboard.Id;
		self.UserID = mySharedService.UserID;
		self.UserName = mySharedService.UserName;
		return self;
	};
	$(window).unbind('resize');
	$(window).bind('resize', function (e) {
		var newWidth;
		newWidth = $window.innerWidth;
		if (window.RT) {
			clearTimeout(window.RT);
		}
		if ($window.innerWidth > 1200) {
			sessionStorage.setItem("Resolution", "High");
			$scope.Resolution = "High";
			$scope.columnValue = 12;
			$scope.opacityValCom = "";
			$scope.opacityValue = "theme-text";
		} else {
			sessionStorage.setItem("Resolution", "Low");
			$scope.Resolution = "Low";
			$scope.columnValue = 8;
			$scope.opacityValCom = "theme-text";
			$scope.opacityValue = "";
		}
		window.RT = setTimeout(function () {
			
			mySharedService.GetchangeViewMode($scope, $scope.columnValue, $scope.GetHomeDetails);
		}, 200);
		$scope.oldWidth = $window.innerWidth;
	});
})