﻿var makeJSONSW = [];
var makeJSONSyn = [],counter=0;
appDashboard.controller('StopWordController', function ($scope,mySharedService, $routeParams, $uibModal, $http, $location,$timeout, limitToFilter, Notification, $window) {
    $scope.AssessmentId = $routeParams.AssessmentId;
    $scope.isStopLoadData = false;
    $scope.IsSynonymAvail = false;
    $scope.IsSynonymNotAvail = false;
    $scope.IsStopAvail = false;
    $scope.IsStopNotAvail = false;
     $scope.DownloadTickFlag = false;
     $scope.DownloadtoclusterData = function () {
         $scope.DownloadTickFlag = true;
        $http.get(uriApiURL + "api/Proffer/DownloadDatatoCluster?AsgId=" + $scope.AssessmentId)
               .then(function (response) {
                   $scope.DwnlData = angular.fromJson(response.data);
                   mySharedService.Json2CSV($scope.DwnlData, "Proc_Data");
                   $scope.DownloadTickFlag = false;
                   $scope.next();
               })
    }
    $scope.MFGetData = function () {
        $scope.isLoadAsgData = false;
        $http.get(uriApiURL + "api/Proffer/FetechFromDB?AsgID=" + $scope.AssessmentId)
        .then(function (response) {
            $scope.WholePreprocVal = angular.fromJson(response.data);
            if ($scope.WholePreprocVal.length != 0) {
                $timeout(function () {
                    $scope.GetStopWordTable($scope.WholePreprocVal);
                },0);
                $scope.IsStopAvail = true;
                $scope.IsStopNotAvail = false;
            } else if ($scope.WholePreprocVal.length == 0) {
                $scope.IsStopAvail = false;
                $scope.IsStopNotAvail = true;
            }          
           
            $scope.isLoadAsgData = true;
            $scope.isStopLoadData = true;
        })
    }
    $scope.MFGetData();
    $scope.GetStopWordTable = function (data) {
        var isChecks;
        $('#stopWordTable').handsontable({
            data: data,
            colHeaders: function (col) {
                var txt;
                switch (col) {
                    case 0:
                        return 'Word';
                    case 1:
                        return 'Count';
		    case 2:
                        return 'Length';
                    case 3:
                        txt = "<input type='checkbox' id='stopchecker' class='checker' ";
                        txt += isChecks ? 'checked="checked"' : '';
                        txt += "> Remove Stop Word";
                        return txt;
                    case 4:
                        return 'Rep_word';
                }
            },
            columnHeaderHeight: 20,
            height: 350,
            columnSorting: true,
            sortIndicator: true,
            stretchH: "all",
			search: true,
            colWidths: [40,40,40,40,40],
            columns: [                
                {
                    data: 'Word',
                    readOnly: true
                },
                {
                    data: 'Count',
                    readOnly: true
                },
                {
                    data: 'Length',
                    readOnly: true
                },				  
                {
                    data: 'Remove_Word',
                    type: "checkbox",
                    checkedTemplate: '1',
                    uncheckedTemplate: '0',
                    className: 'htCenter'
                },
                {
                    data: 'Rep_word'
                    
                }
            ]
        });
        ht = $("#stopWordTable").handsontable("getInstance");
        $("#stopWordTable").on('mousedown', 'input.checker', function (event) {
            event.stopPropagation();
        });
        $("#stopWordTable").on('mouseup', 'input.checker', function (event) {
            event.stopPropagation();
            isChecks = !$(this).is(':checked');
            if (isChecks == true) {
                $scope.Stpchckval = '1';
            } else {
                $scope.Stpchckval = '0';
            }
            var ht = $("#stopWordTable").handsontable("getInstance");
            var update = [];
            var rows = ht.countRows();
            for (i = 0; i < rows; i++) {
                update.push([i, 3, $scope.Stpchckval]);
            }
            ht.setDataAtCell(update);
            ht.render();
        });
        $scope.StopRows = ht.countRows();
    }
   
    $scope.next=function()
    {
        $http.get(uriApiURL + "api/Proffer/SetAsgDataProc?AsgID=" + $scope.AssessmentId + "&Condition=2&Type=1")
        .then(function (response) {
            var tab = ($scope.ActTab == 3) ? $scope.ActTab : $scope.ActTab + 1;
            $scope.ChangeStep(3, tab);
        })
    }
   
    $scope.Process = function () {
       
        $scope.isLoadPreProcess = false;
        var $container = $('#stopWordTable');
        var counter = 0;
        var ErrorWord = "";
        var tblStopWords = [], tblSynWords = [];
        var ht = $container.handsontable('getInstance');
        var htContents = ht.getData();
        for (var i = 0; i < htContents.length; i++) {                
            if (htContents[i][3] == '1') {
                tblStopWords.push({
                    ASG_ID: $scope.AssessmentId,
                    Stop_Word: "" + htContents[i][0] + "",
                    Remove_word: '1'
                })
            }
            if (htContents[i][4] != '') {
                tblSynWords.push({
                    ASG_ID: $scope.AssessmentId,
                    Used_Word: "" + htContents[i][0] + "",
                    Replacement_Word: "" + htContents[i][4] + ""
                })
            }
        }
        if (counter == 0 && (tblSynWords.length != 0 || tblStopWords.length != 0)) {
            var ChangePreWrd = {};     
            ChangePreWrd.StopWords = tblStopWords;
            ChangePreWrd.Synonyms = tblSynWords;
            $http({
                url: uriApiURL + "api/Proffer/UpdatepyDB",
                dataType: 'json',
                method: 'POST',
                data: ChangePreWrd,
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .success(function (response) {
                $scope.isLoadPreProcess = true;
                //$scope.MFGetData();
                Notification.success({ message: 'Changes saved successfully' });

            })
        } else if (tblSynWords.length == 0 && tblStopWords.length == 0) {
            $scope.isLoadPreProcess = true;
            Notification.error({ message: 'No data changed' });
        }
    }
})