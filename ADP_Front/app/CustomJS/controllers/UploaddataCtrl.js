﻿appDashboard.controller('UploadDataController', function ($scope, $http,$window, $routeParams, mySharedService, $location, Notification) {
    $scope.DownlData = [], $scope.DwnTimeData = [];
    $scope.IsReqMaster = false;
    if ($scope.ActTab > 1) {
        $scope.IsReqMaster = true;
    }
   
    var userID;
    $scope.SharedName = " ";
    $scope.AsgFileName = mySharedService.AsgName;
    $scope.AssessmentId = $routeParams.AssessmentId;
    $scope.IS_Shared = mySharedService.Is_Shared;
    $scope.SharedName = mySharedService.UserName;    
    $scope.SharedUser = [];
    $scope.isUploadData = true;
    $scope.DownloadFormat = function (val) {
        $scope.Filecond = val;
        $window.open(uriApiURL + "api/Proffer/Downloadsample?Cond=" + $scope.Filecond,"_blank");
        
    }
	$scope.uploadDumpclick = function() {
		setTimeout(function() {
		    angular.element('#chooseFile').click();
		}, 0);
	};
	$scope.fileEffectChanged = function() {
		
	};
	
	$scope.uploadEffortclick = function() {
		setTimeout(function() {
			angular.element('#chooseTimeBook').click();
		}, 0);
	};
    $scope.loadTags = function ($query) {
        var url = uriApiURL + 'api/Dashboard/GetUserDataToMapData?Keyword=';
        return $http({
            method: 'GET',
            url: url + $query
        })
                .then(function (response) {
                    var WholeData = angular.fromJson(response.data.m_StringValue);
                    $scope.UserDetails = WholeData[0].SearchUser;
                    return $scope.UserDetails.filter(function (item) {
                        if (item.name.toLowerCase().indexOf($query.toLowerCase()) != -1)
                            return item;
                    });
                });
    };  
  
    $scope.UploadMasterData = function () {
        $scope.isUploadData = false;
        var file = $('#chooseFile').get(0).files[0];
        var formData = new FormData();
        formData.append('file', file);
        var file1 = $("#chooseTimeBook").get(0).files[0];
        formData.append('file', file1);
         if (file != undefined) {
            $scope.Filecond = 0;
        } else if (file1 != undefined) {
            $scope.Filecond = 1;
        }
        if (file == undefined && file1 == undefined) {
            Notification.error({
                message: 'No file to upload! Kindly select a file to Upload'
            });
            $scope.isUploadData = true;
        } else if ((file != undefined && file.name.split('.')[1] != 'xlsx') || (file1 != undefined && file1.name.split('.')[1] != 'xlsx')) {
            Notification.error({
                message: 'Kindly select a file in the given format!'
            });
            $scope.isUploadData = true;
        } else {
            $.ajax({
                type: "POST",
                url: uriApiURL + "api/Proffer/UploadData?AsgName=" + $scope.AsgFileName + "&AsgID=" + $scope.AssessmentId + "&Cond=" + $scope.Filecond + "&IsStepVal=2",
                data: formData,
                success: function (response) {
                    var data = response;
                    if (data != "Error" && data != "Duplicates") {
                        var tab = ($scope.ActTab == 3) ? $scope.ActTab : $scope.ActTab + 1;                        
                        $scope.ChangeStep(2, tab);
                        $scope.isUploadData = true;
                        Notification.success({ message: 'Upload successful' });
                    } else if (data == "Error") {
                        Notification.error({ message: 'Sorry! An error occurred while uploading the file! Kindly Check the table formats' });
                        $scope.isUploadData = true;
                    } else if (data == "Duplicates") {
                        Notification.error({ message: 'Duplicate tickets found in the file uploaded! Please correct and re-upload' });
                        $scope.isUploadData = true;
                    }
                },
                error: function (data) {
                    Notification.error({ message: 'Sorry! An error occurred while uploading the file! Kindly Check the table formats' });
                    $scope.isUploadData = true;
                },
                contentType: false,
                processData: false
            });
        }
    }
    $scope.isUploadData = true;

    $scope.UploadToValidate = function () {
        $scope.isUploadData = false;
        var file = $('#chooseFile').get(0).files[0];
        var formData = new FormData();
        formData.append('file', file);
        var file1 = $("#chooseTimeBook").get(0).files[0];
        formData.append('file', file1);
         if (file != undefined && file1 == undefined) {
            $scope.Filecond = 1;
        } else if (file1 != undefined && file == undefined) {
            $scope.Filecond = 0;
        }
        if (file == undefined && file1 == undefined) {
            Notification.error({
                message: 'No file to upload! Kindly select a file to Upload'
            });
            $scope.isUploadData = true;
        } else if ((file != undefined && file.name.split('.')[1] != 'xlsx') || (file1 != undefined && file1.name.split('.')[1] != 'xlsx')) {
            Notification.error({
                message: 'Kindly select a file in the given format!'
            });
            $scope.isUploadData = true;
        } else {
            $.ajax({
                type: "POST",
                url: uriApiURL + "api/Proffer/UploadToValidate?AsgName=" + $scope.AsgFileName + "&AsgID=" + $scope.AssessmentId + "&Cond=" + $scope.Filecond,
                data: formData,
                success: function (response) {
                    $scope.Result = angular.fromJson(response);
                    if ($scope.Result.Status == "FAIL") {
                        Notification.error({ message: $scope.Result.Message });
                        var content = $scope.Result.Message;
                        var a = document.createElement('a');
                        var blob = new Blob([content], { 'type': 'text/plain' });
                        a.href = window.URL.createObjectURL(blob);
                        a.download = "Error" + '.txt';
                        a.click();                        
                    }
                    else
                    {
                        Notification.success({ message: $scope.Result.Status });
                    }                  
                    $scope.isUploadData = true;
                  //  }
                },
                error: function (data) {
                    Notification.error({ message: 'Sorry! An error occurred while uploading the file! Kindly use the table formats specified!Dates should be in "dd-mm-yy hh:mm:sss" format.' });
                    $scope.isUploadData = true;
                },
                contentType: false,
                processData: false
            });
        }
    }

    $scope.Save = function () {
        var sendData = { "Cond": $scope.PrivacySetting, "List": $scope.SharedUser, "Name": $scope.AsgFileName }
        var Userval = "" + JSON.stringify(sendData) + "";
        var UserData = { 'Userval': Userval };

        $http({
            url: uriApiURL + "api/Proffer/SaveAsg?AsgID=" + $scope.AssessmentId,
            dataType: 'json',
            method: 'POST',
            data: UserData,
            headers: {
                "Content-Type": "application/json"
            }
        })
        .success(function (response) {
            $scope.AssessmentId = angular.fromJson(response);
            mySharedService.AsgName = $scope.AsgFileName;
            Notification.success({ message: 'Settings Saved Successfully' });
            $location.path("/proc/" + $scope.AssessmentId);
        })
    };

    $scope.ChangePrivacySetting = function (privacy, type) {
        if (type == "0") {
            $scope.IsInPrivate = true;
            $scope.IsInPublic = false;
            $scope.IsInShared = false;
        } else if (type == "1") {
            $scope.IsInPrivate = false;
            $scope.IsInPublic = true;
            $scope.IsInShared = false;
        } else {
            $scope.IsInPrivate = false;
            $scope.IsInPublic = false;
            $scope.IsInShared = true;

        }
    }
    if ($scope.IS_Shared == 0) {
        $scope.PrivacySetting = "0"
        $scope.IsInPrivate = true;
        $scope.IsInPublic = false;
        $scope.IsInShared = false;
    } else if ($scope.IS_Shared == 1) {
        $scope.PrivacySetting = "1"
        $scope.IsInPrivate = false;
        $scope.IsInPublic = true;
        $scope.IsInShared = false;
    } else {
        $scope.PrivacySetting = "2"
        $scope.IsInPrivate = false;
        $scope.IsInPublic = false;
        $scope.IsInShared = true;

        if ($scope.SharedName != " ") {

            for (var i = 0; i < $scope.SharedName.length; i++) {
                $scope.SharedUser.push($scope.SharedName[i]);
            }
        }

    }
    
})