﻿appDashboard.controller('ProcController', function ($scope, $routeParams, $rootScope, $http, mySharedService, $uibModal, Notification) {
    $scope.isDMVActive = 1;
    $scope.ProcSteps = [{
        id: '1'
    }];
    $scope.LiElements = [{
            name: "Upload",
            tabID: 1,
            classNm: "",
            icon: "ti-upload"
        },
        {
            name: "View",
            tabID: 2,
            classNm: "",
            icon: "ti-eye"
        },
        {
            name: "Cluster",
            tabID: 3,
            classNm: "",
            icon: "ti-flag-alt"
        }];
    $rootScope.AcceptTck = 0;
    $rootScope.NotClustered = 0;
    $rootScope.UnCatTck = 0;
    $rootScope.ActDrillVal = [];
    mySharedService.UserName = '';
    $rootScope.SndDrillVal = [];
    $rootScope.MarkDrillVal = [];
    $rootScope.NotClusteredVal = [];
    $rootScope.ReClustTck = 0;
    $scope.isLoadAsgData = true;
    $scope.AssessmentId = $routeParams.AssessmentId;
    $scope.ChangeStep = function (val, Act) {
		$scope.ActTabProg = val;
        if (val <= Act) {
            for (var i = 0; i < $scope.LiElements.length; i++) {
                if ($scope.LiElements[i].tabID == val) {
                    $scope.LiElements[i].classNm = "CurrentTab";
                } else {
                    $scope.LiElements[i].classNm = "";
                }
            }
            $scope.ActTab = Act;
			
			 if ( Act == 1) {
				$scope.ActTabProgress = 18;
				
			 }else if ( Act == 2) {
				$scope.ActTabProgress = 50;
			 }else if ( Act == 3) {
				$scope.ActTabProgress = 82; 
			 }
			//console.log("ActTab " + $scope.ActTab)
            $scope.ProcSteps = [];
            $scope.ProcSteps = [{
                id: val
            }];
        }
    }
    $scope.setTckVal = function (acpt, uncat, recl, ntclu) {
        $rootScope.AcceptTck = acpt;
        $rootScope.UnCatTck = uncat;
        $rootScope.ReClustTck = recl;
        $rootScope.NotClustered = recl;
    }
    $scope.MFProcData = function () {
        $scope.isLoadAsgData = false;
        $http.get(uriApiURL + "api/Proffer/GetAsgProcDetails?AsgId=" + $scope.AssessmentId)
            .then(function (response) {
                $scope.ProcSteps = [];
                $scope.StepCls = angular.fromJson(response.data);
                $scope.ActTab = $scope.StepCls.ActTab;
                $scope.AsgName = $scope.StepCls.AsgName;
                mySharedService.AsgName = $scope.AsgName;
                mySharedService.Is_Shared = $scope.StepCls.Is_Shared;
                mySharedService.UserName = $scope.StepCls.List;
                $scope.AsgFileName = $scope.AsgName;
                $scope.IsReqMaster = $scope.StepCls.UploadMaster;
                $scope.ProcSteps = [{
                    id: $scope.ActTab
                }];
                $scope.TotalTck = $scope.StepCls.Total;
                $rootScope.AcceptTck = $scope.StepCls.AcceptTc;
                $rootScope.NotClustered = $scope.StepCls.NotClustered;
                $rootScope.UnCatTck = $scope.StepCls.UnCategTck;
                $rootScope.ReClustTck = $scope.StepCls.ReCluster;
                $scope.ChangeStep($scope.StepCls.ActStep, $scope.ActTab);
                $scope.isLoadAsgData = true;
            })
    }
    $scope.GetTicketDetails = function (val) {
        if ((val == 0 && $rootScope.AcceptTck != 0) || (val == 1 && $rootScope.UnCatTck != 0) || (val == 2 && $rootScope.ReClustTck != 0) || (val == 3 && $rootScope.NotClustered != 0)) {
            $scope.TckTypeVal = val;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'GetTckDrillDown.html',
                controller: 'GetDrillDownModalController',
                backdrop: 'static',
                size: 'lg', //size
                scope: $scope
            })
        }
    }
	
	
	
    $scope.MFProcData();
	
	
	$scope.GetDropdownValues = function () {
            var url = uriApiURL + 'api/Dashboard/GetRequestIDCollections?Keyword=';
            $scope.getsuggesstion = function (sug) {
                return $http.get(url + sug, {
                        SearchJson: sug
                    })
                    .then(function (response) {
                        var WholeData = angular.fromJson(response.data.m_StringValue);
                        var SearchData = WholeData.SearchData;
                        $scope.isFetchData = false;
                        return limitToFilter(SearchData, 5);
                    })
            };
        }
		$scope.GetDropdownValues();


})