appDashboard.controller('dashSettingCtrl', function ($scope, translationService, $timeout, $window, $rootScope, $http, $routeParams, $uibModal, mySharedService, $location, Notification) {

$scope.date = {
        startDate: moment().subtract(100, "days"),
        endDate: moment()
    };
    $scope.singleDate = moment();
	
	$scope.WidgetObj = [
        	{"name":"Average Response Time Analysis","avaial" : "success", value:"BKPI0001" , "space" : 3},
			{"name":"Average Resolution Time Analysis","avaial" : "warning", value:"BKPI0008" , "space" : 3},
			{"name":"Response Violation Trend","avaial" : "success", value:"BKPI0005"  , "space" : 3},
        	{"name":"Resolution Violation Trend","avaial" : "warning", value:"BKPI0006"  , "space" : 3},
        	{"name":"Incoming vs Completed Trend","avaial" : "success", value:"BKPI0002" , "space" : 3},
        	{ "name": "Aging Analysis", "avaial": "success", value: "BKPI0009", "space": 3 },
        	{"name":"Word Cloud","avaial" : "success", value:"BKPI0009"  , "space" : 3},
			{ "name": "Heat Map Daily", "avaial": "success", value: "BKPI0009", "space": 3 },
			{ "name": "Heat Map Hourly", "avaial": "success", value: "BKPI0009", "space": 6 },
        	{"name":"Pareto Analysis","avaial" : "success", value:"BKPI0003" , "space" : 6},
        	{"name":"Pivot Table","avaial" : "success", value:"BKPI0004" , "space" : 6},
        	{"name": "ScoreCard", "avaial": "success", value: "BKPI0007", "space": 6 },
        ];

    $scope.opts = {
        locale: {
            applyClass: 'btn-green',
            applyLabel: "Apply",
            fromLabel: "From",
            format: "YYYY-MM-DD",
            toLabel: "To",
            cancelLabel: 'Cancel',
            customRangeLabel: 'Custom range'
        },
        ranges: {
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()]
        }
    };

    $scope.setStartDate = function () {
        $scope.date.startDate = moment().subtract(4, "days").toDate();
    };

    $scope.setRange = function () {
        $scope.date = {
            startDate: moment().subtract(5, "days"),
            endDate: moment()
        };
    };

    //Watch for date changes
    $scope.$watch('date', function(newDate) {
        console.log('New date set: ', newDate);
    }, false);


});
/*************** APPSETTINGS *************/
/*************** APPSETTINGS *************/
/*************** APPSETTINGS *************/

appDashboard.controller('appSettingCtrl', function ($scope, translationService, $timeout, $window, $rootScope, $http, $routeParams, $uibModal, mySharedService, $location, Notification) {



});



