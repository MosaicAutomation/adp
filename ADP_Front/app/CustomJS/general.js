function getEpocStamp(){

	var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
         /* testing differences between getTime/1000 vs. subtracting milliseconds vs. parseInt(getTime/1000) */
	  var d1 = new Date();
	  var d1t0 = d1.getTime()/1000;
	  var d1t1 = (d1.getTime()-d1.getMilliseconds())/1000;
	  var d1t2 = parseInt(d1.getTime()/1000);
	  var msg = 'Testing various methods:'
	  +'\n1) Date.getTime()/1000                          = '+d1t0
	  +'\n2) (Date.getTime()-Date.getMilliseconds())/1000 = '+d1t1
	  +'\n3) parseInt(Date.getTime()/1000)                = '+d1t2
	  //alert(msg)
	  return d1t2
	  
    }
    else  // If another browser, return 0
    {
      return "1.0"
    }
  
}
function get_browser(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return {name:'IE',version:(tem[1]||'')};
        }   
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
      name: M[0],
      version: M[1]
    };
 }
function sortFunc(a, b) {
    if (a.row < b.row)  return -1;
    else if (a.row > b.row)  return 1;
    else {
        if (a.col > b.col)  return 1;
        else if (a.col < b.col)  return -1;        
        return 0;
    }
}

function blend(color1, color2, percent){
    if (color1.length > 7) return blendRGBColors(color1,color2,percent);
    else return blendColors(color1,color2,percent);
}

function blendColors(c0, c1, p) {
    var f=parseInt(c0.slice(1),16),t=parseInt(c1.slice(1),16),R1=f>>16,G1=f>>8&0x00FF,B1=f&0x0000FF,R2=t>>16,G2=t>>8&0x00FF,B2=t&0x0000FF;
    return "#"+(0x1000000+(Math.round((R2-R1)*p)+R1)*0x10000+(Math.round((G2-G1)*p)+G1)*0x100+(Math.round((B2-B1)*p)+B1)).toString(16).slice(1);
}

function blendRGBColors(c0, c1, p) {
    var f=c0.split(","),t=c1.split(","),R=parseInt(f[0].slice(4)),G=parseInt(f[1]),B=parseInt(f[2]);
    return "rgb("+(Math.round((parseInt(t[0].slice(4))-R)*p)+R)+","+(Math.round((parseInt(t[1])-G)*p)+G)+","+(Math.round((parseInt(t[2])-B)*p)+B)+")";
}

function getColorArray(IsColor, dbColor, Bclr, Aclr, val) {
	var colorarray = [];
	if (IsColor == true) {
		colorarray = generateColorGradient(Bclr, Aclr, val);
	} else {
		colorarray = dbColor;
	}
	return colorarray;
}

function getKPISize(KPIID, KPICollections, SizeObjX, SizeObjY) {
	var pos, size = [];
	for (var i = 0; i < KPICollections.length; i++) {
		if (KPICollections[i].KPI.Id == KPIID) {
			var x = KPICollections[i].sizeX
			var y = KPICollections[i].sizeY
			for (var j = 0; j < SizeObjX.length; j++) {
				if (SizeObjX[j].name == x) {
					pos = j;
					break;
				}
			}
			size.push(SizeObjX[pos])
			for (var j = 0; j < SizeObjY.length; j++) {
				if (SizeObjY[j].name == y) {
					pos = j;
					break;
				}
			}
			size.push(SizeObjY[pos])
		}
	}
	return size;
}

function returnThemeColor(color) {

    switch (color) {
        case 'amber':
            val = 'FBE3B6';
            break;
		case 'red':
            val = 'E51400';
            break;	
        case 'blue':
            val = 'A4D9F3';
            break;
        case 'brown':
            val = 'D9B999';
            break;
        case 'cobalt':
            val = '99B9F9';
            break;
        case 'crimson':
            val = 'DA99A8';
            break;
        case 'cyan':
            val = 'A4D9F3';
            break;
        case 'magenta':
            val = 'EF99C7';
            break;
        case 'lime':
            val = 'DAE6B0';
            break;
        case 'indigo':
            val = 'C399FF';
            break;
        case 'green':
            val = 'ADD6AD';
            break;
        case 'emerald':
            val = '99D099';
            break;
        case 'mango':
            val = 'F9D59D';
            break;
        case 'mauve':
            val = 'C8BFD0';
            break;
        case 'olive':
            val = 'C5CFC1';
            break;
        case 'orange':
            val = 'FDC399';
            break;
        case 'pink':
            val = 'F5C6E3';
            break;
        case 'sienna':
            val = 'CAB0B2';
            break;
        case 'steel':
            val = 'C1C8CF';
            break;
        case 'teal':
            val = '99DDDD';
            break;
        case 'violet':
            val = 'DA99FF';
            break;
        case 'yellow':
            val = 'EFE699';
            break;

    }
    return (val);
}

function generateColorGradient(ThemeColor, TileColor, noOfSteps) {
	if (TileColor == 'amber') {
		return generateGradientArray('FBE3B6', 'A97208', noOfSteps);
	} else if (TileColor == 'blue') {
		return generateGradientArray('A4D9F3', '13719E', noOfSteps);
	} else if (TileColor == 'brown') {
		return generateGradientArray('D9B999', '703800', noOfSteps);
	} else if (TileColor == 'cobalt') {
		return generateGradientArray('99B9F9', '0038A7', noOfSteps);
	} else if (TileColor == 'crimson') {
		return generateGradientArray('DA99A8', '71001A', noOfSteps);
	} else if (TileColor == 'cyan') {
		return generateGradientArray('A4D9F3', '13709E', noOfSteps);
	} else if (TileColor == 'magenta') {
		return generateGradientArray('EF99C7', '970050', noOfSteps);
	} else if (TileColor == 'lime') {
		return generateGradientArray('DAE6B0', '718728', noOfSteps);
	} else if (TileColor == 'indigo') {
		return generateGradientArray('C399FF', '4A00B2', noOfSteps);
	} else if (TileColor == 'green') {
		return generateGradientArray('ADD6AD', '246B24', noOfSteps);
	} else if (TileColor == 'emerald') {
		return generateGradientArray('99D099', '006100', noOfSteps);
	} else if (TileColor == 'mango') {
		return generateGradientArray('F9D59D', 'A86906', noOfSteps);
	} else if (TileColor == 'mauve') {
		return generateGradientArray('C8BFD0', '534260', noOfSteps);
	} else if (TileColor == 'olive') {
		return generateGradientArray('C5CFC1', '4C5E46', noOfSteps);
	} else if (TileColor == 'orange') {
		return generateGradientArray('FDC399', 'AF4901', noOfSteps);
	} else if (TileColor == 'pink') {
		return generateGradientArray('F5C6E3', 'A14F81', noOfSteps);
	} else if (TileColor == 'red') {
		return generateGradientArray('F5A199', 'A00E00', noOfSteps);
	} else if (TileColor == 'sienna') {
		return generateGradientArray('CAB0B2', '56292B', noOfSteps);
	} else if (TileColor == 'steel') {
		return generateGradientArray('C1C8CF', '47535F', noOfSteps);
	} else if (TileColor == 'teal') {
		return generateGradientArray('99DDDD', '007876', noOfSteps);
	} else if (TileColor == 'violet') {
		return generateGradientArray('DA99FF', '7100B2', noOfSteps);
	} else if (TileColor == 'yellow') {
		return generateGradientArray('EFE699', '978701', noOfSteps);
	}
}

function generateColorGradientFromBaseTheme(ThemeColor, TileColor, noOfSteps) {
	if (ThemeColor == 'dark')
	{
	   ThemeColor = 'FFFFFF';
	}
	else{
	   ThemeColor = 'FFFFFF';
	}
	if (TileColor == 'amber') {
		return generateGradientArray(ThemeColor, 'A97208', noOfSteps);
	} else if (TileColor == 'blue') {
		return generateGradientArray(ThemeColor, '13719E', noOfSteps);
	} else if (TileColor == 'brown') {
		return generateGradientArray(ThemeColor, '703800', noOfSteps);
	} else if (TileColor == 'cobalt') {
		return generateGradientArray(ThemeColor, '0038A7', noOfSteps);
	} else if (TileColor == 'crimson') {
		return generateGradientArray(ThemeColor, '71001A', noOfSteps);
	} else if (TileColor == 'cyan') {
		return generateGradientArray(ThemeColor, '13709E', noOfSteps);
	} else if (TileColor == 'magenta') {
		return generateGradientArray(ThemeColor, '970050', noOfSteps);
	} else if (TileColor == 'lime') {
		return generateGradientArray(ThemeColor, '718728', noOfSteps);
	} else if (TileColor == 'indigo') {
		return generateGradientArray(ThemeColor, '4A00B2', noOfSteps);
	} else if (TileColor == 'green') {
		return generateGradientArray(ThemeColor, '246B24', noOfSteps);
	} else if (TileColor == 'emerald') {
		return generateGradientArray(ThemeColor, '006100', noOfSteps);
	} else if (TileColor == 'mango') {
		return generateGradientArray(ThemeColor, 'A86906', noOfSteps);
	} else if (TileColor == 'mauve') {
		return generateGradientArray(ThemeColor, '534260', noOfSteps);
	} else if (TileColor == 'olive') {
		return generateGradientArray(ThemeColor, '4C5E46', noOfSteps);
	} else if (TileColor == 'orange') {
		return generateGradientArray(ThemeColor, 'AF4901', noOfSteps);
	} else if (TileColor == 'pink') {
		return generateGradientArray(ThemeColor, 'A14F81', noOfSteps);
	} else if (TileColor == 'red') {
		return generateGradientArray(ThemeColor, 'A00E00', noOfSteps);
	} else if (TileColor == 'sienna') {
		return generateGradientArray(ThemeColor, '56292B', noOfSteps);
	} else if (TileColor == 'steel') {
		return generateGradientArray(ThemeColor, '47535F', noOfSteps);
	} else if (TileColor == 'teal') {
		return generateGradientArray(ThemeColor, '007876', noOfSteps);
	} else if (TileColor == 'violet') {
		return generateGradientArray(ThemeColor, '7100B2', noOfSteps);
	} else if (TileColor == 'yellow') {
		return generateGradientArray(ThemeColor, '978701', noOfSteps);
	}
	else{
		var clrTileColor = TileColor.replace("#", "");
		return generateGradientArray(ThemeColor, clrTileColor, noOfSteps);
	}
}

function generateGradientArray(colorA, colorB, steps) {
	var result = [];
	colorA = this.convertHexToRgb(colorA); // [r,g,b]
	colorB = this.convertHexToRgb(colorB); // [r,g,b]
	steps -= 1; // Reduce the steps by one because we're including the first item manually
	// Calculate the intervals for each color
	var rStep = (Math.max(colorA[0], colorB[0]) - Math.min(colorA[0], colorB[0])) / steps;
	var gStep = (Math.max(colorA[1], colorB[1]) - Math.min(colorA[1], colorB[1])) / steps;
	var bStep = (Math.max(colorA[2], colorB[2]) - Math.min(colorA[2], colorB[2])) / steps;
	result.push('#' + this.convertRgbToHex(colorA));
	// Set the starting value as the first color value
	var rVal = colorA[0],
		gVal = colorA[1],
		bVal = colorA[2];
	// Loop over the steps-1 because we're including the last value manually to ensure it's accurate
	for (var i = 0; i < (steps - 1) ; i++) {
		// If the first value is lower than the last - increment up otherwise increment down
		rVal = (colorA[0] < colorB[0]) ? rVal + Math.round(rStep) : rVal - Math.round(rStep);
		gVal = (colorA[1] < colorB[1]) ? gVal + Math.round(gStep) : gVal - Math.round(gStep);
		bVal = (colorA[2] < colorB[2]) ? bVal + Math.round(bStep) : bVal - Math.round(bStep);
		result.push('#' + this.convertRgbToHex([rVal, gVal, bVal]));
	};
	result.push('#' + this.convertRgbToHex(colorB));
	return result;
}



function ColorLuminance(hex, lum) {

	// validate hex string
	hex = String(hex).replace(/[^0-9a-f]/gi, '');
	if (hex.length < 6) {
		hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
	}
	lum = lum || 0;

	// convert to decimal and change luminosity
	var rgb = "#", c, i;
	for (i = 0; i < 3; i++) {
		c = parseInt(hex.substr(i*2,2), 16);
		c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
		rgb += ("00"+c).substr(c.length);
	}

	return rgb;
}
function getWidgetSize() {

	var pos;

	for (var i = 0; i < $scope.KPICollections.length; i++) {
		if ($scope.KPICollections[i].KPI.Id == $scope.KPIID) {

			var x = $scope.KPICollections[i].sizeX
			var y = $scope.KPICollections[i].sizeY
			for (var j = 0; j < $scope.SizeObjX.length; j++) {
				if ($scope.SizeObjX[j].name == x) {
					pos = j;
					break;
				}
			}
			$scope.SizeXVal = $scope.SizeObjX[pos];
			for (var j = 0; j < $scope.SizeObjY.length; j++) {
				if ($scope.SizeObjY[j].name == y) {
					pos = j;
					break;
				}
			}
			$scope.SizeYVal = $scope.SizeObjY[pos];
		}
	}
}

function convertHexToRgb(hex) {
	var r, g, b;
	r = parseInt(hex.substr(0, 2), 16);
	g = parseInt(hex.substr(2, 2), 16);
	b = parseInt(hex.substr(4, 2), 16);
	return [r, g, b];
}

function convertRgbToHex(color) {
	// Set boundries of upper 255 and lower 0
	color[0] = (color[0] > 255) ? 255 : (color[0] < 0) ? 0 : color[0];
	color[1] = (color[1] > 255) ? 255 : (color[1] < 0) ? 0 : color[1];
	color[2] = (color[2] > 255) ? 255 : (color[2] < 0) ? 0 : color[2];
	return this.zeroFill(color[0].toString(16), 2) + this.zeroFill(color[1].toString(16), 2) + this.zeroFill(color[2].toString(16), 2);
}

function zeroFill(number, width) {
	width -= number.toString().length;
	if (width > 0) {
		return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
	}
	return number;
}

function convert2ddhhmm(minutesValue, suffix) {
	if (suffix != "") {
		if (suffix.toUpperCase() == "H") {
			minutesValue = minutesValue * 60;
		} else if (suffix.toUpperCase() == "D") {
			minutesValue = minutesValue * 60 * 8;
		}
		var hours = Math.floor(minutesValue / 60);
		var minutes = Math.round((minutesValue % 60), 2);
		var days = 0;
		if (hours > 8) {
			days = Math.floor(hours / 8);
			hours = Math.floor(hours % 8);
		}
		var returnVal = '';
		if (days != 0) {
			returnVal += days + 'd ';
		}
		if (hours != 0) {
			returnVal += hours + 'h ';
		}
		if (minutes != 0) {
			returnVal += minutes + 'm ';
		}
	} else {
		returnVal = minutesValue;
	}
	return returnVal;
}

function showPosition(position) {
	var xmlhttp1 = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	var xmlhttp2 = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	xmlhttp1.onreadystatechange = function () {
		if (xmlhttp1.readyState == 4 && xmlhttp1.status == 200) {
			document.getElementById("myDiv").innerHTML = xmlhttp1.responseText;
			var prefix = 'wi wi-';
			var code1 = JSON.parse(xmlhttp1.responseText);
			var code = JSON.parse(xmlhttp1.responseText).weather[0].id;
			var icon = weatherIcons[code].icon;

			// If we are not in the ranges mentioned above, add a day/night prefix.
			if (!(code > 699 && code < 800) && !(code > 899 && code < 1000)) {
				icon = 'day-' + icon;
			}
			// Finally tack on the prefix.
			icon = prefix + icon;
			document.getElementById("outro").innerHTML = '<p><i class="' + icon + '"></i><span>' + code1.main.temp + '&ordm;</span></p>';
		}
	}
	xmlhttp2.onreadystatechange = function () {
		if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
			var code = JSON.parse(xmlhttp2.responseText);
			document.getElementById("outro").innerHTML = document.getElementById("outro").innerHTML + "";
			var ff1 = 11;
		}
	}
	xmlhttp1.open("GET", "http://api.openweathermap.org/data/2.5/weather?id=1269135&APPID=c5b45ba59083990aa298b7f5f7976982&units=metric", true);
	xmlhttp1.send();
	xmlhttp2.open("GET", "http://api.openweathermap.org/data/2.5/forecast/daily?id=1269135&APPID=c5b45ba59083990aa298b7f5f7976982&units=metric&cnt=2", true);
	xmlhttp2.send();
}


function duplicateVal(a) {
	var counts = {};
	var duplicateVal = [];
	for (var i = 0; i <= a.length; i++) {
		if (counts.hasOwnProperty(a[i])) {
			duplicateVal.push(a[i]);
		} else {
			counts[a[i]] = 1;
		}
	}
	return duplicateVal;
}
$(document).ready(function () {

	function body_sizer() {
		var bodyheight = $(window).height() - $('footer').outerHeight();
		$("#allPage").height(bodyheight);
	}

	body_sizer();
	$(window).resize(body_sizer);

	$(window).scroll(function () {
		var y = $(this).scrollTop();
		if (y > 60) {
			$('.sidebar-toggle-btn').addClass("active");
		} else {
			$('.sidebar-toggle-btn').removeClass("active");
		}
	});/*

	var overlay = $('.sidebar-overlay');
	$('.sidebar-toggle').on('click', function () {
		var sidebar = $('#sidebar');
		sidebar.toggleClass('open');
		if ((sidebar.hasClass('sidebar-fixed-left') || sidebar.hasClass('sidebar-fixed-right')) && sidebar.hasClass('open')) {
			overlay.addClass('active');
		} else {
			overlay.removeClass('active');
		}
	});
	overlay.on('click', function () {
		$(this).removeClass('active');
		$('#sidebar').removeClass('open');
	});*/




	var sidebar = $('#sidebar');
	var sidebarHeader = $('#sidebar .sidebar-header');
	var sidebarImg = sidebarHeader.css('background-image');
	var toggleButtons = $('.sidebar-toggle');

	$('#sidebar-position').change(function () {
		var value = $(this).val();
		sidebar.removeClass('sidebar-fixed-left sidebar-fixed-right sidebar-stacked').addClass(value).addClass('open');
		if (value == 'sidebar-fixed-left' || value == 'sidebar-fixed-right') {
			$('.sidebar-overlay').addClass('active');
		}
		if (value != '') {
			toggleButtons.css('display', 'initial');
			$('body').css('display', 'initial');
		} else {
			toggleButtons.css('display', 'none');
			$('body').css('display', 'table');
		}
	});
	$('#sidebar-theme').change(function () {
		var value = $(this).val();
		sidebar.removeClass('sidebar-default sidebar-inverse sidebar-colored sidebar-colored-inverse').addClass(value);
	});
	$('#sidebar-header').change(function () {
		var value = $(this).val();
		$('.sidebar-header').removeClass('header-cover').addClass(value);
		if (value == 'header-cover') {
			sidebarHeader.css('background-image', sidebarImg);
		} else {
			sidebarHeader.css('background-image', '');
		}
	});
});
(function ($) {
	var dropdown = $('.dropdown');
	dropdown.on('show.bs.dropdown', function (e) {
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown();
	});
	dropdown.on('hide.bs.dropdown', function (e) {
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp();
	});
}(jQuery));
(function (removeClass) {
	jQuery.fn.removeClass = function (value) {
		if (value && typeof value.test === 'function') {
			for (var i = 0, l = this.length; i < l; i++) {
				if (window.CP.shouldStopExecution(2)) {
					break;
				}
				var elem = this[i];
				if (elem.nodeType === 1 && elem.className) {
					var classNames = elem.className.split(/\s+/);
					for (var n = classNames.length; n--;) {
						if (window.CP.shouldStopExecution(1)) {
							break;
						}
						if (value.test(classNames[n])) {
							classNames.splice(n, 1);
						}
					}
					window.CP.exitedLoop(1);
					elem.className = jQuery.trim(classNames.join(' '));
				}
			}
			window.CP.exitedLoop(2);
		} else {
			removeClass.call(this, value);
		}
		return this;
	};
}(jQuery.fn.removeClass));
//# sourceURL=pen.js