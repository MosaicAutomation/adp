﻿var makeJSON = [];


function duplicateVal(a) {
	var counts = {};
	var duplicateVal = [];
	for (var i = 0; i <= a.length; i++) {
		if (counts.hasOwnProperty(a[i])) {
			duplicateVal.push(a[i]);
		} else {
			counts[a[i]] = 1;
		}
	}
	return duplicateVal;
}

//.factory('authService', ['$http', '$q', 'localStorageService', 'ngAuthSettings', function ($http, $q, localStorageService, ngAuthSettings) {
//    //var serviceBase = ngAuthSettings.apiServiceBaseUri;
//    var authServiceFactory = {};
//    var _authentication = {
//        isAuth: false,
//        userName: "",
//        password: "",
//        useRefreshTokens: false
//    };
//    var _externalAuthData = {
//        provider: "",
//        userName: "",
//        externalAccessToken: ""
//    };
//    var _saveRegistration = function (registration) {
//        _logOut();
//        return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
//            return response;
//        });
//    };
//    var _login = function (loginData) {
//        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
//        if (loginData.useRefreshTokens) {
//            data = data + "&client_id=" + ngAuthSettings.clientId;
//        }
//        var deferred = $q.defer();
//        $http.post(uriApiURL + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
//            if (loginData.useRefreshTokens) {
//                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
//            }
//            else {
//                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false });
//            }
//            _authentication.isAuth = true;
//            _authentication.userName = loginData.userName;
//            _authentication.useRefreshTokens = loginData.useRefreshTokens;
//            deferred.resolve(response);
//        }).error(function (err, status) {
//            _logOut();
//            deferred.reject(err);
//        });
//        return deferred.promise;
//    };
//    var _logOut = function () {
//        localStorageService.remove('authorizationData');
//        _authentication.isAuth = false;
//        _authentication.userName = "";
//        _authentication.useRefreshTokens = false;
//    };
//    var _fillAuthData = function () {
//        var authData = localStorageService.get('authorizationData');
//        if (authData) {
//            _authentication.isAuth = true;
//            _authentication.userName = authData.userName;
//            _authentication.useRefreshTokens = authData.useRefreshTokens;
//        }
//    };
//    var _refreshToken = function () {
//        var deferred = $q.defer();
//        var authData = localStorageService.get('authorizationData');
//        if (authData) {
//            if (authData.useRefreshTokens) {
//                var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + ngAuthSettings.clientId;
//                localStorageService.remove('authorizationData');
//                $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
//                    localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
//                    deferred.resolve(response);
//                }).error(function (err, status) {
//                    _logOut();
//                    deferred.reject(err);
//                });
//            }
//        }
//        return deferred.promise;
//    };
//    var _obtainAccessToken = function (externalData) {
//        var deferred = $q.defer();
//        $http.get(serviceBase + 'api/account/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {
//            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
//            _authentication.isAuth = true;
//            _authentication.userName = response.userName;
//            _authentication.useRefreshTokens = false;
//            deferred.resolve(response);
//        }).error(function (err, status) {
//            _logOut();
//            deferred.reject(err);
//        });
//        return deferred.promise;
//    };
//    var _registerExternal = function (registerExternalData) {
//        var deferred = $q.defer();
//        $http.post(serviceBase + 'api/account/registerexternal', registerExternalData).success(function (response) {
//            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
//            _authentication.isAuth = true;
//            _authentication.userName = response.userName;
//            _authentication.useRefreshTokens = false;
//            deferred.resolve(response);
//        }).error(function (err, status) {
//            _logOut();
//            deferred.reject(err);
//        });
//        return deferred.promise;
//    };
//    authServiceFactory.saveRegistration = _saveRegistration;
//    authServiceFactory.login = _login;
//    authServiceFactory.logOut = _logOut;
//    authServiceFactory.fillAuthData = _fillAuthData;
//    authServiceFactory.authentication = _authentication;
//    authServiceFactory.refreshToken = _refreshToken;
//    authServiceFactory.obtainAccessToken = _obtainAccessToken;
//    authServiceFactory.externalAuthData = _externalAuthData;
//    authServiceFactory.registerExternal = _registerExternal;
//    return authServiceFactory;
//}])
//.controller('LoginControl', ['$scope', '$location', 'mySharedService', '$http', 'authService', 'ngAuthSettings', function ($scope, $location, mySharedService, $http, authService, ngAuthSettings) {
//    $scope.loginData = {
//        userName: "",
//        password: "",
//        useRefreshTokens: false
//    };
//    $scope.message = "";
//    $scope.login = function () {
//        authService.login($scope.loginData).then(function (response) {
//            mySharedService.LoginDetails = $scope.loginData;
//            console.log(mySharedService.LoginDetails)
//            $location.path('/main');
//        },
//         function (err) {
//             $scope.message = err.error_description;
//         });
//    };
//    $scope.authExternalProvider = function (provider) {
//        var redirectUri = location.protocol + '//' + location.host + '/authcomplete.html';
//        var externalProviderUrl = ngAuthSettings.apiServiceBaseUri + "api/Account/ExternalLogin?provider=" + provider
//                                                                    + "&response_type=token&client_id=" + ngAuthSettings.clientId
//                                                                    + "&redirect_uri=" + redirectUri;
//        window.$windowScope = $scope;
//        var oauthWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
//    };
//    $scope.authCompletedCB = function (fragment) {
//        $scope.$apply(function () {
//            if (fragment.haslocalaccount == 'False') {
//                authService.logOut(); 
//                authService.externalAuthData = {
//                    provider: fragment.provider,
//                    userName: fragment.external_user_name,
//                    externalAccessToken: fragment.external_access_token
//                };
//                $location.path('/associate');
//            }
//            else {
//                //Obtain access token and redirect to orders
//                var externalData = { provider: fragment.provider, externalAccessToken: fragment.external_access_token };
//                authService.obtainAccessToken(externalData).then(function (response) {
//                    $location.path('/orders');
//                },
//             function (err) {
//                 $scope.message = err.error_description;
//             });
//            }
//        });
//    }
//}])