appDashboard.directive('pageHeight', function ($window, $timeout) {
    return {
        link: function (scope, element, attrs) {
            $timeout(function () {

                var dheight = $(document).height(); //$document.height()
                var wheight = $($window).height();
                var heightChange = $('footer').outerHeight(); //$('header').outerHeight() +
                element.css('min-height', $window.innerHeight - heightChange);
                $($window).bind('resize', function () {
                    //  console.log("RESIZE");
                    var heightChange = $('footer').outerHeight()
                    element.css('min-height', $window.innerHeight - heightChange);
                    scope.$digest();
                });


            });
        }
    }
});



appDashboard.directive('fixCrappyIeSelect', function () {
    return {
        restrict: 'A',
        scope: {
            options: '=fixCrappyIeSelect'
        },
        controller: ['$scope', '$element', function ($scope, $element) {
            $scope.$watch('options', function () {
                $element.hide();
                $element.show();
            });
        }]
    };
});
appDashboard.directive("hrsToDays", [function () {
    return {
        template: '',
        restrict: 'A',
        scope: {
            hrsToDaysValue: '=hrsToDays'
        },
        link: function (scope, element, attrs) {

            // get the angle from the parent scope  
            var angle = scope.arrow;
            scope.$watch('hrsToDaysValue', function () {
                var pier = scope.hrsToDaysValue;
                var pier = Math.round(pier);
                var hours = Math.floor(pier / 60);
                var minutes = pier % 60;
                var timeret = "";
                if (hours > 0) {
                    if (hours < 24) {
                        timeret += hours + "h ";
                    } else {
                        var days = Math.floor(hours / 24);
                        hours = hours - (days * 24);
                        timeret += days + "d " + hours + "h ";
                    }
                }
                if (minutes > 0) {
                    timeret += minutes + "m ";
                }
                if (timeret == "") {
                    timeret = "-"
                }
                element.html(timeret);
            });

        }
    };
}]);
appDashboard.directive("pageBodyBgr", [function () {
    return {
        template: '',
        restrict: 'A',
        scope: {
            pageBodyBgrValue: '=pageBodyBgr'
        },
        link: function (scope, element, attrs) {

            // get the angle from the parent scope  
            var angle = scope.arrow;
            scope.$watch('pageBodyBgrValue', function () {
                // get the image to turn () 
                // element is the img 
                element.css({
                    'background': 'url(../MosaicDiscovery/Media/' + pageBodyBgrValue + '/1.jpg)  center center no-repeat fixed'
                });
            });

        }
    };
}]);




appDashboard.directive('themeColorChange', function ($window, $timeout) {
    return {
        template: '',
        restrict: 'A',
        scope: {
            themeColorChange: '=themeColorChange',
            steps: '=steps'
        },
        link: function (scope, element, attrs) {
            scope.$watch('themeColorChange', function () {

                function unique(list) {
                    var result = [];
                    $.each(list, function (i, e) {
                        if ($.inArray(e, result) == -1) result.push(e);
                    });
                    return result;
                }

                var classList = [];
                $timeout(function () {
                    var scrollable = element.find('span');
                    var cls = [];
                    $(scrollable).each(function () {
                        cls.push(this.className);
                    });
                    var clsnew = unique(cls);
                    scope.Applycolors = generateColorGradient('', scope.themeColorChange, clsnew.length);
                    scope.Applycolors.reverse();
                    //console.log("steps "+scope.steps +" clsnew " + clsnew);
                    $(clsnew).each(function (index) {

                        element.find('.' + this).css({
                            'color': scope.Applycolors[index]
                        });
                    });

                });

            });

        }
    };
});
appDashboard.directive('insertCssFill', function ($window, $timeout) {
    return {
        template: '',
        restrict: 'A',
        scope: {
            insertCssFill: '=insertCssFill'
        },
        link: function (scope, element, attrs) {
            scope.$watch('insertCssFill', function () {
                var theColorIs = returnThemeColor(scope.insertCssFill);
                var CSS = '<style> .treeMap.' + scope.insertCssFill + '.theme-fill rect { fill: #' + theColorIs + '  }</style>';
                element.empty();
                element.append(CSS)


            });

        }
    };
});
appDashboard.directive('slickMinWidth', function ($window, $timeout) {
    return {
        template: '',
        restrict: 'A',
        scope: {
            slickMinWidth: '=slickMinWidth'
        },
        link: function (scope, element, attrs) {
		 $timeout(function () {

               // var eleparwid = element.parent().width() -2;
				  var eleparwid = (element.closest('.slick-slider').width() / Number(scope.slickMinWidth))-10;
             //   console.log("eleparwid " + eleparwid );slider ng-isolate-scope slick-initialized 
				element.css('min-width', eleparwid);
                //var CSS = '<style>  .slickClassSlide {min-width: ' + eleparwid+'px ;}</style>';
                //element.empty();
               // element.append(CSS)

            },10);
		
           

        }
    };
});
appDashboard.directive('widgetSetHeight', function ($window, $timeout) {
    return {
        template: '',
        restrict: 'A',
        scope: {
            widgetSetHeight: '=widgetSetHeight'
        },
        link: function (scope, element, attrs) {
		 $timeout(function () {
			
            var setminHeight =  parseInt((element.width() / 100) * Number(scope.widgetSetHeight)) ;
			console.log("widgetSetHeight" + scope.widgetSetHeight + " " + setminHeight);
             element.css({
				'min-height':setminHeight+ 'px'
			});

         },1);
        }
    };
});
appDashboard.directive('cssFillDayHeat', function ($window, $timeout) {
    return {
        template: '',
        restrict: 'A',
        scope: {
            cssFillDayHeat: '=cssFillDayHeat'
        },
        link: function (scope, element, attrs) {
            scope.$watch('cssFillDayHeat', function () {
                var theColorIs = returnThemeColor(scope.cssFillDayHeat);
                scope.Applycolors = generateColorGradient('', scope.cssFillDayHeat, 9);
                // scope.Applycolors.reverse();
                scope.CSSContent = "";
                $.each($(scope.Applycolors), function (index) {
                    var ex = index + 1;
                    scope.CSSContent += ".cal-heatmap-container .q" + ex +
                        "{background-color: " + scope.Applycolors[index] + "; fill: " + scope.Applycolors[index] + "}";
                });
                var CSS = '<style> ' + scope.CSSContent + '</style>';
                element.empty();
                element.append(CSS)


            });

        }
    };
});

appDashboard.directive("ieCheckCarsouel", [function () {
    return {
        template: '',
        restrict: 'A',
        scope: {
            ieCheckCarsouel: '=ieCheckCarsouel'
        },
        link: function (scope, element, attrs) {		
            scope.$watch('ieCheckCarsouel', function () {
					if((scope.ieCheckCarsouel = "True") || (scope.ieCheckCarsouel = "true")){
					element.addClass('col-md-9');
					}
                    
            });

        }
    };
}]);

appDashboard.directive('scoreCardList', function ($document, $window) {
    return function (scope, element, attr) {
	
	$window.setTimeout(function () {
            //console.log('OP Self:   ' + element.width() + 'x' + element.height() + ' Parent: ' + element.parent().width() + 'x' + element.parent().height());
            var getEle = element.prev('.carsouelList'); 
			
			if ( $(getEle).hasClass( "col-md-9" ) ) {
				 element.addClass("col-md-3");
			}
			
			

        }, 100);
	
  /*      $window.setTimeout(function () {
            //console.log('OP Self:   ' + element.width() + 'x' + element.height() + ' Parent: ' + element.parent().width() + 'x' + element.parent().height());
            var getEle = element.prev('.carsouelList').width() 
			//console.log("getEle" + getEle);
			var scoreCardHeigh =parseInt( (getEle / 12) * 6);	
			console.log("getEle " + scoreCardHeigh);			
          element.css({
                'height': '550px'
            });

        }, 100);

*/
    };
});
appDashboard.directive('setHeight', function ($document, $window) {
    return function (scope, element, attr) {
        $window.setTimeout(function () {
            //console.log('OP Self:   ' + element.width() + 'x' + element.height() + ' Parent: ' + element.parent().width() + 'x' + element.parent().height());
            //var setminHeight = element.parent().height() - 40;
            var setminHeight = element.closest(".kpiGridster").height() - 40;
            var setminvar = setminHeight - element.parent().find('.swContainr').height();
            var setminvper = (setminvar / setminHeight) * 100;
            // console.log('OP setminHeight' + setminHeight);
            setminHeight = setminHeight - setminvar;
            setminHeight = setminHeight
            element.css({
                'min-height': setminvper + '%'
            });

        }, 1000);


    };
});


appDashboard.directive('dayHeatToolTipCheck', function ($window, $timeout) {
    return {
        template: '',
        restrict: 'A',
        scope: {
            dayHeatToolTipCheck: '=dayHeatToolTipCheck'
        },
        link: function (scope, element, attrs) {
            var telerep = element.closest(".parentC").find("#ch-tooltip-rep"); 
            var childElep = element.find(".cal-heatmap-container")
            var observer = new MutationObserver(function (mutations) {
                var elLeft = element.scrollLeft();
                var elOfLef = Number(childElep.offset.left) - Number(element.offset.left);
               // console.log("sdfsd " + elLeft + " " +elOfLef )
                var tempelement = element
                    .find(".ch-tooltip")
                    .clone();
                tempelement.css({left: element.find(".ch-tooltip").offset.left- elLeft });
              //  telerep.html(tempelement);
            });
            observer.observe(element[0], {
                childList: true,
                subtree: true
            });
        }
    };
});

appDashboard.directive('tooltipCheck', function ($window, $timeout) {
    return {
        template: '',
        restrict: 'A',
        scope: {
            tooltipCheck: '=tooltipCheck'
        },
        link: function (scope, element, attrs) {

            scope.$watch('tooltipCheck', function () {
                //console.log(" tooltipCheck " );
                var tempelement = element
                    .clone()
                    .css({
                        display: 'inline',
                        width: 'auto',
                        visibility: 'hidden'
                    })
                    .appendTo('body');
                if (tempelement.width() > $(element).width()) {
                    element.closest(".kpi-heading").find(".ng-isvisible").addClass("ng-hide");
                    element.removeClass("ng-invisible").addClass("ng-isvisible");
                    tempelement.remove();
                }
            });



        }
    };
});

appDashboard.directive('setMaxHeightIndeed', function ($document, $window) {
    return function (scope, element, attr) {
        $window.setTimeout(function () {
            var setminHeight = element.parent().height() - 40;
            element.css({
                'max-height': setminHeight + 'px !important'
            });

        }, 1);


    };
});
appDashboard.directive('setHeightIndeed', function ($document, $window) {
    return function (scope, element, attr) {
        $window.setTimeout(function () {
            var setminHeight = element.closest(".kpiGridster").height() - 40;
            var setminvar = setminHeight - element.parent().find('.swContainr ').height();
            var setminvper = (setminvar / setminHeight) * 100;
            setminHeight = setminHeight - setminvar;
            setminHeight = setminHeight
            element.css({
                'min-height': setminvper + '%'
            });


        }, 1000);
    };
});
appDashboard.directive('setMaxHeightGen', function ($document, $window) {
    return function (scope, element, attr) {
        $window.setTimeout(function () {
            var setminHeight = element.parent().height();
            //console.log("setminHeight " + setminHeight +" "+ element.parent().height());
            var setminvar = setminHeight - 40; //element.parent().find('.rightAlign').height()
            var setminvper = (setminvar / setminHeight) * 100;
            /*element.css({
            	'max-height': setminvper + '%'
            });*/


        }, 1000);
    };
});
appDashboard.directive('swatchGrpWidth', function ($document, $window) {

    return {
        restrict: 'EA',
        replace: false,
        link: function (scope, element, attrs, controller) {
            if (scope.$last === true) {
                $window.setTimeout(function () {

                    var elementList = element.closest(".swatchGrp");
                    var elementListItems = elementList.find('.btn1');
                    var elementsWidth = 0;
                    $.each($(elementListItems), function () {
                        elementsWidth = elementsWidth + $(this).outerWidth();
                    });
                    // console.log("RESPEES " + elementsWidth + " : " + element.closest(".swatchGrp").width());                     

                    if (elementsWidth > elementList.width()) {
                        elementList.addClass('mini');

                    }
                }, 1);

                $($window).bind('resize', function () {
                    var elementList = element.closest(".swatchGrp");
                    var elementListItems = elementList.find('.btn1');
                    var elementsWidth = 0;
                    $.each($(elementListItems), function () {
                        elementsWidth = elementsWidth + $(this).outerWidth();
                    });
                    // console.log("RESPEES " + elementsWidth + " : " + element.closest(".swatchGrp").width());                     

                    if (elementsWidth > elementList.width()) {
                        elementList.addClass('mini');
                    } else {
                        elementList.removeClass('mini');
                    }
                });

            }



        }
    }

});
appDashboard.directive('pageLoaderBlank', function () {
    return {
        restrict: 'A',
        template: '<div class="bgLoad">&nbsp;</div>',
        link: function (scope, element, attrs) {
            //console.log("pageLOADER");            
        }
    };
});
appDashboard.directive('pageLoaderSpin', function () {
    return {
        restrict: 'A',
        template: '<div class="bgrLoad"><div class="bgrLoadMessage" ><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div></div>',
        link: function (scope, element, attrs) {
            //console.log("pageLOADER");            
        }
    };
});
appDashboard.directive('kpiErrorMsgTwo', function () {
    return {
        restrict: 'A',
        template: '<p><a ng-click="openWidgetError()" > <i class="fa fa-info-circle" aria-hidden="true"></i>  </a> Insufficient Data or Wrong Query <br/>  <a ng-click="openWidgetModal(\'lg\',2,KPIID,KpiGroupType,KpiGroupID)" > <i class="fa fa-cogs" aria-hidden="true"></i> Change Settings </a> | <a ng-click="deleteDashboard(\'lg\',0,dashBoardID,KPIID)" title="Delete KPI" class="ng-binding"><i class="fa fa-times iconMD"></i> Delete  </a></p>',
        link: function (scope, element, attrs) {
                       
        }
    };
});

appDashboard.directive('pageLoaderSpinOnly', function () {
    return {
        restrict: 'A',
        template: '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>',
        link: function (scope, element, attrs) {
            //console.log("pageLOADER");            
        }
    };
});
appDashboard.directive('pageLoaderSpinAll', function () {
    return {
        restrict: 'A',
        template: '<div class="bgrLoad Fixed"><div class="bgrLoadMessage" ><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div></div>',
        link: function (scope, element, attrs) {
            //console.log("pageLOADER");            
        }
    };
});
appDashboard.directive('pageReorder', function () {
    return {
        restrict: 'A',
        templateUrl: 'app/views/directive/reorder.tpl.html',
        link: function (scope, element, attrs) {
            //console.log("pageLOADER");            
        }
    };
});

appDashboard.directive('settingsMenu', function () {
    return {
        restrict: 'A',
        templateUrl: 'app/views/settings/directive/menu.html',
        link: function (scope, element, attrs) {
            //console.log("pageLOADER");            
        }
    };
});
appDashboard.directive('assesmentMenu', function () {
    return {
        restrict: 'A',
        templateUrl: 'app/views/directive/assesmentMenu.html',
        link: function (scope, element, attrs) {
            //console.log("pageLOADER");            
        }
    };
});

appDashboard.directive('myTemplate', function ($rootScope) {
    return {
        templateUrl: function (elem, attr) {
            switch (attr.type) {
                case "1":
                    return 'app/views/KPI/RespResln.html';
                    break;
                case "2":
                    return 'app/views/KPI/InVsComp.html';
                    break;
                case "3":
                    return 'app/views/KPI/ParetoKPI.html';
                    break;
                case "4":
                    return 'app/views/KPI/AngularGridKPI.html';
                case "5":
                    return 'app/views/KPI/RespReslnExceedPer.html';
                case "6":
                    return 'app/views/KPI/RespReslnExceedPer.html';
                case "7":
                    return 'app/views/KPI/GeneralKPI.html';
                    break;
                case "8":
                    return 'app/views/KPI/RespResln.html';
                    break;
                case "9":
                    return 'app/views/KPI/AgingReport.html';
                    break;
                case "10":
                    return 'app/views/KPI/TreeMapping.html';
                    break;
                case "11":
                    return 'app/views/KPI/WordCloud.html';
                    break;
                case "12":
                    return 'app/views/KPI/AngularGridKPI.html';
                    break;
                case "13":
                    return 'app/views/KPI/HeatMap.html';
                    break;
                case "14":
                    return 'app/views/KPI/DayHeatMap.html';
                    break;
                case "15":
                    return 'app/views/KPI/DayHeatMap.html';
                    break;

            }
        }
    };
});
appDashboard.directive('processSteps', function ($rootScope) {
    return {
        templateUrl: function (elem, attr) {
            switch (attr.type) {
                case "1":
                    return 'app/views/_UploadData.html';
                    break;
                case "2":
                    return 'app/views/_StopWordData.html';
                    break;
                case "3":
                    return 'app/views/_Cluster.html';
            }
        }
    };
});
appDashboard.directive('numericOnly', function (Notification) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g, '') : null;

                if (transformedInput != inputValue && transformedInput != "" && transformedInput != null) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                if (transformedInput == "" || transformedInput == null) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                    Notification.error({
                        message: 'Please enter a numeric value.'
                    })
                }

                return transformedInput;
            });
        }
    }
});
appDashboard.directive('sibs', function () {
    return {
        link: function (scope, elem, attrs) {
            elem.bind('click', function (event, scope, attrs) {
                var outer = angular.element(this);
                outer.parent().children().addClass('theme-text');
                outer.removeClass('theme-text');
                var parentID = outer.attr("data-parentid");
                var parentElement = document.getElementById(parentID);
                var elementtest = angular.element(parentElement).children();
                elementtest.addClass('theme-text');
            });
        },
    };
});
appDashboard.directive('resize', function ($window, $rootScope) {
    return function (scope, element) {
        var w = angular.element($window);
        w.bind('resize', function () {
            scope.$broadcast('resizeCharts');
            scope.$apply();
        });
    }
});
appDashboard.directive('queryBuilder', ['$compile',  '$filter', '$timeout', '$http', 'Notification', 'mySharedService', function ($compile, $filter, $timeout, $http, Notification, mySharedService) {
    return {
        restrict: 'E',
        scope: {
            group: '='
        },
        templateUrl: '/QueryBuilderDirective.html',
        compile: function (element, attrs) {
            var content, directive;
            content = element.contents().remove();
            return function (scope, element, attrs) {
                scope.data = {};
                scope.strtdt = mySharedService.DBstrtdt;
                scope.enddt = mySharedService.DBenddt;
                scope.$broadcast('setbaseclr');
                scope.$on('pingbaseclr', function (e, data) {
                    scope.data.ATClr = data;

                })
                scope.group.rules.Startdate = new Date();
                scope.startOpened = false;
                scope.test = function () {
                    $timeout(function () {
                        scope.startOpened = true;
                    });
                };
                scope.group.rules.Enddate = new Date();
                scope.endOpened = false;
                scope.testEnd = function () {
                    $timeout(function () {
                        scope.endOpened = true;
                    });
                };

                scope.operators = [
                    {
                        name: 'AND'
					},
                    {
                        name: 'OR'
					}
				];

                scope.AndOroperators = [
                    {
                        name: 'AND'
					},
                    {
                        name: 'OR'
					}
				];
                //$routeParams.AssessmentId = "3"
                $http({
                        method: 'GET',
                        url: uriApiURL + "api/UserSetting/GetScoreboardColumnMap?AsgId=" + mySharedService.AssessmentId,
						params: {version: getEpocStamp()}
                    })
                    .success(function (response) {
                        scope.WholeData = angular.fromJson(response).FieldListVal;
                        scope.fields = scope.WholeData;
                        scope.data.fields = scope.fields;
                    });

                scope.changeField = function (changeVal, indexVal) {
                    scope.group.rules[indexVal].fieldType = $filter('filter')(scope.fields, changeVal.DbName)[0].fieldType;
                };

                scope.conditions = [
                    {
                        name: '='
					},
                    {
                        name: '<>'
					},
                    {
                        name: '<'
					},
                    {
                        name: '<='
					},
                    {
                        name: '>'
					},
                    {
                        name: '>='
					},
                    {
                        name: 'between'
					}
				];

                scope.NonNumericconditions = [
                    {
                        name: 'Is not null',
                        Displayname: 'Is Available'
					},
                    {
                        name: 'Is null',
                        Displayname: 'Is Not Available'
					},
				];

                scope.addCondition = function () {
                    var cmt = "";
                    if (scope.group.rules.length != 0) {
                        if (scope.group.rules[scope.group.rules.length - 1].fieldType == "Numeric") {
                            if (scope.group.rules[scope.group.rules.length - 1].condition == "between") {
                                if (scope.group.rules[scope.group.rules.length - 1].Startdata == undefined) {
                                    cmt += "Enter a Start value in Query Builder"
                                }
                                if (scope.group.rules[scope.group.rules.length - 1].Enddata == undefined) {
                                    cmt += "Enter a End value in Query Builder"
                                }
                            } else {
                                if (scope.group.rules[scope.group.rules.length - 1].data == "") {
                                    cmt += "Enter a value in Query Builder"
                                }
                            }
                        }
                        if (scope.group.rules[scope.group.rules.length - 1].fieldType == "Date") {

                            if (scope.group.rules[scope.group.rules.length - 1].condition == "between") {
                                if (scope.group.rules[scope.group.rules.length - 1].Startdate == undefined) {
                                    cmt += "Select a Start date in Query Builder"
                                }
                                if (scope.group.rules[scope.group.rules.length - 1].Enddate == undefined) {
                                    cmt += "Select a End date in Query Builder"
                                }
                            } else {
                                if (scope.group.rules[scope.group.rules.length - 1].stdate == undefined) {
                                    cmt += "Select a date in Query Builder"
                                }
                            }
                        }
                        if (cmt != "") {
                            Notification.error({
                                verticalSpacing: 60,
                                message: cmt
                            });
                        } else {

                            scope.group.rules.push({
                                AndOrOperator: 'OR',
                                condition: '=',
                                Noncondition: 'Is not null',
                                field: {
                                    name: scope.data.fields[0]['name'],
                                    fieldType: scope.data.fields[0]['fieldType'],
                                    DbName: scope.data.fields[0]['Column']
                                },
                                data: '',
                                fieldType: scope.data.fields[0]['fieldType']
                            });
                        }
                    } else {
                        scope.group.rules.push({
                            condition: '=',
                            field: {
                                name: 'Company',
                                fieldType: 'NonNumeric',
                                DbName: 'COMPANY_DISPLAY_NAME'
                            },
                            data: '',
                            Noncondition: 'Is not null',
                            fieldType: 'NonNumeric'
                        });
                    }
                };

                scope.removeCondition = function (index) {
                    if (index == 0) {
                        scope.group.rules.splice(index, 1);
                        scope.group.rules[0].AndOrOperator = undefined;
                    } else {
                        scope.group.rules.splice(index, 1);
                    }
                };

                scope.addGroup = function () {
                    scope.group.rules.push({
                        group: {
                            operator: 'AND',
                            rules: []
                        }
                    });
                };

                scope.removeGroup = function () {
                    "group" in scope.$parent && scope.$parent.group.rules.splice(scope.$parent.$index, 1);
                };

                if (scope.group.rules.length == 0) {
                    scope.addCondition();
                }

                directive || (directive = $compile(content));

                element.append(directive(scope, function ($compile) {
                    return $compile;
                }));
            }
        }
    }
}]);