﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADP_Common.Model
{
    public interface IUser
    {
        string UserID { get; set; }
        string UserName { get; set; }
    }
    public class ViewModeData
    {
        public virtual List<User> HighRes { get; set; }
        public virtual List<User> LowRes { get; set; }
    }
    public class ResolutionViewMode
    {
        public virtual List<UserViewMode> High { get; set; }
        public virtual List<UserViewMode> Low { get; set; }
    }
    public class UserViewMode
    {
        public virtual int? sizeX { get; set; }
        public virtual int? sizeY { get; set; }
        public virtual int? row { get; set; }
        public virtual int? col { get; set; }
        public virtual BaseKPIVal KPI { get; set; }
    }
    public class BaseKPIVal
    {
        public virtual string Id { get; set; }
        public virtual string baseKPIID { get; set; }
        public virtual string idNum { get; set; }
        public virtual string KPIName { get; set; }
    }
    public class User : UserKPIDashboard, IUser
    {
        public virtual string UserID { get; set; }
        public virtual QueryData QueryData { get; set; }
        public virtual string QueryValue { get; set; }
        public virtual string value { get; set; }
        public virtual string condition { get; set; }
        public virtual string unit { get; set; }
        public virtual string SLIAgeQuery { get; set; }
        public virtual string SLIAgeSelect { get; set; }
        public virtual string QueryJSON { get; set; }
        public virtual string QueryIcon { get; set; }
        public virtual int? ScoreBoardType { get; set; }
        public virtual string UserName { get; set; }
        public virtual string AllPriority { get; set; }
        public virtual string AllRequest { get; set; }
        public virtual string AllStatus { get; set; }
        public virtual string AllCriticality { get; set; }
        public virtual int? SizeXAxis { get; set; }
        public virtual int? SizeYAxis { get; set; }
        public virtual int? PositionXAxis { get; set; }
        public virtual int? PositionYAxis { get; set; }
        public virtual int? IsWithSLA { get; set; }
        public virtual string Resolution { get; set; }
        public virtual List<GenericData> BaseKPI { get; set; }
        public virtual BaseThemeColor UserTheme { get; set; }
        public virtual string UserEmailId { get; set; }
        public virtual string UserImage { get; set; }
        public virtual List<Admin> GetFilterObj { get; set; }
        public virtual List<Admin> GetPivotObj { get; set; }
        public virtual List<Admin> GetScoreboardObj { get; set; }
        public virtual List<Widget> GetWidgetObj { get; set; }
        //public virtual string IsPeriodic { get; set; }
        //public virtual int? PeriodicTime { get; set; }
        //public virtual string PeriodicTimeUnit { get; set; }
    }

    public class UserValidation
    {
        public virtual string CreatorName { get; set; }
        public virtual string CreatorID { get; set; }
        public virtual int? IsValidUser { get; set; }
        public virtual string CreatorEmail { get; set; }
        public virtual string CreatorMobile { get; set; }
    }

    public class UserKPIDashboard : BaseKPI
    {
        public virtual string Request_Priority { get; set; }
        public virtual string Request { get; set; }
        public virtual string Request_Status { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual string Criticality { get; set; }
        public virtual string SLICriticality { get; set; }
        public virtual string PlotType { get; set; }
        public virtual int? PlotValue { get; set; }
        public virtual string IsLine { get; set; }
        public virtual string ParetoBarUnit { get; set; }
        public virtual string ParetoBarColumn { get; set; }
        public virtual string ParetoBarColumnName { get; set; }
        public virtual string ParetoLineUnit { get; set; }
        public virtual string SplitData { get; set; }
        public virtual string GroupData { get; set; }
        public virtual int? IsParent { get; set; }
        public virtual int? IsWithSLA { get; set; }
        public virtual int? ScoreBoardType { get; set; }
        public virtual int? IsQuery { get; set; }
        public virtual string ChartColor { get; set; }
        public virtual int? IsGrouped { get; set; }
        public virtual int? IsUseTheme { get; set; }
        public virtual string Error { get; set; }
        
        public virtual string KpiType { get; set; }
        public virtual string GroupName { get; set; }
        public virtual string GroupKey { get; set; }
        public virtual int? KpiGroupID { get; set; }
        

    }

    public class UserDashboard : Dashboard, IUser
    {
        public virtual string DashboardStartDate { get; set; }
        public virtual string DashboardEndDate { get; set; }
        public virtual string UserEmailId { get; set; }
        public virtual string UserImage { get; set; }
        public virtual string SharedUserId { get; set; }
        public virtual string SharedUserName { get; set; }
        public virtual int InDays { get; set; }
        public virtual int IsPrivacySetting { get; set; }
        public virtual string SharedUser { get; set; }
        public virtual int IsInDays { get; set; }
        public virtual string UserID { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Request_Priority { get; set; }
        public virtual int IsCreator { get; set; }
        public virtual string Request { get; set; }
        public virtual string Criticality { get; set; }
        public virtual string Request_Status { get; set; }
        public virtual List<CommonData> AllPriority { get; set; }
        public virtual List<CommonData> AllRequest { get; set; }
        public virtual List<CommonData> AllStatus { get; set; }
        public virtual List<CommonData> AllCriticality { get; set; }
        public virtual List<CommonData> Filter { get; set; }
        public virtual List<Hiercharchy> HiercharchyFilter { get; set; }
        
        public virtual List<GroupDetail> GroupDetailFilter { get; set; }
        public virtual string IsCursol { get; set; }
        public virtual string IsScoreCard { get; set; }
        public virtual int? IsWithSLA { get; set; }

    }

    public class Hiercharchy : GenericData
    {
        public virtual string FilterName { get; set; }
    }

    public class QueryData
    {
        public virtual string QueryValue { get; set; }
        public virtual string QueryJSON { get; set; }
        public virtual string QueryIcon { get; set; }
        public virtual int? ScoreBoardType { get; set; }
        public virtual string IsPeriodic { get; set; }
        public virtual  int?  PeriodicTime{ get; set; }
    }

    public class GenericData
    {
        public virtual string GenericKey { get; set; }
        public virtual string GenericValue { get; set; }
        public virtual string IsTicked { get; set; }
        public virtual bool TickedVal { get; set; }
    }
    public class ViewModeResol
    {
        public virtual int? PositionX { get; set; }
        public virtual int? PositionY { get; set; }
        public virtual int? SizeX { get; set; }
        public virtual int? SizeY { get; set; }
    }
    public class BaseThemeColor
    {
        public virtual string USERID { get; set; }
        public virtual string Base_Color { get; set; }
        public virtual string Theme_Color { get; set; }

    }
    public class AdminDetails
    {
        public virtual string AdminID { get; set; }
        public virtual string AdminName { get; set; }
        public virtual int? IsAdmin { get; set; }
    }
    public class UserDetails
    {
        public virtual string UserID { get; set; }
        public virtual AdminDetails AdminDet { get; set; }
        public virtual string Location { get; set; }
        public virtual string Country { get; set; }
        public virtual string UserName { get; set; }
        public virtual string UserEmailId { get; set; }
        public virtual string UserImage { get; set; }
        public virtual string Base_Color { get; set; }
        public virtual string Theme_Color { get; set; }
        public virtual bool IsAuth { get; set; }
        public virtual string LastModifiedDate { get; set; }
        public virtual string StartRecordTimeStamp { get; set; }
        public virtual string IsLoginType { get; set; }
        public virtual string RedirectURL { get; set; }
    }

    public class UserData : Dashboard, IUser
    {
        public virtual string UserID { get; set; }
        public virtual string UserName { get; set; }
        public virtual string KPIID { get; set; }
        public virtual string isResp { get; set; }
        public virtual string PrivacySetting { get; set; }
    }
    public class CommonData
    {
        public virtual string value { get; set; }
        public virtual string id { get; set; }
        public virtual string name { get; set; }
        public virtual bool ticked { get; set; }
        public virtual int idNum { get; set; }
    }
    public class KPIUserSetting
    {
        public virtual List<CommonData> Request_Priority { get; set; }
        public virtual List<CommonData> Request { get; set; }
        public virtual List<CommonData> Request_Status { get; set; }
        public virtual List<CommonData> Criticality { get; set; }
        public virtual List<CommonData> ParetoColumn { get; set; }
        public virtual CommonData ScoreBoardType { get; set; }
        public virtual List<CommonData> IsLine { get; set; }
        public virtual string SLIAgeSelect { get; set; }
        public virtual List<CommonData> WhichUnit { get; set; }
        public virtual List<BaseKPI> BaseKPI { get; set; }
        public virtual List<Size> Size { get; set; }
        public virtual List<Position> Position { get; set; }
        public virtual int IsGrouped { get; set; }
        public virtual List<AgingReport> SLIAgeQuery { get; set; }
        public virtual int? IsWithSLA { get; set; }
        public virtual int IsParent { get; set; }
        public virtual int IsUseTheme { get; set; }
        public virtual string KPIName { get; set; }
        public virtual List<CommonData> SplitData { get; set; }
        public virtual List<CommonData> GroupData { get; set; }
        public virtual List<CommonData> SLICri { get; set; }
        public virtual List<ChartPlot> Plot { get; set; }
        public virtual List<SLICriticality> SLI { get; set; }
        public virtual string ChartColor { get; set; }
        public virtual List<CommonData> FilterObj { get; set; }
        public virtual List<CommonData> PivotObj { get; set; }
        public virtual List<CommonData> GetScoreboardObj { get; set; }
        public virtual List<CommonData> WidgetObj { get; set; }
        public virtual QueryData QueryData { get; set; }
        public virtual string ParetoBarUnit { get; set; }
        public virtual string ParetoLineUnit { get; set; }
        public virtual string IsPeriodic { get; set; }
        public virtual int? PeriodicTime { get; set; }
        public virtual string PeriodicTimeUnit { get; set; }
        public virtual List<CommonData> RemedyObj { get; set; }
    }
    public class SLICriticality
    {
        public virtual string SCriticality { get; set; }
        public virtual string Value { get; set; }
    }
    public class ChartPlot
    {
        public virtual string PlotType { get; set; }
        public virtual string PlotValue { get; set; }
    }
    public class Size
    {
        public virtual string SizeXAxis { get; set; }
        public virtual string SizeYAxis { get; set; }
    }
    public class Position
    {
        public virtual string PositionXAxis { get; set; }
        public virtual string PositionYAxis { get; set; }
    }

    public class GroupDetail
    {
        public virtual int? GroupID { get; set; }
        public virtual int? GroupType { get; set; }
        public virtual string GroupName { get; set; }
        public virtual int? GroupView { get; set; }
        public virtual List<BaseKPIVal> KPI { get; set; }
        public virtual int? GroupIndex { get; set; }
        public virtual string GroupFilter { get; set; }
        public virtual string GroupFilterValue { get; set; }
        public virtual string DashBoardID { get; set; }
        public virtual string IsFilter { get; set; }
        public virtual string GroupKey { get; set; }
        public virtual string GroupFilterLabel { get; set; }
    }


    public class KPICollections
    {
       public virtual string DashBoardID { get; set; }
       public virtual List<BaseKPIVal> Cursol { get; set; }
        public virtual List<BaseKPIVal> ScoreCard { get; set; }
        public virtual List<GroupDetail> groups { get; set; }
        public virtual string IsCursol { get; set; }
        public virtual string IsScoreCard { get; set; }

    }

    [Serializable]
    public class UserProfileSessionData
    {
        public string UserId { get; set; }

        public string EmailAddress { get; set; }

        public string FullName { get; set; }
    }

    public class UserDashboarddetail
    {
        public virtual string Userval { get; set; }
    }

    public class CloneDashboard
    {
        public virtual string CloneDb { get; set; }
       
    }
    public class CloneDashboarddetail
    {
        public virtual string dashBoardIcon { get; set; }
        public virtual string dashBoardName { get; set; }

    }

    public class AdminDetailCollection
    {
        public virtual List<AdminInfoDetail> AdminInfo { get; set; }
        public virtual List<CommonData> Request_Priority { get; set; }
        public virtual List<CommonData> Request { get; set; }
        public virtual List<CommonData> Request_Status { get; set; }
        public virtual List<CommonData> Criticality { get; set; }
    }

    public class AdminInfoDetail
    {
        public virtual int Slno { get; set; }
        public virtual string AppName { get; set; }
        public virtual string AppLogo { get; set; }
        public virtual string Status { get; set; }

    }

    public class SaveFilteValue
    {
        // public virtual string WidgetSetting { get; set; }
        public virtual string Userval { get; set; }
    }
    public class GetFilteValue
    {
        public virtual string FilterValue { get; set; }
        public virtual string Userval { get; set; }
    }
    public class DrilldownData
    {
        public virtual int ISMonth { get; set; }
        public virtual string Userval { get; set; }
        public virtual string Request_Priority { get; set; }
        public virtual string YearMonthWeek { get; set; }
        public virtual string Exceeding { get; set; }
        public virtual string FirstGenericData { get; set; }
        public virtual string SecondGenericData { get; set; }
        public virtual string ThirdGenericData { get; set; }
    }
    public class GetPartoValue
    {
        public virtual string SpinnerData { get; set; }
        public virtual string PrioritySelected { get; set; }
    }
}
