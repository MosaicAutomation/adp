﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADP_Common.Model
{
    public class Asg_Map
    {
        public virtual int? AsgId { get; set; }
        public virtual string AsgJson { get; set; }
    }
    public class ClusterJson
    {
        public virtual string ASG_ID { get; set; }
        public virtual string TAG { get; set; }
        public virtual string DISPOSITION { get; set; }
        public virtual string CLUSTER_ID { get; set; }
        public virtual string REQUEST_ID { get; set; }
        public virtual string PROC_DESCRIPTION { get; set; }
    }
    public class ClusterSaveJson
    {
        public virtual string ASG_ID { get; set; }
        public virtual string Tag_Name { get; set; }
        public virtual string Disposition { get; set; }
        public virtual string Assignments { get; set; }
        public virtual string Request_Id { get; set; }
        public virtual string Proc_Description { get; set; }
    }
    public class CategoryChoose
    {
        public virtual string value { get; set; }
        public virtual string name { get; set; }
        public virtual bool ticked { get; set; }
    }
    public class AssignmentDetails
    {
        public virtual string AssignmentId { get; set; }
        public virtual List<WordCloud> WordCloud { get; set; }
        public virtual List<ClusterDetail> ClusterData { get; set; }
    }
    public class MapTable
    {
        public virtual List<MapTableData> MapData { get; set; }
    }
    public class excelsample
    {
        public virtual string Data { get; set; }
    }
    public class MapTableData
    {
        public virtual string name { get; set; }
        public virtual string jsonFile { get; set; }
    }
    public class ClusterDetail
    {
        public virtual string tn { get; set; }
        public virtual string ct1 { get; set; }
        public virtual string ct2 { get; set; }
        public virtual string ct3 { get; set; }
        public virtual string sd { get; set; }
        public virtual string pd { get; set; }
        public virtual string wd { get; set; }
        public virtual string asg { get; set; }
        public virtual string Tags { get; set; }
        public virtual string Disposition { get; set; }
    }
    public class Contact
    {
        public string name { get; set; }
        public string phone { get; set; }
    }
    public class DwnlDatatocluster
    {
        public virtual string Request_Id { get; set; }
        public virtual string Description { get; set; }
        public virtual string Proc_Description { get; set; }
    }
    public class DwnlData
    {
        public virtual string Request_Id { get; set; }
        public virtual string Description { get; set; }
        public virtual string Proc_Description { get; set; }
    }
    public class WordCloud
    {
        public virtual string text { get; set; }
        public virtual int weight { get; set; }
    }
    public class StepProgress
    {
        public virtual int AcceptTc { get; set; }
        public virtual int UnCategTck { get; set; }
        public virtual int ReCluster { get; set; }
        public virtual int NotClustered { get; set; }
        public virtual int Total { get; set; }
        public virtual int? ActStep { get; set; }
        public virtual int ActTab { get; set; }
        public virtual bool UploadMaster { get; set; }
        public virtual string AsgName { get; set; }
        public virtual int? Is_Shared { get; set; }
        public virtual string USER_NAME_MAP { get; set; }
        public virtual string User_ID_Map { get; set; }
        public virtual List<UserDetailPrivacysettings> List { get; set; }

    }
    public class Ref
    {
        public virtual int RefId { get; set; }
    }
    public class ListPreProc
    {
        public virtual List<PreProc> StopWords { get; set; }
        public virtual List<PreProc> Synonyms { get; set; }
    }
    public class ListDelWrds
    {
        public virtual List<DelSynms> Synonyms { get; set; }
        public virtual List<DelStopWrds> StopWords { get; set; }
    }
    public class DelSynms
    {
        public virtual int ASG_ID { get; set; }
        public virtual string Used_Word { get; set; }
        public virtual string Replacement_Word { get; set; }
    }
    public class DelStopWrds
    {
        public virtual int ASG_ID { get; set; }
        public virtual string Stop_Word { get; set; }
        public virtual int Remove_word { get; set; }
    }
    public class PreProc
    {
        public virtual int Rel_Id{ get; set; }
        public virtual string word { get; set; }
        public virtual string rep_word { get; set; }
        public virtual int? AsgId { get; set; }
    }
    public class PythonProcess
    {
        public virtual int? Count { get; set; }
        public virtual int? Length { get; set; }
        public virtual string Rep_word { get; set; }
        public virtual string Word { get; set; }
        public virtual int? Remove_Word { get; set; }

    }  

    public class PythonOrder
    {
        public virtual string Request_Id { get; set; }
        public virtual string Description { get; set; }
        public virtual string Proc_Description { get; set; }
       

    }
}